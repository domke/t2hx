%\fix{page goal: 1(r);2(l)}
%
\begin{figure*}[tbp]
    \centering
    \begin{subfigure}[b]{.33\textwidth}
        \centering
        \def\svgwidth{\columnwidth}
        {\scriptsize\input{figures/karyn.pdf_tex}}
        \caption{\label{sfig:karyn} 4-ary~2-tree with 16 compute nodes}
    \end{subfigure}\hfill
    \begin{subfigure}[b]{.35\textwidth}
        \centering
        \def\svgwidth{.8\columnwidth}
        {\tiny\input{figures/hyperx.pdf_tex}}
        \caption{\label{sfig:hyperx} 2-D 4x4 HyperX with 32 compute nodes}
    \end{subfigure}\hfill
    \begin{subfigure}[b]{.30\textwidth}
        \centering
        \includegraphics[width=\columnwidth]{figures/system}
        \caption{\label{sfig:t2hx} 28-node rack with dual-plane network}
    \end{subfigure}
    %J\vspace{-.5\baselineskip}
    \caption{\label{fig:net} Two full-bisection bandwidth networks (Fig.~\ref{sfig:karyn}: indirect Fat-Tree topology; Fig.~\ref{sfig:hyperx}: direct HyperX topology) and Fig.~\ref{sfig:t2hx} shows one of 24 racks of our 672-node supercomputer with two edge switches connecting to the 3-level Fat-Tree and four switches for the 12x8 HyperX network (brown = rack-internal passive copper cables; gaps used for cable management)}
\end{figure*}
%
\section{Introduction}\label{sec:intro}
The striking scale-out effect, seen in previous generations at the top end of the
supercomputer spectrum, e.g., the K computer with over 80,000 compute nodes or the
Sunway TaihuLight system with over 40,000 nodes~\cite{strohmaier_top500_2018}, is
likely to continue into the far future to tackle the challenges associated with
the ending of Moore's law. The interconnection network, used to tightly couple these
HPC systems together, faces increased demands for ultra-low latency from traditional
scientific applications, as well as new demands for high throughput of very large
messages from emerging deep learning workloads.
%%While traditional, and commonly deployed, Clos or Fat-Tree network topologies
%%could provide the throughput, the observable latency will suffer from increasing
%%in the number of intermediate switches when scaling up the tree levels, and the
%%overall hardware cost for these networks could soon dominate the procurement cost
%%of the whole HPC system.
%Traditional, and commonly deployed, Clos or Fat-Tree network topologies
%theoretically can provide the throughput, but the observable latency will suffer
%from increasing in the number of intermediate switches. In addition, scaling up
%the tree levels, yielding an aggravation in required networking equipment,
%could soon dominate the overall procurement cost of a HPC system.
While the commonly deployed Clos or Fat-Tree network topologies could provide the
throughput, their cost structure is prohibitive and the observable latency will
suffer from the increasing number of intermediate switches when scaling up the
tree levels. Furthermore, as signaling rates are headed beyond \unit[50]{Gbps}
(greater than \unit[200]{Gbps} per 4X port), ``electrically-optimized'',
low-diameter topologies like the various ``flies'' --- e.g.,
Dragonfly~\cite{kim_technology-driven_2008}, Dragonfly+~\cite{shpiner_dragonfly+:_2017},
or Slimfly~\cite{besta_slim_2014} --- may
no longer be relevant.

In a future driven by co-packaged optics and fiber shuffles, the
HyperX~topology~\cite{ahn_hyperx:_2009} could be the alternative to provide
high-throughput and low network dimensionality, with the most competitive
cost-structure.
%
%Alternative, low-diameter topology designs, such as the various ``flies'' ---
%e.g., Dragonfly~\cite{kim_technology-driven_2008}, Slimfly~\cite{besta_slim_2014} ---
%or the HyperX topology~\cite{ahn_hyperx:_2009}, are promising contenders to the
%Fat-Trees, especially from the perspective of procurement cost.
%It stems from the fact
%that these networks achieve the same or similar throughput as the Fat-Tree with
%drastically reduced number of switches and (active optical) cables.
%One drawback of
%these direct and/or low-diameter topologies is, however, that the connection between
%two switching elements creates a bottleneck, usually bypassed by non-minimal, adaptive
%routing~\cite{abts_high_2011}.
One drawback of
these direct and/or low-diameter topologies is, however, the potential bottleneck
between two adjacent switches, usually bypassed by non-minimal, adaptive
routing~\cite{abts_high_2011}.
The adverse effect of static, minimal
routing for a HyperX is visualized in Figure~\ref{fig:teaser}. The average observable
intra-rack bandwidth for bisecting communication patterns is with \unit[2.26]{GiB/s}
close to the maximum on a Fat-Tree (left), whereas this average drops to
mere~\unit[0.84]{GiB/s} on a HyperX (middle).
The cause: up to seven traffic streams may share a single cable in
the default HyperX network configuration with minimal routing.
Our non-minimal, static PARX routing, developed for the HyperX,
alleviates this issue, as shown in the right-most heatmap of Figure~\ref{fig:teaser},
boosting the average bandwidth by~66\% to~\unit[1.39]{GiB/s} per node pair.

In this study, we empirically quantify and analyze the viability of the
HyperX topology --- so far only theoretically investigated --- by rewiring a
large-scale, multi-petaflop, and recently decommissioned supercomputer.
The 1\textsuperscript{st} network plane of the original system is kept as
Fat-Tree topology, while the 2\textsuperscript{nd} plane is modified to
facilitate our fair comparison.
We stress both network topologies with numerous synthetic network benchmarks,
as well as a broad set of scientific (proxy-)applications, e.g., sampled from
the Exascale Computing Project (ECP) proxy applications~\cite{exascale_computing_project_ecp_2018}
and other procurement benchmarks used by the HPC community.
To mitigate the lack of adaptive routing, and resulting bottlenecks, in our
dated generation of InfiniBand (QDR type), we develop and test different strategies,
i.e., rank placements and our novel communication-/topology-aware routing algorithm.
In short, the contributions of our paper are:
\begin{enumerate}
%    \item The construction of the first large-scale HPC prototype employing a
%      HyperX topology by resurrecting an old system,
    \item We perform a fair, in-depth comparison between HyperX and Fat-Tree using
      more than a dozen different HPC workloads,
    \item We develop a novel topology- and communication-aware routing algorithm for
      HyperX, which mitigates bottlenecks induced when routing along shortest
      paths, and
    \item We demonstrate that even a statically routed HyperX network can
      rival or outperform the more costly Fat-Tree topology for some realistic
      HPC workloads.
\end{enumerate}
