%\fix{page goal: 4;5(l)}
%
\section{Bottleneck Mitigation for HyperX}\label{sec:mitigation}
Similar to other low-diameter topologies, such as Dragonfly or Slimfly, the HyperX
relies on AR to avoid oversubscribing the
shortest path.
%--- usually only connected by a few links --- between two
%groups of nodes.
Without it, see Figure~\ref{fig:teaser}, the
throughput is severely limited.
%Unfortunately, as described in the last
%Section~\ref{sec:networks},
Hence, we develop two mitigation
strategies for our QDR IB technologies which only allows
for static and flow-oblivious routing.
%, detailed hereafter.



\subsection{Application-to-Compute Node Mapping}\label{ssec:placement}
The most obvious solution to the bottleneck problem, induced by static routing,
is to spread out the allocation of nodes for an application, such that node-adjacent
switches are either not directly connected, or only a subset of nodes of each
switch is used. Ideally, one would perform a topology-/routing-aware rank mapping,
see for example~\cite{hoefler_generic_2011,abdel-gawad_rahtm:_2014}. However,
these strategies are impractical in production environments, due to limited
availability of idle resources. Since we do not recommend to deploy HyperX without
adaptive routing, we only use a simple random assignment of ranks to nodes
in this study to test if it mitigates the bottleneck. The disadvantage of
this approach is an increased latency for small messages.



\subsection{Non-Shortest Path Routing for HyperX}\label{ssec:parx}
An alternative to the node mapping is
altering the traffic flow to utilize more bandwidth of the network by using
non-minimal paths, similar to how AR for Dragonfly
topologies operates~\cite{kim_technology-driven_2008,faanes_cray_2012}.
However, statically routing along non-shortest paths is complicated in IB, due
to the destination-based forwarding scheme employed
by IB switches~\cite[Sec. 3.4.3]{infiniband_trade_association_infinibandtm_2015}.
Assume, for example, a triangle of switches A, B, and C with one node per switch,
then theoretically A's node can send traffic to C's via B, but at the same time
B's node cannot send traffic to C's via A, because packets would get stuck in
an infinite forwarding loop between A and B.
Hence, we require a novel routing algorithm for HyperX and communication
scheme for applications which satisfies the following four criteria:
\begin{enumerate}
    \item Small messages are routed along shortest paths;
    \item Large messages utilize non-shortest paths;
    \item For all node pairs the choice between (1) and (2) exists; and
    \item The routing is loop-free, fault-tolerant and deadlock-free.
\end{enumerate}
While the first three criteria obviously aim for low latency and increased
throughput, criterion (4) is required due to our imperfect HyperX deployment, see
Section~\ref{sec:networks}. The deadlock-freedom demand became essential after
initial tests with OpenSM's SSSP routing~\cite{openfabrics_alliance_opensm_2018}.
The next Sections~\ref{ssec:parx1}--\ref{ssec:parx4} detail how we
accomplish our goal.



\subsubsection{Concurrently Forwarding along Minimal \& Non-Minimal Paths}\label{ssec:parx1}
As the previous thought experiment examplifies, it can be impossible --- with
out-of-the-box InfiniBand routing mechanisms --- to accomplish criteria (1)--(3)
for the general case, i.e., larger topologies comprised of multiple switches.
%However, the InfiniBand technology allows administrators to configure up to 128
%virtual destinations per physical port of each host channel adapters (HCA;
%InfiniBand's terminology for network interface cards),
%see~\cite[Sec. 4.1.2]{infiniband_trade_association_infinibandtm_2015}.
%Each port gets assigned one base local identifier (LID) --- default configuration
%--- and between 0 and 127 additional LIDs depending on the LID mask control (LMC)
%parameter specified in the configuration for the subnet manager. The subnet
%manager, usually OpenSM, then calculates the forwarding tables as if each
%(virtual) LID would be a physical endpoint / compute node in the
%fabric~\cite[Sec. 3.5.10]{infiniband_trade_association_infinibandtm_2015}.
However, IB enables up to 128 virtual destinations, called local identifier (LID),
per physical port of host channel adapters (HCA),
see~\cite[Sec. 4.1.2]{infiniband_trade_association_infinibandtm_2015}.
The LID mask control (LMC) defines how many (up to 127) additional LIDs are
assigned besides the base $\text{LID}_0$. The subnet
manager, usually OpenSM, then calculates the forwarding tables as if each
(virtual) LID would be a physical endpoint in the
fabric~\cite[Sec. 3.5.10]{infiniband_trade_association_infinibandtm_2015}.

\begin{figure}[tbp]
    \centering
    \begin{subfigure}[b]{.31\columnwidth}
        \centering
        \def\svgwidth{\columnwidth}
        {\Huge\input{figures/parx1.pdf_tex}}
        \caption{\label{sfig:quad1} Quadrants}
    \end{subfigure}\hfill
    \begin{subfigure}[b]{.31\columnwidth}
        \centering
        \def\svgwidth{\columnwidth}
        {\Huge\input{figures/parx2.pdf_tex}}
        \caption{\label{sfig:quad2} Forced Detours}
    \end{subfigure}\hfill
    \begin{subfigure}[b]{.31\columnwidth}
        \centering
        \def\svgwidth{\columnwidth}
        {\Huge\input{figures/parx3.pdf_tex}}
        \caption{\label{sfig:quad3} Minimum Paths}
    \end{subfigure}
    %J\vspace{-.5\baselineskip}
    \caption{\label{fig:quad} Illustrate quadrant-based partitioning of a 2D HyperX to achieve minimal \& non-minimal routing via LMC-based multi-pathing; See Sec.~\ref{ssec:parx1} for more details}
\end{figure}
%
%We utilize this multi-destination feature of InfiniBand to construct a
%topology-aware and static routing algorithm for 2-dimensio-nal HyperX topologies,
%such as deployed in our rewired supercomputer. Our novel approach is certainly
%generalizable to higher dimensions, however due to the prototype nature of
%it\footnote{Future HyperX deployments use AR, making our static routing prototype
%obsolete.}, we limit our implementation to only 2D topologies with even dimensions.
We utilize this multi-destination feature of IB to construct a topology-aware,
static routing for our rewired supercomputer with 2D HyperX topology.
Our novel approach is generalizable to higher dimensions, however due
to the prototypic nature of
it\footnote{Future HyperX deployments use AR, making our static routing prototype
obsolete.}, we limit ourselves to only 2D HyperX topologies with even dimensions.

Assuming, we virtually divide the 2D HyperX into four quadrants Q0--Q3, see
Figure~\ref{sfig:quad1}, and each HCA/port is configured with four destination
LIDs (by setting $\text{LMC}=2$ and indexed via
$\text{LID}_0,\ldots,\text{LID}_3$), then the calculated
paths towards them may be partially or fully disjoint\footnote{Statement depends on
applied routing and omits switch-to-HCA sections of the path.}.
Unfortunately, available static
routing for IB will only calculate routes along the minimal
paths in the network, unless it serves the purpose of deadlock-avoidance,
e.g., as performed by Up*/Down*~\cite{schroeder_autonet:_1991} or
Nue routing~\cite{domke_routing_2016}. Hence, to enforce the traffic forwarding
along non-minimal paths, a routing algorithm must be
topology-aware and/or temporarily disregard links which otherwise contribute to
a minimal path length.

Our approach for this challenge is to virtually remove
some adverse links during the path calculation.
Let's illustrate this approach through an example before generalizing it for the
entire HyperX:
Assuming, the routing iteratively processes the destination LIDs and the
``currently'' processed $\text{LID}_0$ belongs to quadrant Q1, see
Figure~\ref{sfig:quad2}.
Now, if we virtually remove all network links within the left half of the HyperX, then
it will cause all ``shortest'' paths originating from Q0 or Q1 to traverse through Q2
and/or Q3. The benefit is an increase
of non-overlapping paths, i.e., from at most two to $\frac{D_1}{2}$, if the
1\textsuperscript{st} dimension of the HyperX is larger than~4.
\added{In this example, we gain three additional paths.} Large messages
should utilize the increased link bandwidth to avoid creating bottlenecks on
direct links in the left half. \added{In contrast, if another virtual $\text{LID}_1$ is
attached to the same switch in Q1 and the link removal is applied to the right half, then
all paths towards $\text{LID}_1$ will be minimal, exemplified in Figure~\ref{sfig:quad3}.
However, paths ending in Q2/Q3 may detour.}
%Figure~\ref{sfig:quad2}.
%If one would virtually remove all network links which are connecting switches
%within Q0 or Q1, or links between Q0 and Q1 --- essentially cutting all direct
%links in the left half of the HyperX --- then all ``shortest'' paths originating
%from Q0 or Q1 have to traverse through Q2 and/or Q3. The benefit is an increase
%of non-overlapping paths, i.e., from at most two to $\frac{D_1}{2}$, if the
%1\textsuperscript{st} dimension of the HyperX is larger than~4. These paths
%can be used by large messages to avoid creating bottlenecks on direct links
%in the left half. In contrast, if a $\text{LID}^*_0$ would be attached to a switch
%in Q2, as shown in Fig.~\ref{sfig:quad3}, and the link removal is the same, then
%all paths towards $\text{LID}^*_0$ will be minimal.

To generalize this example, we define the following four rules when processing
a given destination LID in the routing engine:
\begin{enumerate}
    \item[(R1)] $\text{given LID} \equiv \text{LID}_0 \;\Rightarrow\; $ remove all links in left half
    \item[(R2)] $\text{given LID} \equiv \text{LID}_1 \;\Rightarrow\; $ remove all links in right half
    \item[(R3)] $\text{given LID} \equiv \text{LID}_2 \;\Rightarrow\; $ remove all links in top half
    \item[(R4)] $\text{given LID} \equiv \text{LID}_3 \;\Rightarrow\; $ remove all links in bottom half
\end{enumerate}
and any communication traffic injected into the network, which is addressed to
arrive at node~$n$, should select the correct virtual destination
$\text{LID}^n_x$ based on the message size and based on the numbers for~$x$
listed in Table~\ref{tab:quad}.
%
One can easily deduct from these two Tables~\ref{tab:quadA} and \ref{tab:quadB},
and Figure~\ref{fig:quad}, that this routing approach achieves criteria (1)--(3)
for a 2D HyperX topology, such as ours\footnote{We accomplish the identification of
quadrants in our implementation by predefining the LID-to-\{port|switch\}
assignment through OpenSM's \textit{guid2lid} mapping file.}.
%
\begin{table}[tbp]
    \caption{\label{tab:quad} Valid choices of virtual destination $\text{LID}_x$ depending on message size ( s=source; d=destination; | stands for \textit{or} ); Sec.~\ref{ssec:parx4} for how we distinguish between small and large}
    %J\vspace{-.5\baselineskip}
    \centering\footnotesize
    \begin{minipage}{.5\linewidth}
        \centering
        \subcaption{\label{tab:quadA} $x$ for small messages}
        \begin{tabular}{|c|c|c|c|c|}
            \hline \hCC
            \tBSB{s}{d} &   \tF{Q0} &   \tF{Q1} &   \tF{Q2} &   \tF{Q3} \\ \hline
            \tF{Q0}     &   \textbf{1 | 3}   &   \textbf{1}       &   \textbf{0 | 2}   &   \textbf{3}       \\ \hline
            \tF{Q1}     &   \textbf{1}       &   \textbf{1 | 2}   &   \textbf{2}       &   \textbf{0 | 3}   \\ \hline
            \tF{Q2}     &   \textbf{1 | 3}   &   \textbf{2}       &   \textbf{0 | 2}   &   \textbf{0}       \\ \hline
            \tF{Q3}     &   \textbf{3}       &   \textbf{1 | 2}   &   \textbf{0}       &   \textbf{0 | 3}   \\ \hline
        \end{tabular}
    \end{minipage}%
    \begin{minipage}{.5\linewidth}
        \centering
        \subcaption{\label{tab:quadB} $x$ for large messages}
        \begin{tabular}{|c|c|c|c|c|}
            \hline \hCC
            \tBSB{s}{d} &   \tF{Q0} &   \tF{Q1} &   \tF{Q2} &   \tF{Q3} \\ \hline
            \tF{Q0}     &   \textbf{0 | 2}   &   \textbf{0}       &   \textbf{0 | 2}   &   \textbf{2}       \\ \hline
            \tF{Q1}     &   \textbf{0}       &   \textbf{0 | 3}   &   \textbf{3}       &   \textbf{0 | 3}   \\ \hline
            \tF{Q2}     &   \textbf{1 | 3}   &   \textbf{3}       &   \textbf{1 | 3}   &   \textbf{1}       \\ \hline
            \tF{Q3}     &   \textbf{2}       &   \textbf{1 | 2}   &   \textbf{1}       &   \textbf{1 | 2}   \\ \hline
        \end{tabular}
    \end{minipage}
\end{table}



\subsubsection{Optimization for Communication Demands of Applications}\label{ssec:parx2}
Another advantage of AR over static routing is the ability to dynamically
balance traffic flows. For the latter, a mismatch between
the calculated paths and injected flows can cause congestion~\cite{hoefler_multistage_2008}.
%
Tuning the static routes towards the actual communication demands
of one or more applications may be beneficial --- assuming a relatively
sparse and reoccurring communication pattern --- for emulating AR.
A similar strategy, called scheduling-aware routing
(SAR)~\cite{domke_scheduling-aware_2016}, adds application-to-node mappings
into the routing engine.
%Considering
%actual communication demands takes this optimization of the path calculation,
%to meet the applications' demands, one step further towards real adaptive routing.
Hence, we modify SAR~\cite{domke_scheduling-aware_2016} in Section~\ref{ssec:parx3}
to ingest communication profiles.

Acquiring these communication profiles for point-to-point MPI messages~\cite{message_passing_interface_forum_mpi:_2015}
is trivial with tools such as Vampir(Trace)~\cite{knupfer_vampir_2008} or
TAU~\cite{shende_tau_2006}, but for MPI's collective operations
these tools only provide high-level information. Actual point-to-point
messages which compose the collectives, and therefore the real node-to-node
traffic demand, remain unrecorded. Hence, we
rely on a low-level IB profiler~\cite{brown_hardware-centric_2015},
to record/store the profiles for each
combination of benchmark, input, and number of MPI ranks\footnote{This profile is
immune to changed in MPI rank placement, topology, and IB routing.}.
Our routing engine can then perform fine-grain path balancing optimizations.



\subsubsection{Pattern-Aware Routing for HyperX Topologies (a.k.a.~PARX)}\label{ssec:parx3}
We combine the above outlined technical aspects into one novel
routing algorithm which is tailored for statically-routed, InfiniBand-based
2D HyperX topologies. PARX increases path diversity by 
simultaneously providing minimal and non-minimal paths and allows for
communication demand-based re-routing of the fabric, all while
being fault-tolerant\footnote{Fault-tolerance is limited, because temporarily
removing links, see Sec.~\ref{ssec:parx1}, can result in unreachable
LIDs if the adjacent switch has no remaining links.} and
deadlock-free\footnote{For all of our evaluations, see Sec.~\ref{sec:eval},
PARX requires between 5 and 8 virtual lanes (VLs), depending on ingested
communication profile, which is within limit of 8 available VLs for our
IB hardware. PARX may exceed a VL hardware limit for larger HPC systems.}.

We implement PARX in IB's subnet manager (OpenSM) by taking the
deadlock-free SSSP routing (DFSSSP)~\cite{domke_deadlock-free_2011,openfabrics_alliance_opensm_2018}
as basis, and modify it by adding an altered version of the SAR
extensions~\cite{domke_scheduling-aware_2016} --- to ingest communication profiles
instead of rank-to-node mappings. Furthermore, we changed DFSSSP's path
calculation to temporarily ignore/remove links from the network to abide by
the rules (R1)--(R4), and to consider the data from the communication profiles
for path balancing and tuning purposes. The pseudo-code of the PARX routing
engine for our HyperX is shown in Algorithm~\ref{algo:parx}.
%
\begin{algorithm}[tbp]
        \addtolength{\hsize}{.8\algomargin}
        \KwIn{Network graph $I=G(N,C)$ with nodes $N$ and links $C$,\quad Communication demand file with one line per source node $D:=[(\text{<destination>},\text{<send demand>}),\ldots]$}
        \KwResult{Communication-aware, minimal \& non-minimal, deadlock-free routing \\\qquad\quad\ \,with valid paths $P_{n_x,n_y}$ for all $n_x,n_y \in N$}
        \tcc{Process of the communication demands}
        \ForEach{$\,\text{node } n \in N\,$}{
                $n$.demands $\leftarrow$ empty list []\;
                \ForEach{$\text{pair\, (nodeName, send demand) } \in D[n]$}{
                        \lIf{$\text{nodeName} \equiv m \in N$}{
                                $n$.demands.append[($m$, send demand)]
                        }
                }
        }
        \tcc{Optimize routing for compute nodes listed in $D$, assuming $2^\text{LMC}=4$}
        \ForEach{$\,\text{node } n_d \in N \text{ with } (n_d,\cdot) \in D\,$}{
                \ForEach{$i \in \{0,\ldots,3\}$}{
                        Create temporary graph $I^*=G(N,C^*)$ by remove links from $C$ according to $\text{LID}_i$ of $n_d$ and according to the rules (R1)--(R4) listed in Sec.~\ref{ssec:parx1}\;
                        Calculate a path $P_{n_x,n_d}$ for every pair $(n_x,n_d)$, with $n_x \in N$ in $I^*$ by using the modified Dijkstra algorithm of DFSSSP routing (details in~\cite{domke_deadlock-free_2011})\;
                        \tcc{Update edge/link weights in graph $I$ before the next round}
                        \ForEach{$\text{node } n_x \in N$}{
                                \If{$(n_{d}, w) \in n_{x}.\text{demands}$}{
                                        Increase edge weight by $+w$ for each link in path $P_{n_x,n_d}$
                                }
                        }
                }
        }
        \tcc{Calculate routing for all other nodes which are not listed in $D$}
        \ForEach{$\,\text{node } n_d \in N \wedge n_d \text{ not processed before}\,$}{
                Calculate paths $P_{\cdot,n_d}$ for all $n_x \in N$ and $i=0,\ldots,3$ as shown above, however\;
                Only update edge weights for all links used by $P(\cdot,n_d)$ with a $\,+1$ per path\;
        }
        \tcc{Create deadlock-free routing configuration}
        \ForEach{$\,\text{path } P_{n_x,n_y} \text{ calculated above (incl. all virtual LIDs)}\,$}{
                Assign $P_{n_x,n_y}$ to one virtual layer without creating a cycle in the corresponding channel dependency graph (see~\cite{domke_deadlock-free_2011,skeie_layered_2002} for details on VL-based deadlock-avoidance)\;
        }
        \caption{\label{algo:parx} Pattern-Aware Routing for 2D HyperX (PARX)}
\end{algorithm}

\added{The communication profiles contain the absolute number of bytes
transferred between every pair of MPI ranks during the program execution.
We normalize these (potentially large) numbers to the integer range of
$D_n:=[0,\ldots,255]$, where~0 stands for absolutely no bytes transferred
between two ranks. A~1 indicates a relatively low amount of bytes
and~255 represents the highest traffic demand between two MPI ranks.
These normalized traffic demands are used by PARX to balance the
routes assigned to links such that the number of overlapping paths,
carrying high traffic demands, are minimized. The base algorithm, DFSSSP,
alternates between calculating all paths towards one destination
$\text{LID}_x$ and performing edge update of the weighted graph representing
the network topology. Per calculated path, DFSSSP adds +1 to each link along
the path~\cite{domke_deadlock-free_2011}. Hence, DFSSSP's edge update results
in global path balancing, oblivious to the workload on the HPC system.
In contrast, our PARX algorithm updates the edges by adding~$w \in D_n$,
resulting in per-application(s) optimized paths, see the
inner-most loop of the triple nested loop in Algorithm~\ref{algo:parx}.
This approach reduces the dark fiber~\cite{domke_scheduling-aware_2016},
and high-traffic paths are separated as much as possible to reduce
congestion observed by the applications.}



\subsubsection{Modifications to the Message Passing Interface (MPI) Library}\label{ssec:parx4}
The criteria (1) and (2) listed in Section~\ref{ssec:parx}, and routing approach
with PARX, require a categorisation of messages injected into the network, as well
as the assignment of appropriate (virtual) destination LIDs for these messages.
None of the existing MPI libraries is capable of performing this task.
While, Open~MPI~\cite{gabriel_open_2004} supports IB's multi-LID addressing,
the default configuration --- with the \textit{ob1} point-to-point messaging
layer (PML) --- uses multiple LIDs only for fail-over in case of connection
issues on the primary path.

The alternative PML, called \textit{bfo}, offers concurrent multi-pathing
for IB by setting up as many connections between two HCAs
as these are (virtual) LIDs assigned to them, i.e.,
$\text{LID}_0$ of port~$s$ can communicate with $\text{LID}_0$ of port~$d$,
$\text{LID}^s_1$ with $\text{LID}^d_1$, etc.
The bfo~PML iterates through the $2^\text{LMC}$ LIDs in a round-robin fashion.
After transferring a message (or message segment for larger messages) to
$\text{LID}_x$ the layer increments~$x$ or resets to~$0$.
Hence, we can easily modify this bfo point-to-point messaging layer to
set~$x$ based on the HyperX quadrants\footnote{As done in PARX routing,
we identify the quadrants by an appropriately predefined LID-to-port assignment
and determine quadrant $q$ via equation $q := \lfloor\frac{\text{LID}}{1000}\rfloor$.}
and rules provided in Table~\ref{tab:quad}. Whenever Table~\ref{tab:quad}
lists two alternatives, we randomly select one.

In addition to the quadrant identification for a give injected message ($s \rightarrow d$),
we need to distinguish message sizes for
the selection of $x$ for the virtual $\text{LID}^d_x$. We performed an initial test
with Intel's Multi-PingPong MPI benchmark~\cite{intel_corporation_intel_2018} and mpiGraph,
to evaluate at which node-count per switch and which
message size we observe latency degradation due to congestion on the single
link between the two involved HyperX switches.
Consequently, we define the threshold to be \unit[512]{bytes} for all PARX-based
evaluations\footnote{This threshold depends on
interconnect technology and \#\{nodes\} attached to each HyperX switch.
Determining an optimal threshold is beyond the scope of this paper.}.
