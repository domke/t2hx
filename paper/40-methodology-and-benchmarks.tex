%\fix{page goal: 5(r);6}
%
\section{Methodology}\label{sec:bm}
% what, why, and how - little intro
While we could deduce the suitability of the HyperX topology for HPC
applications from simple MPI benchmarks, actually testing a broad spectrum of
real scientific/HPC workloads, as listed below, will refine our understanding
of the novel topology. The following inputs are tuned using a smaller node
count such that each test should theoretically take $\approx$\unit[1--5]{min},
regardless of scale.



\subsection{Pure MPI/Network Benchmarks}\label{sec:bm:netw}
We evaluate raw latency/throughput performance for small messages, as found in
HPC codes~\cite{klenk_overview_2017}, and large communication loads, as
required for deep learning applications, with three benchmarks:

$\bullet$~\textit{Intel MPI Benchmarks (IMB)} perform network latency/through-put
measurements of point-to-point and collective MPI operations of varying
message sizes~\cite{intel_corporation_intel_2018}. We focus on IMB's single-mode
\mbox{MPI-1} collectives (non-\textit{v} version), meaning Barrier, Bcast,
$\ldots$, Alltoall.

$\bullet$~\textit{Netgauge's eBB} evaluates the effective
bisection bandwidth~\cite{hoefler_netgauge:_2007} for a given topology, as induced by
the selected routing. We execute 1,000 random bisections with~\unit[1]{MiB} message
size per sample.

$\bullet$~\textit{Baidu's DeepBench Allreduce (AllR)} implements a ring-based
allreduce and evaluates the latency for a variety of messages sizes
(\unit[0--2]{GiB})~\cite{baidu_inc._baidu-allreduce_2017}. We evaluate the
CPU-only version of the code.
%with slightly reduced iteration count, i.e., at most
%1,000 per message size.



\subsection{Scientific Application Benchmarks}\label{sec:bm:apps}
Besides pure MPI benchmarks, we cover a broad set of scientific and HPC
domains by selecting Exascale Computing Project~(ECP)~\cite{exascale_computing_project_ecp_2018}
procurement benchmarks, workloads from
RIKEN R-CCS' Fiber Suits~\cite{riken_aics_fiber_2015}, and two codes
--- known to be highly communication-intensive --- from the
Trinity~\cite{national_energy_research_scientific_computing_center_nersc_nersc-8_2016}
and CORAL~\cite{leininger_coral_2014} set:

$\bullet$~\textit{Algebraic multi-grid (AMG)} solver of the \textit{hypre} library is
a parallel solver for unstructured grids~\cite{park_high-performance_2015}
arising from fluid dynamics problems. We choose \textit{problem~1} with a
$256^3$ cube per process for our tests, which applies a 27-point stencil on a
3-D linear system.

$\bullet$~\textit{Co-designed Molecular Dynamics (CoMD)} serves as the reference
implementation for ExMatEx~\cite{mohd-yusof_co-design_2013} to facilitate
co-design for (and evaluation of) classical molecular dynamics algorithms.
We are using the included weak-scaling example to calculate the inter-atomic
potential for $64^3$ atoms per process.

$\bullet$~\textit{MiniFE (MiFE)} is a reference code of an implicit finite
elements solver~\cite{heroux_improving_2009} for scientific methods resulting
in unstructured 3-dimensional grids. For our study, we follow the recommended
$n_x = n_y = n_z = \sqrt[3]{n_{x_b} * n_{y_b} * n_{z_b} * \#processes}$
weak-scaling formula with $n_{\{{x|y|z\}}_b} = 100$ to define the grid's input
dimensions.

$\bullet$~\textit{SWFFT (FFT)} represents the compute kernel of the HACC cosmology
application~\cite{habib_hacc:_2016} for N-body simulations. The 3-D fast
Fourier transformation of SWFFT emulates one performance-critical part of
HACC's Poisson solver. In our tests, we perform 16 repetitions on a 3-D grid,
which is weak-scaled similar to~\cite[Tab.~4.1]{tomov_evaluation_2018}.

$\bullet$~\textit{Frontflow/violet Cartesian (FFVC)} uses the finite volume method
(FVM)~\cite{ono_ffv-c_nodate} to solve the incompressible Navier-Stokes
equation for thermo-fluid analysis. Here, we calculate the 3-D cavity flow in
a $128^3$ cuboid per process for weak-scaling.

$\bullet$~\textit{many-variable Variational Monte Carlo (mVMC)} method implemented
by this mini-app is used to simulate quantum lattice models for studying the
physics of condensed matter~\cite{misawa_mvmc--open-source_2018}. We use
mVMC's included weak-scaling test (\textit{job\_middle}) without modifications.

$\bullet$~\textit{NTChem (NTCh)} implements a computational kernel of the software
framework (NTChem) for quantum chemistry calculations of molecular electronic
structures, i.e., the solver for the second-order M{\o}ller-Plesset
perturbation theory~\cite{nakajima_ntchem:_2014}. We select the provided
\textit{taxol} test case for our study as strong-scaling input.

$\bullet$~\textit{MIMD Lattice Computation (MILC)} is performing quantum
chromodynamics~(QCD) simulations using the lattice gauge theory on the Lie
group SU(3)~\cite{bernard_scaling_2000}. We use NERSC's Trinity MILC benchmark
code and weak-scale their single node \textit{benchmark\_n8}
input~\cite{national_energy_research_scientific_computing_center_nersc_milc_2018}.

$\bullet$~\textit{LLNL's qb@ll (Qbox)} is an improved Qbox version~\cite{gygi_architecture_2008,lawrence_livermore_national_laboratory_qbox:_2019}
for first-principles molecular dynamics, which uses
Density Functional Theory (DFT) to, for example, calculate the electronic
structure of atoms. We weak-scale the computational load of qb@ll's included \textit{gold}
benchmark, assuming a single-node case of 32 gold atoms.



\subsection{\textit{x}500 Benchmarks}\label{sec:bm:500}
Lastly, we employ three HPC benchmarks\footnote{For both HPL and HPCG, we employ
the highly tuned versions shipped with Intel's Parallel Studio XE (v2018; update 3)
with appropriate~\textit{PNB} parameter for our system.}, which the community uses to
compare the supercomputers in a world-wide ranking:

$\bullet$~\textit{High Performance Linpack (HPL)} is solving a dense system of linear
equations $Ax = b$ to demonstrate the double-precision compute capabilities of
a (HPC) system~\cite{strohmaier_top500_2018}. Our problem size is tuned such that
matrix $A$ occupies $\approx$\unit[1]{GiB} per process.

$\bullet$~\textit{High Performance Conjugate Gradients (HPCG)} is applying a conjugate
gradient solver to a system of linear equations (sparse matrix $A$)~\cite{dongarra_hpcg_2015},
to demonstrate the system's memory and network limits. We choose
192$\times$192$\times$192 as process-local problem domain.

%D == 500 in roman numeral
$\bullet$~\textit{Graph 500 Benchmark (GraD)} measures the data analytics performance
of (super-)computers by evaluating the traversed-edges-per-second metric (TEPS)
for a breadth-first search (BFS) on a large graph~\cite{murphy_introducing_2010}.
Our input graph occupies $\approx$\unit[1]{GiB} per process and we perform
16 BFSs with a highly optimized implementation~\cite{ueno_extreme_2016}.



\subsection{Test Strategies, Environment, and Metrics}\label{sec:bm:conf}
The benchmarks and applications, listed in Section~\ref{sec:bm:netw}--\ref{sec:bm:500},
will be evaluated in two different settings, i.e., in isolation to show system
capability, and in a more realistic multi-application environment.



\subsubsection{Capability Evaluations}\label{sec:bm:conf:capability}
Our exclusive system access allows us to execute capability runs sequentially
and without overlap, while keeping unoccupied nodes idle, which should give
insight into idealized achievable performance on the given topology.
We scale each benchmark starting from a single switch, i.e., seven nodes,
or four nodes if the benchmark requires \#nodes in power-of-two. From there
we double the node count in each step, up to the maximum possible node
count, i.e., $7,14,\ldots,448,672$ or $4,8,\ldots,512$, respectively.
Each combination of: benchmark (and scale), topology,
routing, and placement (cf. Section~\ref{sec:bm:conf:rp}), is executed ten
times to capture the best performance and occurring run-to-run variability.
%. Details about the routing and
%placement is provided in , and extracted
%metrics per benchmark are described in Section~\ref{sec:bm:metric}.



\subsubsection{Capacity Evaluations}\label{sec:bm:conf:capacity}
The multi-application execution model is more common for many
supercomputers~\cite{rodrigo_alvarez_hpc_2015}. These concurrently running jobs
may compete for bandwidth, or create inter-job interference which can
increase message latency~\cite{jain_partitioning_2017}.
We select the following applications: AMG, CoMD, FFVC, Graph500, HPCG, HPL,
MILC, MiniFE, mVMC, NTChem, qb@ll, and SWFFT, plus one IMB Multi-PingPong
(\textit{MuPP}) and one modified IMB Allreduce (\textit{EmDL}) benchmark\footnote{
EmDL is a modified IMB Allreduce to mimic deep learning workloads by alternating between
communication and an \unit[0.1]{s} compute phase simulated via usleep.}.
Each application gets a dedicated set of nodes (32 or 56 nodes,
respectively), and all are submitted simultaneously and are configured
to execute thousands of runs. We let the supercomputer perform this capacity evaluation
for \unit[3]{h}, using 664 of the 672 available compute nodes\footnote{Designed
as qualitative comparison between the two topologies (due to complexity of such
evaluations) to look for potential weaknesses in our HyperX to aid future R\&D.}.
We evaluate the number of runs per application and compare these numbers across
the different topologies, routings, and allocations.



\subsubsection{Routing and Placement}\label{sec:bm:conf:rp}
Both the routing algorithm and the MPI rank placement can
positively (or negatively) influence the communication performance for parallel
applications. Hence, we evaluate five different routing and placement combinations.

We choose the commonly used \textit{ftree} routing~\cite{zahavi_d-mod-k_2010},
as well as SSSP routing~\cite{hoefler_optimized_2009} (both part of IB's OpenSM~\cite{infiniband_trade_association_infinibandtm_2015}), for the Fat-Tree.
The latter theoretically yields increased throughput for faulty Fat-Tree
deployments~\cite{domke_fail--place_2014} such as ours (cf.~Section~\ref{ssec:net:modded}).
Furthermore, we test two MPI rank placements for the Fat-Tree.
The first placement is a \textit{linear} assignment of MPI ranks, meaning rank~1
is placed on compute node~$n_1$, $\ldots$, rank~$i$ on node~$n_i$, and so on, which is a
common resource allocation practise~\cite{yoo_slurm:_2003,schedmd_llc_slurm.conf_2019}.
It reduces small message latency while isolating small-scale runs into subpartitions
of the network to reduce interference~\cite{michelogiannakis_aphid:_2017}.
The \textit{clustered} placement is more realistic and yields from the system's
fragmentation over its operational period~\cite{pollard_evaluation_2018}.
We simulate it by drawing the stride~$\Delta$ from node $n_i$ to the
next node $n_{j}$ from a geometric distribution with an (arbitrarily chosen) 80\%
probability, and hence $j:=i+\Delta$.
%As a result, e.g., our 224-rank application runs will be spread across 20 edge
%switches, while theoretically fitting onto just 16 switches.

We rely on OpenSM's DFSSSP~\cite{domke_deadlock-free_2011} for our HyperX
network due to aforementioned deadlock-issues, see Section~\ref{ssec:parx}.
DFSSSP requires only~3 virtual lanes (VL) to achieve the deadlock-freedom for
our HyperX, which is well within the hardware limit of~8 VLs.
Additionally, we test our novel PARX routing for bottleneck mitigation,
see Section~\ref{ssec:parx3}, as well as the \textit{random} rank placement
introduced in Section~\ref{ssec:placement}. Furthermore, we test \textit{linear} and
\textit{clustered} placements.
Our stored communication profiles are MPI rank-based and placement oblivious,
therefore we require an interface --- similar to
SAR's~\cite{domke_scheduling-aware_2016} --- between the job-submission and OpenSM.
This interface combines the profile(s) and selected node allocation
for one (or more) application into a node/LID-based demand data file, which
PARX uses to re-route the fabric prior to the job start.

In summary, in Section~\ref{sec:eval} we are evaluating the following five
combinations of topology, routing, and resource allocation scheme:
\begin{enumerate}
    \item Fat-Tree with ftree routing and linear placement;
    \item Fat-Tree with SSSP routing and clustered placement;
    \item HyperX with DFSSSP routing and linear placement;
    \item HyperX with DFSSSP routing and random placement; and
    \item HyperX with PARX routing and clustered placement,
\end{enumerate}
and collect performance data from all runs, as indicated below.



\subsubsection{Evaluated Performance Metrics}\label{sec:bm:metric}
%
\setlength\tabcolsep{4.1pt}
\begin{table}[tbp]
    \caption{\label{table:bm} List of applications/benchmarks; Overview of used MPI functions; Collected metrics from each BM; Deployed scaling method (*: instances where we scaled down the input for larger \#nodes to reduce runtime; further details in Sec.~\ref{sec:eval})}
    \centering\scriptsize
    \begin{tabular}{|l|l|l|l|}
        \hline \hC
        \tH{MPI}  & \tH{Used MPI point-to-point \& collective functions}    & \tH{Scaling}  & \tH{Metric} \\ \hline
        IMB       & (All)Reduce Alltoall Barrier Bcast Gather Scatter       & weak          & Latency $t_{min}$ [\unit[]{$\mu$s}] \\ \hline \rC
        eBB       & Isend Irecv Barrier Gather Scatter                      & strong        & Throughput [\unit[]{MiB/s}] \\ \hline
        AllR      & Send Irecv Sendrecv Allgather                           & weak          & Latency $t_{avg}$ [\unit[]{s}] \\ \hline
        \hline \hC
        \tH{Apps} & \tH{Used MPI point-to-point \& collective functions}    & \tH{Scaling}  & \tH{Metric} \\ \hline
        AMG       & (I)Send (I)Recv Allgather(v) Allreduce Bcast\;\;\;etc.  & weak          & Kernel runtime [\unit[]{s}] \\ \hline \rC
        CoMD      & Sendrecv Allreduce Barrier Bcast                        & weak          & Kernel runtime [\unit[]{s}] \\ \hline
        MiFE      & Send Irecv Allgather Allreduce Bcast                    & weak          & Kernel runtime [\unit[]{s}] \\ \hline \rC
        FFT       & (I)Send (I)Recv Allreduce Barrier                       & weak          & Kernel runtime [\unit[]{s}] \\ \hline
        FFVC      & Isend Irecv (All)Reduce Gather                          & weak*         & Kernel runtime [\unit[]{s}] \\ \hline \rC
        mVMC      & (I)Send Sendrecv Recv (All)Reduce Bcast Scatter         & weak          & Kernel runtime [\unit[]{s}] \\ \hline
        NTCh      & Isend Irecv Allreduce Barrier Bcast                     & strong        & Kernel runtime [\unit[]{s}] \\ \hline \rC
        MILC      & Isend Irecv Allreduce Barrier Bcast                     & weak & Kernel runtime [\unit[]{s}] \\ \hline
        Qbox      & (I|R)Send (I)Recv (All)Reduce Alltoallv Bcast\;\;\;etc. & weak* & Kernel runtime [\unit[]{s}] \\ \hline
        \hline \hC
        \tH{\textit{x}500} & \tH{Used MPI point-to-point \& collective functions}    & \tH{Scaling}  & \tH{Metric} \\ \hline
        HPL       & Send (I)Recv                                            & weak*         & \unit[]{Floating-point Op/s} \\ \hline \rC
        HPCG      & Send Irecv Allreduce Alltoall(v) Barrier Bcast          & weak          & \unit[]{Floating-point Op/s} \\ \hline
        GraD      & Isend Irecv Allgather (All)Reduce(\_scatter)\;\;\;etc.  & weak          & \unit[]{Traversed edges/s} \\ \hline \rC
    \end{tabular}
\end{table}
%
We extract the performance data directly from the output of the pure network
benchmarks (cf.~Section~\ref{sec:bm:netw}), i.e., observable communication latency
and message throughput for different MPI operations and messages sizes.
Details about the collected metrics is provided in Table~\ref{table:bm}, which also
summarizes how we scale up each (application-)benchmark. For reference purposes,
we include the executed MPI communication functions used by each benchmark.
The same methodology is used for the \textit{x}500 benchmarks (cf.~Section~\ref{sec:bm:500}),
which directly report either floating-point operations per seconds or median
traversed graph edges per second. In contrast, for the nine HPC workloads
listed in Section~\ref{sec:bm:apps}, we uniformly report the runtime of the
main computational solver/kernel\footnote{We injected timing
instructions into the original code, whenever the proxy-app did not
provide accurate timings of the solver phase or reported alternative
metrics.}.
Focusing on only the solver phase is required, because (for most proxy-apps)
the pre- and/or post-processing phase is disproportionally long which
skews the performance expectations for the real applications.



\subsubsection{Execution Environment}\label{sec:bm:env}
Our HPC system uses the CentOS~7.4 operating system for the compute nodes,
configured for diskless operation, and the OpenFabrics Enterprise Distribution (OFED) stack
(version 4.8) for the IB networks, with one exception: a renewed OpenSM (v3.3.21).
OpenMPI~1.10.7 serves as communication library.
We refrain from adding other (usually needed) HPC software components, such as
batch scheduler or parallel file system\footnote{The network filesystem (NFS)
is able to handle the miniscule I/O of the proxy-apps, and for simplicity/repeatability,
we rely on \texttt{hostfiles} and manual execution of jobs.
While our software stack may appear dated, it roughly
matches the environment when the system was in production.
Please, refer to the AD/AE appendix for further details.}.

All applications and  micro-benchmarks are compiled with the OS-provided Gnu
compilers\footnote{See footnote 11 in Secion~\ref{sec:bm:500} for the two exceptions in the
compiler selection.}. We refrain from modifying the default, application-provided compiler
options, and only additionally optimize for our CPUs by specifying the \texttt{-march=native}
flag.

The general execution model for our benchmarks is MPI+OpenMP with
one MPI rank per compute node and one OpenMP thread per physical CPU core. The
OpenMP threads are pinned and MPI ranks are mapped sequentially onto the nodes
provided by a sorted \texttt{hostfile}. While there are certainly more optimal (in terms of
computational performance) configurations for some of the applications --- with more
ranks per node or thread-to-core under- or oversubscription --- we focus in our study
on the 1-to-1 network comparison for which our rank/thread model should be sufficient.
Each benchmark invocation is allowed a \unit[15]{min} walltime before being
forcefully terminated, to prevent excessive overruns.
