set term pdf color enhanced font "arial,14" size 16,3.4
set output "../figures/mpigraph.pdf"

unset key
set style increment default
set view map scale 1
set style data lines
set xtics border in scale 0,0 mirror norotate  autojustify
set ytics border in scale 0,0 mirror norotate  autojustify
set ztics border in scale 0,0 nomirror norotate  autojustify
unset cbtics
set rtics axis in scale 0,0 nomirror norotate  autojustify
set xlabel "Node ID (sender)" font "arial,16"
set ylabel "Node ID (receiver)" font "arial,16"
set xrange [-0.5:27.5] noreverse nowriteback
set xtics ("1" 0, "4" 3, "8" 7, "12" 11, "16" 15, "20" 19, "24" 23, "28" 27)
set ytics ("1" 0, "4" 3, "8" 7, "12" 11, "16" 15, "20" 19, "24" 23, "28" 27)
set yrange [-0.5:27.5] noreverse nowriteback
set x2range [*:*] noreverse writeback
set y2range [*:*] noreverse writeback
set zrange [*:*] noreverse writeback

set cblabel "Thoughput [in GiByte/s]" font "arial,16"
set cbrange [0:3000] noreverse nowriteback
set rrange [*:*] noreverse writeback
set palette defined ( 0 "#141414", 1 "#ffffff", 2 "#4DAC26" )

set multiplot layout 1, 3
set rmargin 1.5

unset colorbox
set title "Fat-Tree with ftree routing" font "arial,18" offset 0,-0.5
plot "../data/sr/mpigraph.fattree.ftree.linear.data" matrix with image

set title "HyperX with DFSSSP routing" font "arial,18" offset 0,-0.5
plot "../data/sr/mpigraph.hyperx.sssp.linear.data" matrix with image

set colorbox
set label "0 GiB/s" at 28,-1.5
set label "3 GiB/s" at 28,28.5
set title "HyperX with PARX routing" font "arial,18" offset 0,-0.5
plot "../data/sr/mpigraph.hyperx.parx.linear.data" matrix with image

