set term svg enhanced font "arial,9.5"
set output "../figures/imb.Barrier.svg"
set size ratio 0.552

set auto x
set grid y
set notitle

set xlabel "Number of compute nodes" font "arial,11.5" offset 0,+0.2
set xtics rotate 90
set xrange [0:72+16+2]

#set x2label "Relative Performance Gain over FatTree/ftree/linear" font "arial,11.5" offset 0,-0.4
set x2tics rotate 90
set x2range [0:72+16+2]

set encoding utf8
set ylabel "Latency [in μs]" font "arial,11.5" offset +2.0,0
set yrange [0:500]

set y2label ""
set y2range [0:1]

set boxwidth 1.3 absolute
set style fill solid 1.00 border
set datafile missing "-"
set key on outside left center width -4 samplen 1 spacing 3.5 maxcols 1 maxrows 5 \
        title "Combinations\n  (left to right)\n" font "arial,11.5"

ftftli = "#8ECD1D"
ftspcl = "#E2968E"
hxspli = "#556BD8"
hxsprd = "#BA3508"
hxpxcl = "#F9C90D"

plot "<paste ../data/sr/imb.Barrier.fattree.ftree.linear.data ../data/sr/imb.Barrier.fattree.sssp.cluster.data ../data/sr/imb.Barrier.hyperx.sssp.linear.data ../data/sr/imb.Barrier.hyperx.sssp.random.data ../data/sr/imb.Barrier.hyperx.parx.cluster.data" \
        using   ($1+0):5:4:8:7:xtic(2)      with candlesticks lt 1 lw 2 lc rgb ftftli title "Fat-Tree\nftree / linear" whiskerbars, \
     "" using   ($1+0):6:6:6:6 with candlesticks lt -1 lw 2 notitle, \
     "" using  ($9+18):13:12:16:15:xtic(10) with candlesticks lt 2 lw 2 lc rgb ftspcl title "Fat-Tree\nSSSP / clustered" whiskerbars, \
     "" using  ($9+18):14:14:14:14 with candlesticks lt -1 lw 2 notitle, \
     "" using  ($9+18):(NaN):x2tic(sprintf("%+-.2f",($4/$12-1.0))) lt -1 axes x2y2 notitle, \
     "" using ($17+36):21:20:24:23:xtic(18) with candlesticks lt 2 lw 2 lc rgb hxspli title "HyperX\nDFSSSP / linear" whiskerbars, \
     "" using ($17+36):22:22:22:22 with candlesticks lt -1 lw 2 notitle, \
     "" using ($17+36):(NaN):x2tic(sprintf("%+-.2f",($4/$20-1.0))) lt -1 axes x2y2 notitle, \
     "" using ($25+54):29:28:32:31:xtic(26) with candlesticks lt 2 lw 2 lc rgb hxsprd title "HyperX\nDFSSSP / random" whiskerbars, \
     "" using ($25+54):30:30:30:30 with candlesticks lt -1 lw 2 notitle, \
     "" using ($25+54):(NaN):x2tic(sprintf("%+-.2f",($4/$28-1.0))) lt -1 axes x2y2 notitle, \
     "" using ($33+72):37:36:40:39:xtic(34) with candlesticks lt 2 lw 2 lc rgb hxpxcl title "HyperX\nPARX / clustered" whiskerbars, \
     "" using ($33+72):38:38:38:38 with candlesticks lt -1 lw 2 notitle, \
     "" using ($33+72):(NaN):x2tic(sprintf("%+-.2f",($4/$36-1.0))) lt -1 axes x2y2 notitle

