set term pdf color enhanced font "arial,11" size 16,2.5
set output "../figures/baidu.pdf"

unset key
set style increment default
set view map scale 1
set style data lines
set xtics border in scale 0,0 mirror norotate  autojustify
set ytics border in scale 0,0 mirror norotate  autojustify
set ztics border in scale 0,0 nomirror norotate  autojustify
unset cbtics
set rtics axis in scale 0,0 nomirror norotate  autojustify
set xlabel "Node count" font "arial,13"
set ylabel "Array length (containing 4-byte floats)" font "arial,13"
set xrange [-0.5:7.5] noreverse nowriteback
set yrange [-0.5:11.5] noreverse nowriteback
set x2range [*:*] noreverse writeback
set y2range [*:*] noreverse writeback
set zrange [*:*] noreverse writeback

set cblabel "Relative Perforamnce Gain" font "arial,13"
set cbrange [-1:1] noreverse nowriteback
set rrange [*:*] noreverse writeback
set palette defined ( 0 "#DE77AE", 1 "#E9A3C9", 2 "#ffffff", 3 "#7FBC41", 4 "#4DAC26" )

# Parameters to tune
PlotRows = 1
PlotCols = 4
MultiPlotTop = 0.93
MultiPlotLeft = 0.00
MultiPlotBottom = 0.02
# define origin functions
PlotGridX = (1-MultiPlotLeft)/PlotCols
PlotOriginX(n) = ((n-1)%PlotCols)*PlotGridX + MultiPlotLeft
PlotGridY = (MultiPlotTop-MultiPlotBottom)/PlotRows
PlotOriginY(n) = (PlotRows-1-int((n-1)/PlotCols))*PlotGridY + MultiPlotBottom

set multiplot layout 1, 4
set rmargin 0

set size 0.26,1
set origin PlotOriginX(1),PlotOriginY(1)
unset colorbox
set title "Fat-Tree / SSSP / clustered" font "arial,16" offset 0,-0.5
plot "<paste ../data/sr/baidu.fattree.ftree.linear.data ../data/sr/baidu.fattree.sssp.cluster.data | awk '(NR>1){print FNR-2,$0}'" \
        using (int(int($1)/12)):(int($1)%12):($4/$7-1.0):xtic(2):ytic(3) with image, \
     "" using (int(int($1)/12)):(int($1)%12):(sprintf("%+-.2f",($4/$7-1.0))) with labels

set size 0.23,1
MultiPlotLeft = 0.01
set origin PlotOriginX(2),PlotOriginY(2)
unset ytics
unset ylabel
set format y ""
set title "HyperX / DFSSSP / linear" font "arial,16"
plot "<paste ../data/sr/baidu.fattree.ftree.linear.data ../data/sr/baidu.hyperx.sssp.linear.data | awk '(NR>1){print FNR-2,$0}'" \
        using (int(int($1)/12)):(int($1)%12):($4/$7-1.0):xtic(2):ytic(3) with image, \
     "" using (int(int($1)/12)):(int($1)%12):(sprintf("%+-.2f",($4/$7-1.0))) with labels

set size 0.23,1
MultiPlotLeft = -0.01
set origin PlotOriginX(3),PlotOriginY(3)
set title "HyperX / DFSSSP / random" font "arial,16"
plot "<paste ../data/sr/baidu.fattree.ftree.linear.data ../data/sr/baidu.hyperx.sssp.random.data | awk '(NR>1){print FNR-2,$0}'" \
        using (int(int($1)/12)):(int($1)%12):($4/$7-1.0):xtic(2):ytic(3) with image, \
     "" using (int(int($1)/12)):(int($1)%12):(sprintf("%+-.2f",($4/$7-1.0))) with labels

set size 0.26,1
MultiPlotLeft = -0.03
set origin PlotOriginX(4),PlotOriginY(4)
set colorbox
set label "-1.0" at 7.7,-1.1 font "arial,13"
set label "+1.0" at 7.7,12.1 font "arial,13"
set title "HyperX / PARX / clustered" font "arial,16"
plot "<paste ../data/sr/baidu.fattree.ftree.linear.data ../data/sr/baidu.hyperx.parx.cluster.data | awk '(NR>1){print FNR-2,$0}'" \
        using (int(int($1)/12)):(int($1)%12):($4/$7-1.0):xtic(2):ytic(3) with image, \
     "" using (int(int($1)/12)):(int($1)%12):(sprintf("%+-.2f",($4/$7-1.0))) with labels

