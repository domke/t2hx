set term pdf color enhanced font "arial,14" size 16,2.5
set output "../figures/capacity.pdf"

set auto x
set style data histogram
set style fill solid 1.00 border lt -1
set xtic font ",14" rotate by -45 scale 0 left
set xrange [1:16]
set yrange [0:350]
set y2range [0:350]
set ylabel "Number valid runs" offset +1.5,0
set grid noytics

ftftli = "#8ECD1D"
ftspcl = "#E2968E"
hxspli = "#556BD8"
hxsprd = "#BA3508"
hxpxcl = "#F9C90D"

# Parameters to tune
PlotRows = 1
PlotCols = 5
MultiPlotTop = 0.93
MultiPlotLeft = 0.00
MultiPlotBottom = 0.02
# define origin functions
PlotGridX = (1-MultiPlotLeft)/PlotCols
PlotOriginX(n) = ((n-1)%PlotCols)*PlotGridX + MultiPlotLeft
PlotGridY = (MultiPlotTop-MultiPlotBottom)/PlotRows
PlotOriginY(n) = (PlotRows-1-int((n-1)/PlotCols))*PlotGridY + MultiPlotBottom

set multiplot layout 1, 5
set rmargin 0

set ytics nomirror
set title "Fat-Tree / ftree / linear" font "arial,16" offset 0,-0.8
set label "Sum of finished runs:\n                          1202" font "arial,16" at 8,320
set size 0.21,1
set origin PlotOriginX(1),PlotOriginY(1)
plot "../data/tp/amg.fattree.ftree.linear.tp.data"      using    ($1):($3):(.8):xtic("AMG") w boxes notitle lc rgb ftftli, \
                                                     "" using    ($1):($3+20):($3) w labels font "arial,12" notitle, \
     "../data/tp/comd.fattree.ftree.linear.tp.data"     using  ($1+1):($3):(.8):xtic("CoMD") w boxes notitle lc rgb ftftli, \
                                                     "" using  ($1+1):($3+20):($3) w labels font "arial,12" notitle, \
     "../data/tp/ffvc.fattree.ftree.linear.tp.data"     using  ($1+2):($3):(.8):xtic("FFVC") w boxes notitle lc rgb ftftli, \
                                                     "" using  ($1+2):($3+20):($3) w labels font "arial,12" notitle, \
     "../data/tp/graph500.fattree.ftree.linear.tp.data" using  ($1+3):($3):(.8):xtic("GraD") w boxes notitle lc rgb ftftli, \
                                                     "" using  ($1+3):($3+20):($3) w labels font "arial,12" notitle, \
     "../data/tp/hpcg.fattree.ftree.linear.tp.data"     using  ($1+4):($3):(.8):xtic("HPCG") w boxes notitle lc rgb ftftli, \
                                                     "" using  ($1+4):($3+20):($3) w labels font "arial,12" notitle, \
     "../data/tp/hpl.fattree.ftree.linear.tp.data"      using  ($1+5):($3):(.8):xtic("HPL") w boxes notitle lc rgb ftftli, \
                                                     "" using  ($1+5):($3+20):($3) w labels font "arial,12" notitle, \
     "../data/tp/milc.fattree.ftree.linear.tp.data"     using  ($1+6):($3):(.8):xtic("MILC") w boxes notitle lc rgb ftftli, \
                                                     "" using  ($1+6):($3+20):($3) w labels font "arial,12" notitle, \
     "../data/tp/minife.fattree.ftree.linear.tp.data"   using  ($1+7):($3):(.8):xtic("MiFE") w boxes notitle lc rgb ftftli, \
                                                     "" using  ($1+7):($3+20):($3) w labels font "arial,12" notitle, \
     "../data/tp/mvmc.fattree.ftree.linear.tp.data"     using  ($1+8):($3):(.8):xtic("mVMC") w boxes notitle lc rgb ftftli, \
                                                     "" using  ($1+8):($3+20):($3) w labels font "arial,12" notitle, \
     "../data/tp/ntchem.fattree.ftree.linear.tp.data"   using  ($1+9):($3):(.8):xtic("NTCh") w boxes notitle lc rgb ftftli, \
                                                     "" using  ($1+9):($3+20):($3) w labels font "arial,12" notitle, \
     "../data/tp/qball.fattree.ftree.linear.tp.data"    using ($1+10):($3):(.8):xtic("Qbox") w boxes notitle lc rgb ftftli, \
                                                     "" using ($1+10):($3+20):($3) w labels font "arial,12" notitle, \
     "../data/tp/swfft.fattree.ftree.linear.tp.data"    using ($1+11):($3):(.8):xtic("FFT") w boxes notitle lc rgb ftftli, \
                                                     "" using ($1+11):($3+20):($3) w labels font "arial,12" notitle, \
     "../data/tp/multip2p.fattree.ftree.linear.tp.data" using ($1+12):($3):(.8):xtic("MuPP") w boxes notitle lc rgb ftftli, \
                                                     "" using ($1+12):($3+20):($3) w labels font "arial,12" notitle, \
     "../data/tp/simdl.fattree.ftree.linear.tp.data"    using ($1+13):($3):(.8):xtic("EmDL") w boxes notitle lc rgb ftftli, \
                                                     "" using ($1+13):($3+20):($3) w labels font "arial,12" notitle

unset ytics
unset ylabel
set title "Fat-Tree / SSSP / clustered" font "arial,16"
unset label
set label "Sum of finished runs:\n                           980" font "arial,16" at 8,320
set size 0.19,1
MultiPlotLeft = 0.01
set origin PlotOriginX(2),PlotOriginY(2)
plot "../data/tp/amg.fattree.sssp.cluster.tp.data"      using    ($1):($3):(.8):xtic("AMG") w boxes notitle lc rgb ftspcl, \
                                                     "" using    ($1):($3+20):($3) w labels font "arial,12" notitle, \
     "../data/tp/comd.fattree.sssp.cluster.tp.data"     using  ($1+1):($3):(.8):xtic("CoMD") w boxes notitle lc rgb ftspcl, \
                                                     "" using  ($1+1):($3+20):($3) w labels font "arial,12" notitle, \
     "../data/tp/ffvc.fattree.sssp.cluster.tp.data"     using  ($1+2):($3):(.8):xtic("FFVC") w boxes notitle lc rgb ftspcl, \
                                                     "" using  ($1+2):($3+20):($3) w labels font "arial,12" notitle, \
     "../data/tp/graph500.fattree.sssp.cluster.tp.data" using  ($1+3):($3):(.8):xtic("GraD") w boxes notitle lc rgb ftspcl, \
                                                     "" using  ($1+3):($3+20):($3) w labels font "arial,12" notitle, \
     "../data/tp/hpcg.fattree.sssp.cluster.tp.data"     using  ($1+4):($3):(.8):xtic("HPCG") w boxes notitle lc rgb ftspcl, \
                                                     "" using  ($1+4):($3+20):($3) w labels font "arial,12" notitle, \
     "../data/tp/hpl.fattree.sssp.cluster.tp.data"      using  ($1+5):($3):(.8):xtic("HPL") w boxes notitle lc rgb ftspcl, \
                                                     "" using  ($1+5):($3+20):($3) w labels font "arial,12" notitle, \
     "../data/tp/milc.fattree.sssp.cluster.tp.data"     using  ($1+6):($3):(.8):xtic("MILC") w boxes notitle lc rgb ftspcl, \
                                                     "" using  ($1+6):($3+20):($3) w labels font "arial,12" notitle, \
     "../data/tp/minife.fattree.sssp.cluster.tp.data"   using  ($1+7):($3):(.8):xtic("MiFE") w boxes notitle lc rgb ftspcl, \
                                                     "" using  ($1+7):($3+20):($3) w labels font "arial,12" notitle, \
     "../data/tp/mvmc.fattree.sssp.cluster.tp.data"     using  ($1+8):($3):(.8):xtic("mVMC") w boxes notitle lc rgb ftspcl, \
                                                     "" using  ($1+8):($3+20):($3) w labels font "arial,12" notitle, \
     "../data/tp/ntchem.fattree.sssp.cluster.tp.data"   using  ($1+9):($3):(.8):xtic("NTCh") w boxes notitle lc rgb ftspcl, \
                                                     "" using  ($1+9):($3+20):($3) w labels font "arial,12" notitle, \
     "../data/tp/qball.fattree.sssp.cluster.tp.data"    using ($1+10):($3):(.8):xtic("Qbox") w boxes notitle lc rgb ftspcl, \
                                                     "" using ($1+10):($3+20):($3) w labels font "arial,12" notitle, \
     "../data/tp/swfft.fattree.sssp.cluster.tp.data"    using ($1+11):($3):(.8):xtic("FFT") w boxes notitle lc rgb ftspcl, \
                                                     "" using ($1+11):($3+20):($3) w labels font "arial,12" notitle, \
     "../data/tp/multip2p.fattree.sssp.cluster.tp.data" using ($1+12):($3):(.8):xtic("MuPP") w boxes notitle lc rgb ftspcl, \
                                                     "" using ($1+12):($3+20):($3) w labels font "arial,12" notitle, \
     "../data/tp/simdl.fattree.sssp.cluster.tp.data"    using ($1+13):($3):(.8):xtic("EmDL") w boxes notitle lc rgb ftspcl, \
                                                     "" using ($1+13):($3+20):($3) w labels font "arial,12" notitle

set title "HyperX / DFSSSP / linear" font "arial,16"
unset label
set label "Sum of finished runs:\n                          1355" font "arial,16" at 8,320
set size 0.19,1
MultiPlotLeft = 0.00
set origin PlotOriginX(3),PlotOriginY(3)
plot "../data/tp/amg.hyperx.sssp.linear.tp.data"      using    ($1):($3):(.8):xtic("AMG") w boxes notitle lc rgb hxspli, \
                                                   "" using    ($1):($3+20):($3) w labels font "arial,12" notitle, \
     "../data/tp/comd.hyperx.sssp.linear.tp.data"     using  ($1+1):($3):(.8):xtic("CoMD") w boxes notitle lc rgb hxspli, \
                                                   "" using  ($1+1):($3+20):($3) w labels font "arial,12" notitle, \
     "../data/tp/ffvc.hyperx.sssp.linear.tp.data"     using  ($1+2):($3):(.8):xtic("FFVC") w boxes notitle lc rgb hxspli, \
                                                   "" using  ($1+2):($3+20):($3) w labels font "arial,12" notitle, \
     "../data/tp/graph500.hyperx.sssp.linear.tp.data" using  ($1+3):($3):(.8):xtic("GraD") w boxes notitle lc rgb hxspli, \
                                                   "" using  ($1+3):($3+20):($3) w labels font "arial,12" notitle, \
     "../data/tp/hpcg.hyperx.sssp.linear.tp.data"     using  ($1+4):($3):(.8):xtic("HPCG") w boxes notitle lc rgb hxspli, \
                                                   "" using  ($1+4):($3+20):($3) w labels font "arial,12" notitle, \
     "../data/tp/hpl.hyperx.sssp.linear.tp.data"      using  ($1+5):($3):(.8):xtic("HPL") w boxes notitle lc rgb hxspli, \
                                                   "" using  ($1+5):($3+20):($3) w labels font "arial,12" notitle, \
     "../data/tp/milc.hyperx.sssp.linear.tp.data"     using  ($1+6):($3):(.8):xtic("MILC") w boxes notitle lc rgb hxspli, \
                                                   "" using  ($1+6):($3+20):($3) w labels font "arial,12" notitle, \
     "../data/tp/minife.hyperx.sssp.linear.tp.data"   using  ($1+7):($3):(.8):xtic("MiFE") w boxes notitle lc rgb hxspli, \
                                                   "" using  ($1+7):($3+20):($3) w labels font "arial,12" notitle, \
     "../data/tp/mvmc.hyperx.sssp.linear.tp.data"     using  ($1+8):($3):(.8):xtic("mVMC") w boxes notitle lc rgb hxspli, \
                                                   "" using  ($1+8):($3+20):($3) w labels font "arial,12" notitle, \
     "../data/tp/ntchem.hyperx.sssp.linear.tp.data"   using  ($1+9):($3):(.8):xtic("NTCh") w boxes notitle lc rgb hxspli, \
                                                   "" using  ($1+9):($3+20):($3) w labels font "arial,12" notitle, \
     "../data/tp/qball.hyperx.sssp.linear.tp.data"    using ($1+10):($3):(.8):xtic("Qbox") w boxes notitle lc rgb hxspli, \
                                                   "" using ($1+10):($3+20):($3) w labels font "arial,12" notitle, \
     "../data/tp/swfft.hyperx.sssp.linear.tp.data"    using ($1+11):($3):(.8):xtic("FFT") w boxes notitle lc rgb hxspli, \
                                                   "" using ($1+11):($3+20):($3) w labels font "arial,12" notitle, \
     "../data/tp/multip2p.hyperx.sssp.linear.tp.data" using ($1+12):($3):(.8):xtic("MuPP") w boxes notitle lc rgb hxspli, \
                                                   "" using ($1+12):($3+20):($3) w labels font "arial,12" notitle, \
     "../data/tp/simdl.hyperx.sssp.linear.tp.data"    using ($1+13):($3):(.8):xtic("EmDL") w boxes notitle lc rgb hxspli, \
                                                   "" using ($1+13):($3+20):($3) w labels font "arial,12" notitle

set title "HyperX / DFSSSP / random" font "arial,16"
unset label
set label "Sum of finished runs:\n                          1017" font "arial,16" at 8,320
set size 0.19,1
MultiPlotLeft = -0.01
set origin PlotOriginX(4),PlotOriginY(4)
plot "../data/tp/amg.hyperx.sssp.random.tp.data"      using    ($1):($3):(.8):xtic("AMG") w boxes notitle lc rgb hxsprd, \
                                                   "" using    ($1):($3+20):($3) w labels font "arial,12" notitle, \
     "../data/tp/comd.hyperx.sssp.random.tp.data"     using  ($1+1):($3):(.8):xtic("CoMD") w boxes notitle lc rgb hxsprd, \
                                                   "" using  ($1+1):($3+20):($3) w labels font "arial,12" notitle, \
     "../data/tp/ffvc.hyperx.sssp.random.tp.data"     using  ($1+2):($3):(.8):xtic("FFVC") w boxes notitle lc rgb hxsprd, \
                                                   "" using  ($1+2):($3+20):($3) w labels font "arial,12" notitle, \
     "../data/tp/graph500.hyperx.sssp.random.tp.data" using  ($1+3):($3):(.8):xtic("GraD") w boxes notitle lc rgb hxsprd, \
                                                   "" using  ($1+3):($3+20):($3) w labels font "arial,12" notitle, \
     "../data/tp/hpcg.hyperx.sssp.random.tp.data"     using  ($1+4):($3):(.8):xtic("HPCG") w boxes notitle lc rgb hxsprd, \
                                                   "" using  ($1+4):($3+20):($3) w labels font "arial,12" notitle, \
     "../data/tp/hpl.hyperx.sssp.random.tp.data"      using  ($1+5):($3):(.8):xtic("HPL") w boxes notitle lc rgb hxsprd, \
                                                   "" using  ($1+5):($3+20):($3) w labels font "arial,12" notitle, \
     "../data/tp/milc.hyperx.sssp.random.tp.data"     using  ($1+6):($3):(.8):xtic("MILC") w boxes notitle lc rgb hxsprd, \
                                                   "" using  ($1+6):($3+20):($3) w labels font "arial,12" notitle, \
     "../data/tp/minife.hyperx.sssp.random.tp.data"   using  ($1+7):($3):(.8):xtic("MiFE") w boxes notitle lc rgb hxsprd, \
                                                   "" using  ($1+7):($3+20):($3) w labels font "arial,12" notitle, \
     "../data/tp/mvmc.hyperx.sssp.random.tp.data"     using  ($1+8):($3):(.8):xtic("mVMC") w boxes notitle lc rgb hxsprd, \
                                                   "" using  ($1+8):($3+20):($3) w labels font "arial,12" notitle, \
     "../data/tp/ntchem.hyperx.sssp.random.tp.data"   using  ($1+9):($3):(.8):xtic("NTCh") w boxes notitle lc rgb hxsprd, \
                                                   "" using  ($1+9):($3+20):($3) w labels font "arial,12" notitle, \
     "../data/tp/qball.hyperx.sssp.random.tp.data"    using ($1+10):($3):(.8):xtic("Qbox") w boxes notitle lc rgb hxsprd, \
                                                   "" using ($1+10):($3+20):($3) w labels font "arial,12" notitle, \
     "../data/tp/swfft.hyperx.sssp.random.tp.data"    using ($1+11):($3):(.8):xtic("FFT") w boxes notitle lc rgb hxsprd, \
                                                   "" using ($1+11):($3+20):($3) w labels font "arial,12" notitle, \
     "../data/tp/multip2p.hyperx.sssp.random.tp.data" using ($1+12):($3):(.8):xtic("MuPP") w boxes notitle lc rgb hxsprd, \
                                                   "" using ($1+12):($3+20):($3) w labels font "arial,12" notitle, \
     "../data/tp/simdl.hyperx.sssp.random.tp.data"    using ($1+13):($3):(.8):xtic("EmDL") w boxes notitle lc rgb hxsprd, \
                                                   "" using ($1+13):($3+20):($3) w labels font "arial,12" notitle

set rmargin 7

set y2tics
set y2label "Number valid runs" offset -1.2,0
set title "HyperX / PARX / clustered" font "arial,16"
unset label
set label "Sum of finished runs:\n                          1233" font "arial,16" at 8,320
set size 0.22,1
MultiPlotLeft = -0.02
set origin PlotOriginX(5),PlotOriginY(5)
plot "../data/tp/amg.hyperx.parx.cluster.tp.data"      using    ($1):($3):(.8):xtic("AMG") w boxes notitle lc rgb hxpxcl, \
                                                    "" using    ($1):($3+20):($3) w labels font "arial,12" notitle, \
     "../data/tp/comd.hyperx.parx.cluster.tp.data"     using  ($1+1):($3):(.8):xtic("CoMD") w boxes notitle lc rgb hxpxcl, \
                                                    "" using  ($1+1):($3+20):($3) w labels font "arial,12" notitle, \
     "../data/tp/ffvc.hyperx.parx.cluster.tp.data"     using  ($1+2):($3):(.8):xtic("FFVC") w boxes notitle lc rgb hxpxcl, \
                                                    "" using  ($1+2):($3+20):($3) w labels font "arial,12" notitle, \
     "../data/tp/graph500.hyperx.parx.cluster.tp.data" using  ($1+3):($3):(.8):xtic("GraD") w boxes notitle lc rgb hxpxcl, \
                                                    "" using  ($1+3):($3+20):($3) w labels font "arial,12" notitle, \
     "../data/tp/hpcg.hyperx.parx.cluster.tp.data"     using  ($1+4):($3):(.8):xtic("HPCG") w boxes notitle lc rgb hxpxcl, \
                                                    "" using  ($1+4):($3+20):($3) w labels font "arial,12" notitle, \
     "../data/tp/hpl.hyperx.parx.cluster.tp.data"      using  ($1+5):($3):(.8):xtic("HPL") w boxes notitle lc rgb hxpxcl, \
                                                    "" using  ($1+5):($3+20):($3) w labels font "arial,12" notitle, \
     "../data/tp/milc.hyperx.parx.cluster.tp.data"     using  ($1+6):($3):(.8):xtic("MILC") w boxes notitle lc rgb hxpxcl, \
                                                    "" using  ($1+6):($3+20):($3) w labels font "arial,12" notitle, \
     "../data/tp/minife.hyperx.parx.cluster.tp.data"   using  ($1+7):($3):(.8):xtic("MiFE") w boxes notitle lc rgb hxpxcl, \
                                                    "" using  ($1+7):($3+20):($3) w labels font "arial,12" notitle, \
     "../data/tp/mvmc.hyperx.parx.cluster.tp.data"     using  ($1+8):($3):(.8):xtic("mVMC") w boxes notitle lc rgb hxpxcl, \
                                                    "" using  ($1+8):($3+20):($3) w labels font "arial,12" notitle, \
     "../data/tp/ntchem.hyperx.parx.cluster.tp.data"   using  ($1+9):($3):(.8):xtic("NTCh") w boxes notitle lc rgb hxpxcl, \
                                                    "" using  ($1+9):($3+20):($3) w labels font "arial,12" notitle, \
     "../data/tp/qball.hyperx.parx.cluster.tp.data"    using ($1+10):($3):(.8):xtic("Qbox") w boxes notitle lc rgb hxpxcl, \
                                                    "" using ($1+10):($3+20):($3) w labels font "arial,12" notitle, \
     "../data/tp/swfft.hyperx.parx.cluster.tp.data"    using ($1+11):($3):(.8):xtic("FFT") w boxes notitle lc rgb hxpxcl, \
                                                    "" using ($1+11):($3+20):($3) w labels font "arial,12" notitle, \
     "../data/tp/multip2p.hyperx.parx.cluster.tp.data" using ($1+12):($3):(.8):xtic("MuPP") w boxes notitle lc rgb hxpxcl, \
                                                    "" using ($1+12):($3+20):($3) w labels font "arial,12" notitle, \
     "../data/tp/simdl.hyperx.parx.cluster.tp.data"    using ($1+13):($3):(.8):xtic("EmDL") w boxes notitle lc rgb hxpxcl, \
                                                    "" using ($1+13):($3+20):($3) w labels font "arial,12" notitle

