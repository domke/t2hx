set term svg enhanced font "arial,12"
set output "../figures/amg.svg"
#set term pdf color enhanced font "arial,12"
#set output "../figures/amg.pdf"
set size ratio 0.73

set auto x
set grid y
set notitle

set xlabel "Number of compute nodes" font "arial,14" offset 0,+0.2
set xtics rotate 90
set xrange [0:72+16+2]

#set x2label "Relative Performance Gain over FatTree/ftree/linear" font "arial,14" offset 0,-0.4
set x2tics rotate 90
set x2range [0:72+16+2]

set ylabel "Kernel Runtime [in s]" font "arial,14" offset +1.5,0
set yrange [0:950]
set ytics 0,150,900

set y2label ""
set y2range [0:1]

set boxwidth 1.3 absolute
set style fill solid 1.00 border
set datafile missing "-"
set key off

ftftli = "#8ECD1D"
ftspcl = "#E2968E"
hxspli = "#556BD8"
hxsprd = "#BA3508"
hxpxcl = "#F9C90D"
getCol(x) = 1

plot "<paste ../data/sr/amg.fattree.ftree.linear.data ../data/sr/amg.fattree.sssp.cluster.data ../data/sr/amg.hyperx.sssp.linear.data ../data/sr/amg.hyperx.sssp.random.data ../data/sr/amg.hyperx.parx.cluster.data" \
        using   ($1+0):5:4:8:7:xtic(2)      with candlesticks lt 1 lw 2 lc rgb ftftli title "FT/ftree/lin" whiskerbars, \
     "" using   ($1+0):6:6:6:6 with candlesticks lt -1 lw 2 notitle, \
     "" using  ($9+18):13:12:16:15:xtic(10) with candlesticks lt 2 lw 2 lc rgb ftspcl title "FT/sssp/clu" whiskerbars, \
     "" using  ($9+18):14:14:14:14 with candlesticks lt -1 lw 2 notitle, \
     "" using  ($9+18):(NaN):x2tic(sprintf("%+-.2f",($4/$12-1.0))) lt -1 axes x2y2 notitle, \
     "" using ($17+36):21:20:24:23:xtic(18) with candlesticks lt 2 lw 2 lc rgb hxspli title "HX/sssp/lin" whiskerbars, \
     "" using ($17+36):22:22:22:22 with candlesticks lt -1 lw 2 notitle, \
     "" using ($17+36):(NaN):x2tic(sprintf("%+-.2f",($4/$20-1.0))) lt -1 axes x2y2 notitle, \
     "" using ($25+54):29:28:32:31:xtic(26) with candlesticks lt 2 lw 2 lc rgb hxsprd title "HX/sssp/rnd" whiskerbars, \
     "" using ($25+54):30:30:30:30 with candlesticks lt -1 lw 2 notitle, \
     "" using ($25+54):(NaN):x2tic(sprintf("%+-.2f",($4/$28-1.0))) lt -1 axes x2y2 notitle, \
     "" using ($33+72):37:36:40:39:xtic(34) with candlesticks lt 2 lw 2 lc rgb hxpxcl title "HX/parx/clu" whiskerbars, \
     "" using ($33+72):38:38:38:38 with candlesticks lt -1 lw 2 notitle, \
     "" using ($33+72):(NaN):x2tic(sprintf("%+-.2f",($4/$36-1.0))) lt -1 axes x2y2 notitle

#as fault box_min whisker_min whisker_high box_high box_avg
#plot '../data/amg.fattree.ftree.linear.data' \
#	using  ($1+0):5:4:8:7:xtic(2) with candlesticks lt 1 lw 2 lc rgb nue1 title 'FT/ftree/lin' whiskerbars, \
#     '' using  ($1+0):6:6:6:6 with candlesticks lt -1 lw 2 notitle, \
#     '../data/amg.fattree.sssp.cluster.data' \
#	using ($1+18):5:4:8:7:xtic(2) with candlesticks lt 2 lw 2 lc rgb nue2 title 'FT/sssp/clu' whiskerbars, \
#     '' using ($1+18):6:6:6:6 with candlesticks lt -1 lw 2 notitle, \
#     '../data/amg.hyperx.sssp.linear.data' \
#	using ($1+36):5:4:8:7:xtic(2) with candlesticks lt 2 lw 2 lc rgb nue3 title 'HX/sssp/lin' whiskerbars, \
#     '' using ($1+36):6:6:6:6 with candlesticks lt -1 lw 2 notitle, \
#     '../data/amg.hyperx.sssp.random.data' \
#	using ($1+54):5:4:8:7:xtic(2) with candlesticks lt 2 lw 2 lc rgb nue4 title 'HX/sssp/rnd' whiskerbars, \
#     '' using ($1+54):6:6:6:6 with candlesticks lt -1 lw 2 notitle, \
#     '../data/amg.hyperx.parx.cluster.data' \
#	using ($1+72):5:4:8:7:xtic(2) with candlesticks lt 2 lw 2 lc rgb nue5 title 'HX/parx/clu' whiskerbars, \
#     '' using ($1+72):6:6:6:6 with candlesticks lt -1 lw 2 notitle, \
