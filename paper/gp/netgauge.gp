set term svg enhanced font "arial,7"
set output "../figures/netgauge.svg"
set size ratio 0.3

set auto x
set grid y
set notitle

set xlabel "Number of compute nodes" font "arial,9" offset 0,+0.2
set xtics rotate 90
set xrange [0:128+30+2]

#set x2label "Relative Performance Gain over FatTree/ftree/linear" font "arial,9" offset 0,-0.4
set x2tics rotate 90
set x2range [0:128+30+2]

set ylabel "Eff. Bisection Bandwidth [in GiB/s]" font "arial,9" offset +1.0,0
set yrange [0:4]
set ytics 0,1,4

set y2label ""
set y2range [0:1]

set boxwidth 1.1 absolute
set style fill solid 1.00 border
set datafile missing "-"
set key off

ftftli = "#8ECD1D"
ftspcl = "#E2968E"
hxspli = "#556BD8"
hxsprd = "#BA3508"
hxpxcl = "#F9C90D"

s(x) = x/1024.0

plot "<paste ../data/sr/netgauge.fattree.ftree.linear.data ../data/sr/netgauge.fattree.sssp.cluster.data ../data/sr/netgauge.hyperx.sssp.linear.data ../data/sr/netgauge.hyperx.sssp.random.data ../data/sr/netgauge.hyperx.parx.cluster.data" \
        using   ($1+0):(s($5)):(s($4)):(s($8)):(s($7)):xtic(2)      with candlesticks lt 1 lw 2 lc rgb ftftli title "FT/ftree/lin" whiskerbars, \
     "" using   ($1+0):(s($6)):(s($6)):(s($6)):(s($6)) with candlesticks lt -1 lw 2 notitle, \
     "" using  ($9+32):(s($13)):(s($12)):(s($16)):(s($15)):xtic(10) with candlesticks lt 2 lw 2 lc rgb ftspcl title "FT/sssp/clu" whiskerbars, \
     "" using  ($9+32):(s($14)):(s($14)):(s($14)):(s($14)) with candlesticks lt -1 lw 2 notitle, \
     "" using  ($9+32):(NaN):x2tic(sprintf("%+-.2f",($16/$8-1.0))) lt -1 axes x2y2 notitle, \
     "" using ($17+64):(s($21)):(s($20)):(s($24)):(s($23)):xtic(18) with candlesticks lt 2 lw 2 lc rgb hxspli title "HX/sssp/lin" whiskerbars, \
     "" using ($17+64):(s($22)):(s($22)):(s($22)):(s($22)) with candlesticks lt -1 lw 2 notitle, \
     "" using ($17+64):(NaN):x2tic(sprintf("%+-.2f",($24/$8-1.0))) lt -1 axes x2y2 notitle, \
     "" using ($25+96):(s($29)):(s($28)):(s($32)):(s($31)):xtic(26) with candlesticks lt 2 lw 2 lc rgb hxsprd title "HX/sssp/rnd" whiskerbars, \
     "" using ($25+96):(s($30)):(s($30)):(s($30)):(s($30)) with candlesticks lt -1 lw 2 notitle, \
     "" using ($25+96):(NaN):x2tic(sprintf("%+-.2f",($32/$8-1.0))) lt -1 axes x2y2 notitle, \
     "" using ($33+128):(s($37)):(s($36)):(s($40)):(s($39)):xtic(34) with candlesticks lt 2 lw 2 lc rgb hxpxcl title "HX/parx/clu" whiskerbars, \
     "" using ($33+128):(s($38)):(s($38)):(s($38)):(s($38)) with candlesticks lt -1 lw 2 notitle, \
     "" using ($33+128):(NaN):x2tic(sprintf("%+-.2f",($40/$8-1.0))) lt -1 axes x2y2 notitle

