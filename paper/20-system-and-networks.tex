%\fix{page goal: 2(r);3}
%
\section{System Architecture \& Topologies}\label{sec:networks}
Here, we briefly introduce the state-of-the-art Fat-Tree, used by
many HPC systems, and the recently proposed HyperX alternative.
Furthermore, we show how we re-wired a decommissioned system
to perform a 1-to-1 comparison between these two topologies.



\subsection{$k$-ary $n$-tree Topology}\label{ssec:net:karyn}
The $k$-ary $n$-tree topology~\cite{petrini_k-ary_1997}, also known as Folded Clos
or Fat-Tree, has become the
predominant topology for large scale computing systems. This topology derives from
multistage Clos networks~\cite{clos_study_1953}, and has risen to
dominance mainly because of its ability to support efficient deterministic routing
wherein packets follow the same path every time from source to destination.
In order to support full throughput for uniform random traffic, a Folded Clos
must be provisioned with~100\% bisection bandwidth. While this yields a system
with predictable behavior, its cost is high and often prohibitive due
to the indirect nature of this topology and increase in required levels~$n$
for larger supercomputers. Figure~\ref{sfig:karyn} shows a small 4-ary~2-tree.
To reduce cost, large systems can be deployed with less bisection bandwidth by
oversubscribing the lowest level of the tree~\cite{leon_characterizing_2016}.
For example, a 2-to-1 oversubscription cuts the network cost by more than~50\%
however reduces the uniform random throughput to~50\%. The benefit to the
Folded Clos is that all admissible (non-incast) traffic patterns theoretically
yield the same throughput~\cite{petrini_k-ary_1997}, assuming the routing is
congestion-free~\cite{hoefler_multistage_2008}.



\subsection{HyperX Topology}\label{ssec:net:hx}
Ahn et al.~introduced the HyperX topology~\cite{ahn_hyperx:_2009} as a generalization
to all flat integer lattice networks where dimensions are fully connected, e.g.,
HyperCube, Flattened Butterfly, see~\cite[Sec.~3.3]{abts_high_2011} for details.
Since the HyperX is a superset of these topologies,
the remainder of this paper will use the term HyperX, as the methodologies presented
herein apply to all HyperX configurations.
Figure~\ref{sfig:hyperx} depicts a full-bisection bandwidth, 2-dimensional HyperX.
The HyperX methodology is designed as a low-diameter, direct network to fit
with high-radix routers. One of the primary benefits
of HyperX is that it can fit to any physical packaging scheme as each dimension
can be individually augmented to fit within a physical packaging domain, e.g.,
a chassis, a rack, a row of racks, etc. A HyperX network is designed for uniform
random traffic being the average case. A HyperX network designed with only~50\%
bisection bandwidth can still provide~100\% throughput for uniform random, assuming
appropriate message routing, and hence drastically reduce overall network costs.
\added{This advantage in cost structure was shown by Ahn et al., as well as in subsequent
studies~\cite{mudigonda_taming_2011, azizi_topological_2013, kathareios_cost-effective_2015}.}
However, unlike the Folded Clos, even though~100\% throughput is achievable with uniform
random traffic on a HyperX, the worst case traffic will only achieve~50\%
throughput~\cite{dally_principles_2003}.
% lengthy explanation by Nic (in case a rewviewer asks):
%The basic idea is that in most networks (i.e., everything except Clos
%networks), uniform random traffic only crosses the bisection 50% of the
%time, thus you only need 50% bisection bandwidth to support 100% throughput
%for uniform random traffic. In theory, with perfect routing, the worst case
%traffic pattern should still be able to achieve 50% throughput because it
%is getting bottlenecked in the bisection. However, to achieve 50%
%throughput for worse case traffic relies on VERY intelligent adaptive
%routing if you also want to achieve 100% for the uniform random case.
%
%Fat-Tree, Folded-Clos, k-ary n-tree is a weird topology. It requires 100%
%bisection bandwidth to achieve 100% throughput for uniform random. The plus
%side is that it will yield 100% throughput for all traffic patterns. There
%is no bad case traffic, in theory.



\subsection{HPC System Layout and Modifications}\label{ssec:net:modded}
Currently, no large-scale deployment of the HyperX topology exists, hence we have
to build one ourselves. Fortunately,
\iftoggle{doubleblind}{\textbf{<site redacted during review>} decommissioned their supercomputer}
{the Tokyo Institute of Technology decommissioned their TSUBAME2 supercomputer}
recently, and allowed us to modify its topology to construct a HyperX prototype.
The system's original configuration, before being shut down, consisted of over
1,500 compute and auxiliary nodes --- totalling a theoretical peak performance of
$\approx$\unit[6]{Pflop/s} --- interconnected by two QDR InfiniBand-based (IB)
full-bisection bandwidth Fat-Trees\iftoggle{doubleblind}{}{~\cite{gsic_tokyo_institute_of_technology_tsubame2.5_2013}}.
Each compute node is equipped with two hexa-core Intel CPUs (Westmere~EP
generation) and at least \unit[4]{GiB} RAM per core, but primarily gains the
compute capability from GPU acceleration\footnote{
Facility power constraints, for running old and new HPC system
simultaneously, prohibited us from using the GPUs, which should
not impede our network benchmarks.}.

Both IB networks are constructed from 36-port Voltaire~4036 edge switches
(two per rack per network plane), a total of~12 Voltaire Grid Director~4700 switches,
and thousands of QDR active optical cables~(AOC) between the edge and director
switches. The original full-bisectional 18-ary~3-trees were undersubscribed,
i.e., each edge switch hosts only 15 compute nodes (instead of 18).
Tearing down one of the two network planes and re-wire it as HyperX topology,
as indicated in Figure~\ref{sfig:t2hx}, theoretically allows for
an accurate and fair comparison of Fat-Tree vs.~HyperX, since both physical
IB network cards are attached to CPU0.
Unfortunately, our InfiniBand hardware will be a weak point of our
HyperX topology.

\added{The HyperX was intended to be used and initially
proposed together with the Dimensionally-Adaptive, Load-balanced routing
algorithm (DAL). Our dated QDR-based InfiniBand hardware only supports
flow-oblivious, static routing --- usually along shortest paths --- and
entirely lacks adaptive routing (AR) capabilities,
see~\cite[Sec.~18.2.4.3]{infiniband_trade_association_infinibandtm_2015}.
Hence, to alleviate the lack of AR and improve throughput especially for
adversarial traffic, see previous Sections~\ref{sec:intro}
and~\ref{ssec:net:hx}, we develop two mitigation strategies in
Section~\ref{sec:mitigation}.}

In a multi-months effort, we constructed the largest possible HPC system ---
given our hardware constraints --- with HyperX network resulting
in a 12x8 2D topology with~7 nodes per switch, and slightly over half-bisection
bandwidth, i.e., 57.1\% to be precise.
The 672 compute nodes and 96 IB edge switches, composing our HyperX
network, are distributed over 24 compute racks (plus one auxiliary rack),
giving our new system theoretically a computational peak
performance of~\unit[2.7]{Pflop/s} (double-precision).

Initially, we planed a larger system, but various challenges impeded it: (1)
extracting >900 AOCs from under a raised floor resulted in
58 broken or degraded cables; (2) the aged compute nodes suffer increased
failures rates, limiting available spares (we replaced 107 nodes prior/during
benchmarking); and (3) retrofitting an existing rack/node layout limits
design choices. Nevertheless, we rebuilt one entire server room (of two),
hosting all 24 racks, while keeping the original Fat-Tree network
intact. Spare AOCs, after wiring the HyperX, are used to replace degraded
cables in both topologies\footnote{We filtered out cables by creating fabric
traffic and investigating its port/link error counters, e.g., one filter
criterion was >10,000 symbol errors in a short time period.}.
%
Unfortunately, the number of disabled cables in both networks still exceeds available spares. Consequently, 15 (out of 684) AOCs are absent from a full
12x8 HyperX, while our 204-switch Fat-Tree is missing 197 AOCs (or links
inside the director switches) from a total of 2662.
However, the Fat-Tree's undersubscription should
limit the overall performance degradation.
Hence, we end up with two slightly imperfect networks for the comparison in
Section~\ref{sec:eval}. %But, prior to the performance evaluation, we discuss
%our bottleneck mitigation strategies for the HyperX in the Section hereafter.

