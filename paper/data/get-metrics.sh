#!/bin/bash

ROOTDIR="$( cd "$( dirname "${BASH_SOURCE[0]:-$0}" )/../../" && pwd )"
cd ${ROOTDIR}/paper/data
if [ ! -d ./tp ]; then mkdir tp; fi
LOGPARSER=$ROOTDIR/dep/parse-logs.py

LISTCOMBI="fattree|linear|ftree fattree|cluster|sssp hyperx|linear|sssp hyperx|random|sssp hyperx|cluster|parx"
LISTBM="amg graph500 milc mvmc simdl baidu hpcg minife netgauge swfft comd hpl ntchem ffvc multip2p qball"
for COMBI in $LISTCOMBI; do
	TOPO="`echo $COMBI | cut -d '|' -f1`"
	PLACE="`echo $COMBI | cut -d '|' -f2`"
	ROUT="`echo $COMBI | cut -d '|' -f3`"
	for BM in $LISTBM; do
		$LOGPARSER -b $BM -t $TOPO -r $ROUT -p $PLACE \
			> sr/${BM}.${TOPO}.${ROUT}.${PLACE}.data
		$LOGPARSER -b $BM -t $TOPO -r $ROUT -p $PLACE --tp \
			> tp/${BM}.${TOPO}.${ROUT}.${PLACE}.tp.data
	done
done

# didnt run FT/ftree/linear for 4, 7, and 8 nodes because same as sssp/cluster
LISTBM="amg graph500 milc mvmc hpcg minife swfft comd hpl ntchem ffvc qball"
for BM in $LISTBM; do
	/usr/bin/grep '^2[[:space:]]7\|^2[[:space:]]4\|^4[[:space:]]8' \
		sr/${BM}.fattree.sssp.cluster.data >> sr/${BM}.fattree.ftree.linear.data
	/usr/bin/sort -n -o sr/${BM}.fattree.ftree.linear.data \
		sr/${BM}.fattree.ftree.linear.data
done
BM="baidu"
/usr/bin/grep -v num_mpi sr/${BM}.fattree.ftree.linear.data > tmp
head -13 sr/${BM}.fattree.sssp.cluster.data > sr/${BM}.fattree.ftree.linear.data
cat tmp >> sr/${BM}.fattree.ftree.linear.data
rm -f tmp

BM="imb"
LISTSBM="Barrier Bcast Gather Scatter Reduce Allreduce Alltoall"

for COMBI in $LISTCOMBI; do
	TOPO="`echo $COMBI | cut -d '|' -f1`"
	PLACE="`echo $COMBI | cut -d '|' -f2`"
	ROUT="`echo $COMBI | cut -d '|' -f3`"
	for SBM in $LISTSBM; do
		$LOGPARSER -b $BM -s $SBM -t $TOPO -r $ROUT -p $PLACE \
			> sr/${BM}.${SBM}.${TOPO}.${ROUT}.${PLACE}.data
	done
done

