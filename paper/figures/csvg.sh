#!/bin/bash
for bm in amg comd ffvc graph500 hpcg hpl imb.Barrier milc minife mvmc netgauge ntchem qball swfft system; do
	inkscape --file=${bm}.svg --export-area-drawing --without-gui --export-pdf=${bm}.pdf;
done
