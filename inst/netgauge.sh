#!/bin/bash

ROOTDIR="$( cd "$( dirname "${BASH_SOURCE[0]}" )/../" && pwd )"
cd ${ROOTDIR}
source ${ROOTDIR}/conf/t2hx.sh

BM="netgauge"
if [ ! -f ${ROOTDIR}/${BM}/netgauge ]; then
	mkdir -p ${ROOTDIR}/${BM}/
	cd ${ROOTDIR}/${BM}/
	wget https://htor.inf.ethz.ch/research/netgauge/netgauge-2.4.6.tar.gz
	tar xzf ./netgauge-2.4.6.tar.gz -C ${ROOTDIR}/${BM} --strip-components 1
	for P in `ls ${ROOTDIR}/patches/${BM}/`; do
		patch -p1 < ${ROOTDIR}/patches/${BM}/${P}
	done
	# properly seed the rng instead of dev/urand or clock
	find . -type f -name '*.cpp' -exec sed -i -e 's/MTRand mtrand;/static MTRand mtrand(42);/' {} \;
	./configure MPICC=mpicc MPICXX=mpicxx
	make
	cd ${ROOTDIR}
fi

