#!/bin/bash

ROOTDIR="$( cd "$( dirname "${BASH_SOURCE[0]}" )/../" && pwd )"
cd ${ROOTDIR}
source ${ROOTDIR}/conf/t2hx.sh #intel

BM="SWFFT"  # fortran version is 5-10% faster in my tests
VERSION="d0ef31454577740fbb87618cc35789b7ef838238"
if [ ! -f ${ROOTDIR}/${BM}/build.openmp/TestFDfft ]; then
	cd ${ROOTDIR}/${BM}/
	git checkout -b t2hx ${VERSION}
	for P in `ls ${ROOTDIR}/patches/${BM}/`; do
		git apply --check ${ROOTDIR}/patches/${BM}/${P}
		if [ "x$?" = "x0" ]; then git am --ignore-whitespace < ${ROOTDIR}/patches/${BM}/${P}; fi
	done
	${ROOTDIR}/inst/_math.sh
	oldPATH=${PATH}
	export PATH=${ROOTDIR}/dep/fftw/bin:${oldPATH}

	sed -i -e 's#-ipo -xHost#-march=native#' -e 's# -I${ADVISOR_2018_DIR}/include##' -e 's# -L${ADVISOR_2018_DIR}/lib64 -littnotify##' -e '/^DFFT_MPI_FLDFLAGS/a DFFT_MPI_FLDFLAGS += -lmpi_cxx -lmpi' ./GNUmakefile
	sed -i -e 's#-ipo -xHost#-march=native#' -e 's# -I${ADVISOR_2018_DIR}/include##' -e 's# -L${ADVISOR_2018_DIR}/lib64 -littnotify##' ./GNUmakefile.openmp
	sed -i -e '/STARTSDE/d' -e '/STOPSDE/d' -e '/ittnotify.h/d' ./ssc.c
	make -f GNUmakefile.openmp
	export PATH=${oldPATH}
	cd ${ROOTDIR}
fi

