#!/bin/bash

ROOTDIR="$( cd "$( dirname "${BASH_SOURCE[0]:-$0}" )/../" && pwd )"
cd ${ROOTDIR}
source ${ROOTDIR}/conf/t2hx.sh #intel

BM="graph500"
VERSION="52cf9a1dd0f47382a6daf766c7d99e0e4bebdf5a"
if [ ! -f ${ROOTDIR}/${BM}/mpi/runnable ]; then
	cd ${ROOTDIR}/${BM}/
	git checkout -b t2hx ${VERSION}
	for P in `ls ${ROOTDIR}/patches/${BM}/`; do
		git apply --check ${ROOTDIR}/patches/${BM}/${P}
		if [ "x$?" = "x0" ]; then git am --ignore-whitespace < ${ROOTDIR}/patches/${BM}/${P}; fi
	done
	cd ./mpi/
	sed -i -e 's/ -lnuma//' -e 's/^GCC_BASE := -f/GCC_BASE := -march=native -f/' ./Makefile
	sed -i -e 's/define VALIDATION_LEVEL.*/define VALIDATION_LEVEL 1/' ./parameters.h
	make
	cd ${ROOTDIR}
fi

