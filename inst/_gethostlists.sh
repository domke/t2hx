#!/bin/bash

# reproducible sort/shuf operation
get_seeded_random()
{
	seed="$1"
	openssl enc -aes-256-ctr -pass pass:"$seed" -nosalt </dev/zero 2>/dev/null
}


ROOTDIR="$( cd "$( dirname "${BASH_SOURCE[0]:-$0}" )/../" && pwd )"

###############################################################################
## mappings for single-benchmark runs

# all nodes in linear placement for ftree on fat-tree
${ROOTDIR}/dep/get-hosts.py r 672 > ${ROOTDIR}/conf/hosts.full
# copy for subsets, mpi will parse sequentially and take what it needs
for NumMPI in 4 8 16 32 64 128 256 512 7 14 28 56 112 224 448 672; do
	head -n ${NumMPI} ${ROOTDIR}/conf/hosts.full > ${ROOTDIR}/conf/hosts.linear.${NumMPI}
done

# clustered placements for ftree on fat-tree and PARX on hyperx
for NumMPI in 4 8 16 32 64 128 256 512 7 14 28 56 112 224 448 672; do
	${ROOTDIR}/dep/get-hosts.py g ${NumMPI} > ${ROOTDIR}/conf/hosts.cluster.${NumMPI}
done

# random node placements for sssp on hyperx
for NumMPI in 4 8 16 32 64 128 256 512 7 14 28 56 112 224 448 672; do
	${ROOTDIR}/dep/get-hosts.py r ${NumMPI} > ${ROOTDIR}/conf/hosts.random.${NumMPI}
done


###############################################################################
## mappings for simultanious multi-benchmark runs using 9x 56-node + 5x 32-node configs

# randomzie which app gets which appid
echo -e \
"./simdl.cfg|56
./hpcg.cfg|56
./qball.cfg|56
./amg.cfg|56
./comd.cfg|56
./minife.cfg|56
./ntchem.cfg|56
./hpl.cfg|56
./multip2p.cfg|56
./milc.cfg|32
./swfft.cfg|32
./ffvc.cfg|32
./mvmc.cfg|32
./graph500.cfg|32" | sort -R --random-source=<(get_seeded_random 1234567890) > /dev/shm/grrrrbash
APP=1
for CFG in `cat /dev/shm/grrrrbash | cut -d '|' -f1`; do
	sed -i -e "s/APPID=\"XYZ\"/APPID=\"${APP}\"/" ${ROOTDIR}/conf/${CFG}; APP=$((APP+1))
done
NPERAPP="`cat /dev/shm/grrrrbash | cut -d '|' -f2`"

# all nodes in linear placement for ftree on fat-tree
APP=1; S=1; E=0
for NumMPI in ${NPERAPP}; do
	E=$((E+NumMPI))
	sed -n "${S},${E}p" ${ROOTDIR}/conf/hosts.full > ${ROOTDIR}/conf/hosts.linear${APP}.${NumMPI}
	S=$((S+NumMPI)); APP=$((APP+1));
done

# clustered placements for ftree on fat-tree and PARX on hyperx
APP=1; rm -f /dev/shm/grrrrbash; touch /dev/shm/grrrrbash
for NumMPI in ${NPERAPP}; do
	${ROOTDIR}/dep/get-hosts.py g ${NumMPI} /dev/shm/grrrrbash > ${ROOTDIR}/conf/hosts.cluster${APP}.${NumMPI}
	cat ${ROOTDIR}/conf/hosts.cluster${APP}.${NumMPI} >> /dev/shm/grrrrbash
	APP=$((APP+1))
done

# random node placements for sssp on hyperx
APP=1; rm -f /dev/shm/grrrrbash; touch /dev/shm/grrrrbash
for NumMPI in ${NPERAPP}; do
	${ROOTDIR}/dep/get-hosts.py r ${NumMPI} /dev/shm/grrrrbash > ${ROOTDIR}/conf/hosts.random${APP}.${NumMPI}
	cat ${ROOTDIR}/conf/hosts.random${APP}.${NumMPI} >> /dev/shm/grrrrbash
	APP=$((APP+1))
done

