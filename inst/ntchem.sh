#!/bin/bash

ROOTDIR="$( cd "$( dirname "${BASH_SOURCE[0]:-$0}" )/../" && pwd )"
cd ${ROOTDIR}
source ${ROOTDIR}/conf/t2hx.sh #intel

BM="NTChem"
VERSION="fcafcc4fec195d8a81c19affd1a3b83f7bab4285"
if [ ! -f ${ROOTDIR}/${BM}/bin/rimp2.exe ]; then
	cd ${ROOTDIR}/${BM}/
	git checkout -b t2hx ${VERSION}
	for P in `ls ${ROOTDIR}/patches/${BM}/`; do
		git apply --check ${ROOTDIR}/patches/${BM}/${P}
		if [ "x$?" = "x0" ]; then git am --ignore-whitespace < ${ROOTDIR}/patches/${BM}/${P}; fi
	done
	# link against MKL, because lapack/blas arent parallel and perf is s**t
	source ${ROOTDIR}/conf/intel.cfg
	source ${INTEL_MKL} intel64 lp64
	TOP_DIR=${ROOTDIR}/${BM}
	TYPE=gfortran
	sed -i -e 's/^ATLAS_DIR/#ATLAS_DIR/' -e "s#-latlas -lcblas -lf77blas -llapack#${MKLROOT}/lib/intel64/libmkl_scalapack_ilp64.a -Wl,--start-group ${MKLROOT}/lib/intel64/libmkl_gf_ilp64.a ${MKLROOT}/lib/intel64/libmkl_gnu_thread.a ${MKLROOT}/lib/intel64/libmkl_core.a ${MKLROOT}/lib/intel64/libmkl_blacs_openmpi_ilp64.a -Wl,--end-group -lgomp -lpthread -lm -ldl#" -e 's#-L${ATLAS_DIR} ##' ./platforms/config_mine.${TYPE}
	sed -i -e 's/ -fc=gfortran//' -e 's/-O2/-O2 -march=native/' -e 's/-O3/-O3 -march=native/' ./config/linux64_mpif90_omp_gfortran.makeconfig.in
	sed -i -e '/$(INCLUDE)/d' ./GNUmakefile
	sed -i -e 's/1,MyRank/INT(1,4),INT(MyRank,4)/' -e 's/0,MyRank/INT(0,4),INT(MyRank,4)/' ./src/mp2/mp2_main_mpiomp.f90
	sed -i -e '/STARTSDE/d' -e '/STOPSDE/d' -e '/ittnotify.h/d' ./src/util_lib/ssc.c
	sed -i -e 's# -I${ADVISOR_2018_DIR}/include##' ./src/util_lib/GNUmakefile
	sed -i -e 's# -L${ADVISOR_2018_DIR}/lib64 -littnotify##' ./src/mp2/GNUmakefile
	cp ./platforms/config_mine.${TYPE} ./config_mine
	./config_mine
	mkdir -p ${ROOTDIR}/${BM}/bin
	make CC=mpicc CXX=mpicxx F77C=mpif77 F90C=mpif90 USE_MPI=yes
	cd ${ROOTDIR}
fi

