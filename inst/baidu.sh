#!/bin/bash

ROOTDIR="$( cd "$( dirname "${BASH_SOURCE[0]:-$0}" )/../" && pwd )"
cd ${ROOTDIR}
source ${ROOTDIR}/conf/t2hx.sh #intel

BM="baiduAllRed"
VERSION="73c7b7f8d34f7163a3a46aa0fe48e2a418d25fe9"
if [ ! -f "${ROOTDIR}/${BM}/allreduce-test" ]; then
	cd ${ROOTDIR}/${BM}/
	git checkout -b t2hx ${VERSION}
	for P in `ls ${ROOTDIR}/patches/${BM}/`; do
		git apply --check ${ROOTDIR}/patches/${BM}/${P}
		if [ "x$?" = "x0" ]; then git am --ignore-whitespace < ${ROOTDIR}/patches/${BM}/${P}; fi
	done
	sed -i -e 's/^CFLAGS:=-s/CFLAGS:=-O2 -march=native -s/' ./Makefile
	make MPI_ROOT=$OMPIDIR
	cd ${ROOTDIR}
fi

