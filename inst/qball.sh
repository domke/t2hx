#!/bin/bash

ROOTDIR="$( cd "$( dirname "${BASH_SOURCE[0]:-$0}" )/../" && pwd )"
cd ${ROOTDIR}
source ${ROOTDIR}/conf/t2hx.sh #impi

BM="qball"
VERSION="a33d0438dd83372d645d670bef7e0060c8933011"
if [ ! -f ${ROOTDIR}/${BM}/bin/qball ]; then
	cd ${ROOTDIR}/${BM}/
	git checkout -b t2hx ${VERSION}
	for P in `ls ${ROOTDIR}/patches/${BM}/`; do
		git apply --check ${ROOTDIR}/patches/${BM}/${P}
		if [ "x$?" = "x0" ]; then git am --ignore-whitespace < ${ROOTDIR}/patches/${BM}/${P}; fi
	done
	${ROOTDIR}/inst/_math.sh
	oldPATH=${PATH}
	export PATH=${ROOTDIR}/${BM}/fftw/bin:${oldPATH}

	autoreconf -i
	./configure --prefix=${ROOTDIR}/${BM} \
		--with-fftw3-prefix=${ROOTDIR}/dep/fftw \
		--with-blas=${ROOTDIR}/dep/blas/blas_LINUX.a \
		--with-lapack=${ROOTDIR}/dep/lapack/liblapack.a \
		--with-blacs=${ROOTDIR}/dep/scalapack/libscalapack.a \
		--with-scalapack=${ROOTDIR}/dep/scalapack/libscalapack.a \
		--with-xerces-prefix=${ROOTDIR}/dep/xerces LDFLAGS="-lm"
	sed -i -e 's#-O2#-O2 -march=native#' ./Makefile
	make -j
	make install
	cd ./bin
	cp ../examples/gold_benchmark/Au_PBE.xml .
	cp ../examples/gold_benchmark/qbox_gold_makeinputs.pl .

	export PATH=${oldPATH}
	cd ${ROOTDIR}
fi

