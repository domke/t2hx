#!/bin/bash

ROOTDIR="$( cd "$( dirname "${BASH_SOURCE[0]:-$0}" )/../" && pwd )"
cd ${ROOTDIR}
source ${ROOTDIR}/conf/t2hx.sh #impi

BM="mpiGraph"
VERSION="244d1a9f2b4b949365c568a2941adb980253e3ff"
if [ ! -f ${ROOTDIR}/${BM}/mpiGraph ]; then
	cd ${ROOTDIR}/${BM}/
	git checkout -b t2hx ${VERSION}
	for P in `ls ${ROOTDIR}/patches/${BM}/`; do
		git apply --check ${ROOTDIR}/patches/${BM}/${P}
		if [ "x$?" = "x0" ]; then git am --ignore-whitespace < ${ROOTDIR}/patches/${BM}/${P}; fi
	done
	sed -i -e 's#mpicc -o mpi#mpicc -O2 -march=native -o mpi#' ./makefile
	make
	cd ${ROOTDIR}
fi

