#!/bin/bash

ROOTDIR="$( cd "$( dirname "${BASH_SOURCE[0]:-$0}" )/../" && pwd )"
cd $ROOTDIR
source $ROOTDIR/conf/t2hx.sh #impi

BM="IMB"
VERSION="v2018.1"
if [ ! -f $ROOTDIR/$BM/src/IMB-MPI1 ]; then
	cd $ROOTDIR/$BM/
	git checkout -b t2hx ${VERSION}
	for P in `ls $ROOTDIR/patches/${BM}/`; do
		git apply --check $ROOTDIR/patches/${BM}/${P}
		if [ "x$?" = "x0" ]; then git am --ignore-whitespace < $ROOTDIR/patches/${BM}/${P}; fi
	done
	cd ./src/
	sed -i -e 's/define OVERALL_VOL .*/define OVERALL_VOL 2199023255552L/' ./IMB_settings.h
	sed -i -e 's/define OVERALL_VOL .*/define OVERALL_VOL 2199023255552L/' ./IMB_settings_io.h

	make clean
	sed -i -e 's/mpiicc/mpicc/g' ./make_ict
	OPTFLAGS="-O2 -march=native" make -f make_ict MPI1

	mv ./IMB-MPI1 ./IMB-MPI1.bak

	make clean
	OPTFLAGS=-DCHECK make -f make_ict MPI1

	mv ./IMB-MPI1 ./IMB-MPI1.check
	mv ./IMB-MPI1.bak ./IMB-MPI1

	cd $ROOTDIR
fi

