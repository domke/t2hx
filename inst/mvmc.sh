#!/bin/bash

ROOTDIR="$( cd "$( dirname "${BASH_SOURCE[0]:-$0}" )/../" && pwd )"
cd ${ROOTDIR}
source ${ROOTDIR}/conf/t2hx.sh #intel

BM="mVMC"
VERSION="7c58766b180ccb1035e4c220208b64ace3c49cf2"
if [ ! -f ${ROOTDIR}/${BM}/src/vmc.out ] || [ `stat -c %s ${ROOTDIR}/${BM}/src/vmc.out` -eq 0 ]; then
	cd ${ROOTDIR}/${BM}/
	git checkout -b t2hx ${VERSION}
	for P in `ls ${ROOTDIR}/patches/${BM}/`; do
		git apply --check ${ROOTDIR}/patches/${BM}/${P}
		if [ "x$?" = "x0" ]; then git am --ignore-whitespace < ${ROOTDIR}/patches/${BM}/${P}; fi
	done
	source ${ROOTDIR}/conf/intel.cfg
	source ${INTEL_MKL} intel64 lp64
	cd ${ROOTDIR}/${BM}/src
	sed -i -e '/STARTSDE/d' -e '/STOPSDE/d' -e '/ittnotify.h/d' ./vmcmain.c
	sed -i -e 's#-mavx##' -e 's#-ipo -xHost#-march=native#' -e 's# -I${ADVISOR_2018_DIR}/include##' -e 's#-L${ADVISOR_2018_DIR}/lib64 -littnotify##' -e '/touch vmc.out/d' ./Makefile
	mv ./Makefile_intel ./Makefile_intel.bak
	tr -d '\r' < ./Makefile_skeleton > ./Makefile_intel
	sed -i -e '/^LIB/a LIB = ${MKLROOT}/lib/intel64/libmkl_scalapack_lp64.a -Wl,--start-group ${MKLROOT}/lib/intel64/libmkl_gf_lp64.a ${MKLROOT}/lib/intel64/libmkl_gnu_thread.a ${MKLROOT}/lib/intel64/libmkl_core.a ${MKLROOT}/lib/intel64/libmkl_blacs_openmpi_lp64.a -Wl,--end-group -lgomp -lpthread -lm -ldl' -e 's#-O3 -openmp#-O3 -march=native -fopenmp -m64 -I${MKLROOT}/include#' ./Makefile_intel
	mv ./sfmt/Makefile_intel ./sfmt/Makefile_intel.bak
	tr -d '\r' < ./sfmt/Makefile_skeleton > ./sfmt/Makefile_intel
	sed -i -e 's/^CC =.*/CC = gcc/' -e 's/-O3/-O3 -march=native/' -e 's/19937/19937 -DHAVE_SSE2/' ./sfmt/Makefile_intel
	mv ./pfapack/Makefile_intel ./pfapack/Makefile_intel.bak
	tr -d '\r' < ./pfapack/Makefile_gnu > ./pfapack/Makefile_intel
	sed -i -e 's/-O3/-O3 -march=native/' ./pfapack/Makefile_intel
	make intel
	cd ${ROOTDIR}
fi

