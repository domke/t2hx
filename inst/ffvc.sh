#!/bin/bash

ROOTDIR="$( cd "$( dirname "${BASH_SOURCE[0]:-$0}" )/../" && pwd )"
cd ${ROOTDIR}
source ${ROOTDIR}/conf/t2hx.sh #intel

BM="FFVC"
VERSION="890a3f9bb3a5cf358504063a1751383b7d46f86d"
if [ ! -f ${ROOTDIR}/${BM}/bin/ffvc_mini ]; then
	cd ${ROOTDIR}/${BM}/
	git checkout -b t2hx ${VERSION}
	for P in `ls ${ROOTDIR}/patches/${BM}/`; do
		git apply --check ${ROOTDIR}/patches/${BM}/${P}
		if [ "x$?" = "x0" ]; then git am --ignore-whitespace < ${ROOTDIR}/patches/${BM}/${P}; fi
	done
	cd ${ROOTDIR}/${BM}/src
	sed -i -e '/STARTSDE/d' -e '/STOPSDE/d' -e '/ittnotify.h/d' ./FFV/main.C
	sed -i -e 's#-O3#-O3 -march=native#' ./make_setting.gcc
	rm make_setting; ln -s make_setting.gcc make_setting
	make
	cd ${ROOTDIR}
fi

