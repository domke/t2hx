#!/bin/bash

ROOTDIR="$( cd "$( dirname "${BASH_SOURCE[0]:-$0}" )/../" && pwd )"
cd ${ROOTDIR}
source ${ROOTDIR}/conf/t2hx.sh #intel

BM="miniFE"
VERSION="daeddf3bfaf3b521a932245fad9871336b53c166"
if [ ! -f ${ROOTDIR}/${BM}/openmp-opt/src/miniFE.x ]; then
	cd ${ROOTDIR}/${BM}/
	git checkout -b t2hx ${VERSION}
	for P in `ls ${ROOTDIR}/patches/${BM}/`; do
		git apply --check ${ROOTDIR}/patches/${BM}/${P}
		if [ "x$?" = "x0" ]; then git am --ignore-whitespace < ${ROOTDIR}/patches/${BM}/${P}; fi
	done
	cd ${ROOTDIR}/${BM}/openmp-opt/src
	sed -i -e '/STARTSDE/d' -e '/STOPSDE/d' -e '/ittnotify.h/d' ./driver.hpp
	sed -i -e '/STARTSDE/d' -e '/STOPSDE/d' -e '/ittnotify.h/d' ./main.cpp
	sed -i -e 's#-mavx##' -e 's#-ipo -xHost#-march=native#' -e 's# -I${ADVISOR_2018_DIR}/include##' -e 's#-L${ADVISOR_2018_DIR}/lib64 -littnotify##' -e 's#GLOBAL_ORDINAL=.*#GLOBAL_ORDINAL=long#' ./Makefile
	make
	cd ${ROOTDIR}
fi

