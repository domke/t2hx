#!/bin/bash

ROOTDIR="$( cd "$( dirname "${BASH_SOURCE[0]:-$0}" )/../" && pwd )"
cd ${ROOTDIR}
source ${ROOTDIR}/conf/t2hx.sh intel

BM="HPCG"
VERSION="5422fecd0a009a8731d0bd96b957d443297a53bc"
if [ ! -f ${ROOTDIR}/${BM}/build/bin/xhpcg ]; then
	cd ${ROOTDIR}/${BM}/
	git checkout -b t2hx ${VERSION}
	for P in `ls ${ROOTDIR}/patches/${BM}/`; do
		git apply --check ${ROOTDIR}/patches/${BM}/${P}
		if [ "x$?" = "x0" ]; then git am --ignore-whitespace < ${ROOTDIR}/patches/${BM}/${P}; fi
	done
	mkdir -p build; cd build
	sed -i -e '/STARTSDE/d' -e '/STOPSDE/d' -e '/ittnotify.h/d' ../src/main.cpp
	sed -i -e 's#O3 -openmp -mavx#O3 -DNDEBUG -xHost -qopt-prefetch=0 -qopenmp -std=c++11 -I${MKLROOT}/include#' -e 's#CXXFLAGS)#CXXFLAGS) ${MKLROOT}/lib/intel64/libmkl_scalapack_lp64.a -Wl,--start-group ${MKLROOT}/lib/intel64/libmkl_intel_lp64.a ${MKLROOT}/lib/intel64/libmkl_intel_thread.a ${MKLROOT}/lib/intel64/libmkl_core.a ${MKLROOT}/lib/intel64/libmkl_blacs_openmpi_lp64.a -Wl,--end-group -qopenmp-link=static -lpthread -lm -ldl#' ../setup/Make.MPI_ICPC_OMP
	../configure MPI_ICPC_OMP
	sed -i -e 's# $(LINKFLAGS)##' -e 's#-o bin/xhpcg#-o bin/xhpcg $(LINKFLAGS)#' ./Makefile
	make -j
	cd ${ROOTDIR}
fi

