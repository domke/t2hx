#!/bin/bash

ROOTDIR="$( cd "$( dirname "${BASH_SOURCE[0]}" )/../" && pwd )"
cd ${ROOTDIR}
source ${ROOTDIR}/conf/t2hx.sh #intel

BM="MILC"
#VERSION = http://www.nersc.gov/users/computational-systems/cori/ \
#	nersc-8-procurement/trinity-nersc-8-rfp/nersc-8-trinity-benchmarks/milc/
if [ ! -f ${ROOTDIR}/${BM}/benchmark_n8/single_node/su3_rmd ]; then
	mkdir -p ${ROOTDIR}/${BM}/
	cd ${ROOTDIR}/${BM}/
	wget http://www.nersc.gov/assets/Trinity--NERSC-8-RFP/Benchmarks/May31/TrN8MILC7May30.tar
	tar xf ./TrN8MILC7May30.tar -C ${ROOTDIR}/${BM} --strip-components 1
	for P in `ls ${ROOTDIR}/patches/${BM}/`; do
		patch -p1 < ${ROOTDIR}/patches/${BM}/${P}
	done
	sed -i -e 's#^CC.*= cc#CC = gcc#' -e 's#O3 -fopenmp#O3 -march=native -fopenmp#' ./libraries/Make_opteron
	cd ./ks_imp_dyn
	sed -i -e 's#^CC = cc##' -e 's#^  CC = /usr.*mpicc#  CC = mpicc#' \
		-e 's#^LIBADD = -lhugetlbfs#LIBADD =#' -e 's#O3 -fopenmp#O3 -march=native -fopenmp#' ./Makefile
	gmake su3_rmd
	mv ./su3_rmd ../benchmark_n8/single_node/
	cd ${ROOTDIR}
fi

