#!/bin/bash

ROOTDIR="$( cd "$( dirname "${BASH_SOURCE[0]:-$0}" )/../" && pwd )"
cd ${ROOTDIR}
mkdir -p ${ROOTDIR}/dep/packages
YEL='\033[1;33m'
NCO='\033[0m'

if [[ -f /etc/redhat-release ]] && ! [[ "$1" = *"skip"* ]]; then
	echo -e 'Checking for missing dependencies...'
	yum list installed > /dev/shm/inst
	L="cmake autoconf automake libtool cpupowerutils screen pdsh
	   systemd-devel cmake libnl3-devel valgrind-devel bison flex
	   nmap gcc gcc-c++ gcc-gfortran wget zsh libudev-devel zlib-devel
	   libstdc++-devel pciutils tcl tcl-devel tk glib2-devel
	   kernel-devel vim rpm-build pkgconfig make python2-numpy
	   numactl-devel lsof psmisc git swig python-devel
	   python2-clustershell rdma-core-devel"
	MISSING=0
	for P in ${L}; do
		if ! grep ${P} /dev/shm/inst > /dev/null 2>&1; then
			echo -e "  ${YEL}${P}${NCO}"
			MISSING=1
		fi
	done
	if [ ${MISSING} -ne 0 ]; then
		echo -e 'ERR: missing dep; skip rest; install listed packages or modify this script'
		exit
	fi
	#yum -y install cmake autoconf automake libtool cpupowerutils \
	#	screen pdsh systemd-devel cmake libnl3-devel \
	#	valgrind-devel bison flex nmap gcc gcc-c++ \
	#	gcc-gfortran wget zsh libudev-devel zlib-devel \
	#	libstdc++-devel pciutils tcl tcl-devel tk glib2-devel \
	#	kernel-devel vim rpm-build pkgconfig make python2-numpy \
	#	numactl-devel lsof psmisc git swig python-devel \
	#	python2-clustershell rdma-core-devel
elif ! [[ -f /etc/redhat-release ]]; then
	echo -e '\nNote: untested linux distro; please install everything manuall;\n      everything hereafter may break, no guarantees ;-)'
	exit
fi

if [[ $1 = *"routing"* ]]; then
	echo -e '\nATTENTION!!! This part will require root access to install OFED and dependencies.\n      Installing in NFS will not work when mounted with nosuid flag.\n\n(10s time to abort and manually install)\n'

	BM="OFED"
	VERSION="4.8-2"
	if [ -z "`locate umad.h`" ]; then
		cd ${ROOTDIR}/dep/packages/

		wget https://openfabrics.org/downloads/${BM}/ofed-${VERSION}/${BM}-${VERSION}.tgz
		tar xzf ${BM}-${VERSION}.tgz
		cd ./${BM}-${VERSION}
		sed -i -e "s/libudev_devel = 'libudev-devel/libudev_devel = 'systemd-devel/g" ./install.pl
		echo -e 'ofed-scripts=y\nrdma-core=y\nrdma-core-devel=y\nlibibverbs=y\nlibibverbs-utils=y\nlibrdmacm=y\nlibrdmacm-utils=y\nlibibumad=y\nlibibcm=y\nibacm=y\niwpmd=y\nsrp_daemon=y\nmstflint=y\nofed-docs=y\ncore=y\nmlx4=y\nmlx4_en=y\nmlx5=y\ncxgb3=y\ncxgb4=y\nnes=y\nqib=y\nipoib=y\nqed=y\nfirmware=y\nqede=y\nqedr=y\nbnxt_re=y\nvmw_pvrdma=y\ncompat-rdma=y\nibutils=y\ninfiniband-diags=y\nlibfabric=y\nperftest=y\n' > ./ofed-basic.conf
		./install.pl -vvv --builddir /dev/shm -c ./ofed-basic.conf
		systemctl restart openibd

		cd ${ROOTDIR}
	fi

	BM="metis"
	VERSION="5.1.0"
	if [ ! -f ${ROOTDIR}/dep/${BM}/lib/libmetis.a ]; then
		cd ${ROOTDIR}/dep/packages/

		wget http://glaros.dtc.umn.edu/gkhome/fetch/sw/metis/${BM}-${VERSION}.tar.gz
		tar xzf ${BM}-${VERSION}.tar.gz
		cd ${BM}-${VERSION}/
		mkdir ./build
		make config shared=1 prefix=${ROOTDIR}/dep/${BM}
		cd ./build/Linux-x86_64
		make install

		cd ${ROOTDIR}
	fi

	BM="libcsv"
	VERSION="3.0.3"
	if [ ! -f ${ROOTDIR}/dep/${BM}/lib/libcsv.a ]; then
		cd ${ROOTDIR}/dep/packages/

		wget https://downloads.sourceforge.net/project/libcsv/libcsv/libcsv-3.0.3/libcsv-3.0.3.tar.gz\?r\=https%3A%2F%2Fsourceforge.net%2Fprojects%2Flibcsv%2Ffiles%2Flibcsv%2Flibcsv-3.0.3%2Flibcsv-3.0.3.tar.gz%2Fdownload%3Fuse_mirror%3Djaist\&ts\=1549503522\&use_mirror\=jaist -O ${BM}-${VERSION}.tar.gz
		tar xzf ${BM}-${VERSION}.tar.gz
		cd ${BM}-${VERSION}/
		mkdir ./build; cd ./build
		../configure --prefix=${ROOTDIR}/dep/${BM} --enable-static --enable-shared
		make
		make clean
		make install

		cd ${ROOTDIR}
	fi

	BM="opensm"
	VERSION="3.3.21"
	if [ ! -f ${ROOTDIR}/dep/${BM}/sbin/mpicc ]; then
		cd ${ROOTDIR}/dep/osm.git

		git checkout -b t2hx ${VERSION}
		for P in `ls ${ROOTDIR}/patches/${BM}/`; do
			git apply --check ${ROOTDIR}/patches/${BM}/${P}
			if [ "x$?" = "x0" ]; then git am --ignore-whitespace < ${ROOTDIR}/patches/${BM}/${P}; fi
		done
		./autogen.sh
		mkdir build; cd build
		../configure --prefix=${ROOTDIR}/dep/${BM} \
			--enable-metis \
			--with-metis-includes=${ROOTDIR}/dep/metis/include \
			--with-metis-libs=${ROOTDIR}/dep/metis/lib \
			--enable-libcsv \
			--with-libcsv-includes=${ROOTDIR}/dep/libcsv/include \
			--with-libcsv-libs=${ROOTDIR}/dep/libcsv/lib
		make -j
		make install

		echo -e 'Note: before starting new opensm make sure libmetis.so and libcsv.so are accessible, either via LD_LIBRARY_PATH or ldconfig'

		cd ${ROOTDIR}
	fi

	BM="parseIB.git"
	VERSION="ea9a7cee06d4bb6edc376ce0b94fd817dae6a531"
	cd ${ROOTDIR}/dep/${BM}
	git checkout -b t2hx ${VERSION}
	cd ${ROOTDIR}

else

	BM="openmpi"
	VERSION="v1.10.7"
	if [ ! -f ${ROOTDIR}/dep/${BM}/bin/mpicc ]; then
		cd ${ROOTDIR}/dep/ompi.git

		# building main openmpi lib for benchmarking
		git checkout -b t2hx ${VERSION}
		for P in `ls ${ROOTDIR}/patches/${BM}/`; do
			git apply --check ${ROOTDIR}/patches/${BM}/${P}
			if [ "x$?" = "x0" ]; then git am --ignore-whitespace < ${ROOTDIR}/patches/${BM}/${P}; fi
		done
		./autogen.sh
		mkdir build-default; cd ./build-default
		../configure --prefix=${ROOTDIR}/dep/${BM} \
			--enable-orterun-prefix-by-default \
			--enable-mpi-fortran \
			--enable-mpi-cxx \
			--enable-mpi-cxx-seek \
			--enable-shared \
			--enable-static \
			--with-verbs \
			--with-hwloc=internal \
			--disable-oshmem \
			--disable-java \
			--disable-libompitrace \
			--disable-vt \
			--without-ucx \
			--without-cuda \
			--enable-openib-dynamic-sl \
			CC=gcc CXX=g++ FC=gfortran
		make -j all
		make install
		mv ${ROOTDIR}/dep/${BM} ${ROOTDIR}/dep/${BM}-default

		echo -e "#!/bin/bash
		SW=\"${ROOTDIR}/dep/${BM}\"
		export OMPIDIR=\"\${SW}\"
		export OMPI_MPICC=gcc
		export OMPI_MPICXX=g++
		export OMPI_MPIF77=gfortran
		export OMPI_MPIF90=gfortran
		export PATH=\"\${SW}/bin\":\$PATH
		export LD_LIBRARY_PATH=\"\${SW}/lib\":\$LD_LIBRARY_PATH
		export INCLUDE=\"\${SW}/include\":\$INCLUDE
		export C_INCLUDE_PATH=\"\${SW}/include\":\$C_INCLUDE_PATH
		export MANPATH=\"\${SW}/man\":\$MANPATH" > ${ROOTDIR}/dep/${BM}-default/init

		cd ${ROOTDIR}/dep/ompi.git

		# building ibprof-enabled openmpi lib to get p2p comm data
		git checkout -b t2hx-ibprof ${VERSION}
		for P in `ls ${ROOTDIR}/patches/${BM}-ibprof/`; do
			git apply --check ${ROOTDIR}/patches/${BM}-ibprof/${P}
			if [ "x$?" = "x0" ]; then git am --ignore-whitespace < ${ROOTDIR}/patches/${BM}-ibprof/${P}; fi
		done
		./autogen.sh
		mkdir build-ibprof; cd ./build-ibprof
		../configure --prefix=${ROOTDIR}/dep/${BM}-ibprof \
			--enable-orterun-prefix-by-default \
			--enable-mpi-fortran \
			--enable-mpi-cxx \
			--enable-mpi-cxx-seek \
			--enable-shared \
			--enable-static \
			--with-verbs \
			--with-hwloc=internal \
			--disable-oshmem \
			--disable-java \
			--disable-libompitrace \
			--disable-vt \
			--without-ucx \
			--without-cuda \
			--enable-openib-dynamic-sl \
			--enable-peruse \
			CC=gcc CXX=g++ FC=gfortran
		make -j all
		make install
		mv ${ROOTDIR}/dep/${BM} ${ROOTDIR}/dep/${BM}-ibprof

		echo -e "#!/bin/bash
		SW=\"${ROOTDIR}/dep/${BM}\"
		export OMPIDIR=\"\${SW}\"
		export OMPI_MPICC=gcc
		export OMPI_MPICXX=g++
		export OMPI_MPIF77=gfortran
		export OMPI_MPIF90=gfortran
		export PATH=\"\${SW}/bin\":\$PATH
		export LD_LIBRARY_PATH=\"\${SW}/lib\":\$LD_LIBRARY_PATH
		export INCLUDE=\"\${SW}/include\":\$INCLUDE
		export C_INCLUDE_PATH=\"\${SW}/include\":\$C_INCLUDE_PATH
		export MANPATH=\"\${SW}/man\":\$MANPATH" > ${ROOTDIR}/dep/${BM}-ibprof/init

		ln -s ${ROOTDIR}/dep/${BM}-default ${ROOTDIR}/dep/${BM}

		cd ${ROOTDIR}
	fi

	BM="ibprof"
	VERSION="8c6dc2d9906638f407c08a8250417deb1df665e1"
	if [ ! -f ${ROOTDIR}/dep/${BM}/lib/libibprof.so ]; then
		cd ${ROOTDIR}/dep/${BM}.git

		rm -f ${ROOTDIR}/dep/openmpi
		ln -s ${ROOTDIR}/dep/openmpi-ibprof ${ROOTDIR}/dep/openmpi
		source ${ROOTDIR}/dep/openmpi/init

		# building ibprof-enabled openmpi lib to get p2p comm data
		git checkout -b t2hx ${VERSION}
		for P in `ls ${ROOTDIR}/patches/${BM}/`; do
			git apply --check ${ROOTDIR}/patches/${BM}/${P}
			if [ "x$?" = "x0" ]; then git am --ignore-whitespace < ${ROOTDIR}/patches/${BM}/${P}; fi
		done

		if [ ! -f ${ROOTDIR}/dep/otf/bin/otfprint ]; then
			cd ${ROOTDIR}/dep/packages/
			wget -O OTF-1.12.5salmon.tar.gz http://wwwpub.zih.tu-dresden.de/%7Emlieber/dcount/dcount.php\?package\=otf\&get\=OTF-1.12.5salmon.tar.gz
			tar xzf OTF-1.12.5salmon.tar.gz
			cd ./OTF-1.12.5salmon/
			./configure --prefix=${ROOTDIR}/dep/otf --with-mpi --enable-python-bindings
			make -j
			make install
			cd ${ROOTDIR}/dep/${BM}.git
		fi

		mv ./profiler/ibprof_pmpi.h ./profiler/ibprof_pmpi.h.bak
		cp ./profiler/ibprof8_pmpi.h ./profiler/ibprof_pmpi.h
		sed -i -e 's#/home/users/kevin/opt/python-2.7.9/bin/#/usr/bin/env #' ./converter/ibprofparser.py
		sed -i -e 's#/home/users/kevin/opt/python-2.7.9/bin/#/usr/bin/env #' ./converter/ibprofportdata.py
		mkdir -p ${ROOTDIR}/dep/${BM}
		make OTF_PATH=${ROOTDIR}/dep/otf IBPROF_INSTALL_PATH=${ROOTDIR}/dep/${BM}
		make OTF_PATH=${ROOTDIR}/dep/otf IBPROF_INSTALL_PATH=${ROOTDIR}/dep/${BM} install

		echo -e "#!/bin/bash
		SW1=\"${ROOTDIR}/dep/otf\"
		export PATH=\"\${SW1}/bin\":\$PATH
		export LD_LIBRARY_PATH=\"\${SW1}/lib64\":\"\${SW1}/lib\":\$LD_LIBRARY_PATH
		export PYTHONPATH=\"\${SW1}/lib64/python2.7/site-packages/otf\":\"\${SW1}/lib/python2.7/site-packages/otf\":\$PYTHONPATH
		SW2=\"${ROOTDIR}/dep/ibprof\"
		export IBPROFDIR=\"\${SW2}\"
		export PATH=\"\${SW2}/bin\":\$PATH
		export LD_LIBRARY_PATH=\"\${SW2}/lib\":\$LD_LIBRARY_PATH
		export CPATH=\"\${SW2}/lib\"
		export LIBRARY_PATH=\"\${SW2}/lib\"" > ${ROOTDIR}/dep/${BM}/init

		rm -f ${ROOTDIR}/dep/openmpi
		ln -s ${ROOTDIR}/dep/openmpi-default ${ROOTDIR}/dep/openmpi
		cd ${ROOTDIR}
	fi

	BM="tau"
	VERSION="2.27"
	if [ ! -f ${ROOTDIR}/dep/${BM}/x86_64/lib/libTAU.so ]; then
		cd ${ROOTDIR}/dep/packages/

		wget https://www.cs.uoregon.edu/research/${BM}/${BM}_releases/${BM}-${VERSION}.tar.gz
		tar xzf ${BM}-${VERSION}.tar.gz
		cd ${BM}-${VERSION}/
		source ${ROOTDIR}/dep/openmpi/init
		./configure -c++=g++ -cc=gcc -fortran=gnu -prefix=${ROOTDIR}/dep/${BM} -mpi -mpiinc=${OMPIDIR}/include -mpilib=${OMPIDIR}/lib -mpit -LINUXTIMERS -noplugins
		export PATH=${ROOTDIR}/dep/${BM}/x86_64/bin:${PATH}
		make install

		echo -e "#!/bin/bash
		SW=\"$ROOTDIR/dep/tau/x86_64\"
		export TAUDIR=\"\${SW}\"
		export PATH=\"\${SW}/bin\":\$PATH
		export LD_LIBRARY_PATH=\"\${SW}/lib\":\$LD_LIBRARY_PATH" > ${ROOTDIR}/dep/${BM}/init

		cd $ROOTDIR
	fi

	${ROOTDIR}/inst/_math.sh
	${ROOTDIR}/inst/_gethostlists.sh
	if [ ! -d ${ROOTDIR}/conf/comm4parx ]; then tar xzf ${ROOTDIR}/conf/comm4parx.tgz -C ${ROOTDIR}/conf; fi
fi


###############################################################################
################### SKIP REST #################################################
###############################################################################

# SKIP: installed on image0 for all users
if [ ! 0 ]; then
	pip install --user ClusterShell
	export PYTHONPATH=$PYTHONPATH:$HOME/.local/lib
	export PATH=$PATH:$HOME/.local/bin
fi

#NEEDED ON ALL NODES :-( -> SKIP: installed on image0 for all users
#if [ ! -f $ROOTDIR/dep/pdsh-2.33/bin/pdsh ]; then
if [ ! 0 ]; then
	cd $ROOTDIR/dep/packages
	wget https://github.com/chaos/pdsh/releases/download/pdsh-2.33/pdsh-2.33.tar.gz
	tar xzf ./pdsh-2.33.tar.gz
	cd ./pdsh-2.33
	mkdir build; cd build
	../configure --prefix=$ROOTDIR/dep/pdsh-2.33 --with-rsh --with-ssh
	make -j
	make install

	echo -e "#!/bin/bash
	SW=\"$ROOTDIR/dep/pdsh-2.33\"
	export PATH=\"\${SW}/bin\":\$PATH
	export LD_LIBRARY_PATH=\"\${SW}/lib\":\$LD_LIBRARY_PATH
	export MANPATH=\"\${SW}/man\":\$MANPATH" > $ROOTDIR/dep/pdsh-2.33/init

	cd $ROOTDIR
fi

#SKIP: using older mpi
#if [ ! -f $ROOTDIR/dep/openmpi-3.1.2/bin/mpicc ]; then
if [ ! 0 ]; then
	cd $ROOTDIR/dep/packages
	wget https://download.open-mpi.org/release/open-mpi/v3.1/openmpi-3.1.2.tar.bz2
	tar xjf ./openmpi-3.1.2.tar.bz2
	cd ./openmpi-3.1.2
	mkdir build; cd build
	../configure --prefix=$ROOTDIR/dep/openmpi-3.1.2 \
		--enable-orterun-prefix-by-default \
		--enable-mpi-fortran \
		--enable-mpi-cxx \
		--enable-mpi-cxx-seek \
		--enable-shared \
		--enable-static \
		--with-verbs \
		--with-hwloc=internal \
		--disable-oshmem \
		--disable-java \
		--disable-libompitrace \
		--without-ucx \
		--without-cuda
	make -j all
	make install

	echo -e "#!/bin/bash
	SW=\"$ROOTDIR/dep/openmpi-3.1.2\"
	export OMPIDIR=\"\${SW}\"
	export PATH=\"\${SW}/bin\":\$PATH
	export LD_LIBRARY_PATH=\"\${SW}/lib\":\$LD_LIBRARY_PATH
	export INCLUDE=\"\${SW}/include\":\$INCLUDE
	export C_INCLUDE_PATH=\"\${SW}/include\":\$C_INCLUDE_PATH
	export MANPATH=\"\${SW}/man\":\$MANPATH" > $ROOTDIR/dep/openmpi-3.1.2/init

	cd $ROOTDIR
fi

#SKIP: only works well with lustre which we dont have
#if [ ! -f $ROOTDIR/dep/mpifileutils/bin/dbcast ]; then
if [ ! 0 ]; then
	source $ROOTDIR/dep/openmpi-3.1.2/init
	git clone https://github.com/hpc/mpifileutils.git
	cd ./mpifileutils
	git checkout -b t2hx 090f92cb48d6e17b8901decd976a63afe2eb78e9
	./autogen.sh
	sed -i -e 's#./configure #./configure --enable-static --disable-shared --with-pic #g' ./buildme_dependencies
	./buildme_dependencies
	export PATH=`pwd`/install/bin:$PATH
	export LD_LIBRARY_PATH=`pwd`/install/lib:$LD_LIBRARY_PATH
	sed -i -e 's/ac_lib  $ac_func/ac_lib -llwgrp $ac_func/' ./configure
	./configure --prefix=$ROOTDIR/dep/mpifileutils --enable-static --disable-shared PKG_CONFIG_PATH=`pwd`/install/lib/pkgconfig --with-dtcmp=`pwd`/install --with-pic
	make -j
	make install

	echo -e "#!/bin/bash
	SW=\"$ROOTDIR/dep/mpifileutils\"
	export PATH=\"\${SW}/bin\":\$PATH
	export LD_LIBRARY_PATH=\"\${SW}/lib\":\$LD_LIBRARY_PATH
	export INCLUDE=\"\${SW}/include\":\$INCLUDE
	export C_INCLUDE_PATH=\"\${SW}/include\":\$C_INCLUDE_PATH" > $ROOTDIR/dep/mpifileutils/init

	#EXAMPLE: mpirun --preload-binary -n 4 --npernode 2 -H kiev0,kiev1,kiev2,kiev3,kiev0,kiev1,kiev2,kiev3 /dev/shm/dbcast `pwd`/hello /dev/shm/
fi
