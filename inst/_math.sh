#!/bin/bash

ROOTDIR="$( cd "$( dirname "${BASH_SOURCE[0]:-$0}" )/../" && pwd )"
cd ${ROOTDIR}
source ${ROOTDIR}/conf/t2hx.sh #impi

if [ ! -f ${ROOTDIR}/dep/fftw/bin/fftw-wisdom ]; then
	cd ${ROOTDIR}/dep/packages/
	wget http://fftw.org/fftw-3.3.4.tar.gz
	tar xzf fftw-3.3.4.tar.gz
	cd ./fftw-3.3.4/
	./configure --prefix=${ROOTDIR}/dep/fftw --disable-mpi --enable-openmp --enable-fortran --enable-sse2 CC=gcc CFLAGS="-O3 -march=native"
	make -j
	make install
	cd ${ROOTDIR}/
fi

if [ ! -f ${ROOTDIR}/dep/lapack/liblapack.a ]; then
	cd ${ROOTDIR}/dep/packages/
	wget http://www.netlib.org/lapack/lapack-3.8.0.tar.gz
	tar xzf lapack-3.8.0.tar.gz
	mv ./lapack-*/ ../lapack/
	cd ../lapack/
	ln -s make.inc.example make.inc
	sed -i -e 's#-O3#-O3 -march=native#' -e 's#-O2#-O2 -march=native#' ./make.inc
	make -j
	cd ${ROOTDIR}/
fi

if [ ! -f ${ROOTDIR}/dep/scalapack/libscalapack.a ]; then
	cd ${ROOTDIR}/dep/packages/
	wget http://www.netlib.org/scalapack/scalapack.tgz
	tar xzf scalapack.tgz
	mv ./scalapack-*/ ../scalapack/
	cd ../scalapack/
	ln -s ./SLmake.inc.example SLmake.inc
	sed -i -e 's#-O3#-O3 -march=native#' ./SLmake.inc
	make -j
	cd ./BLACS/
	make
	cd ../SRC
	make
	cd ../PBLAS/SRC
	make
	cd ../../TOOLS
	make
	cd ../REDIST/SRC
	make
	cd ${ROOTDIR}/
fi

if [ ! -f ${ROOTDIR}/dep/blas/blas_LINUX.a ]; then
	cd ${ROOTDIR}/dep/packages/
	wget http://www.netlib.org/blas/blas-3.8.0.tgz
	tar xzf blas-3.8.0.tgz
	mv ./BLAS-*/ ../blas
	cd ../blas/
	sed -i -e 's#-O3#-O3 -march=native#' ./make.inc
	make -j
	cd ${ROOTDIR}/
fi

if [ ! -f ${ROOTDIR}/dep/xerces/lib/libxerces-c.a ]; then
	cd ${ROOTDIR}/dep/packages/
	wget http://ftp.riken.jp/net/apache//xerces/c/3/sources/xerces-c-3.2.2.tar.gz
	tar xzf xerces-c-3.2.2.tar.gz
	mv ./xerces-*/ ../xerces/
	cd ../xerces/
	./configure --prefix=`pwd` --disable-shared --disable-network --enable-transcoder-gnuiconv
	sed -i -e 's#-O2#-O2 -march=native#' ./Makefile
	make -j
	make install
	cd ${ROOTDIR}/
fi

