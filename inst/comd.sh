#!/bin/bash

ROOTDIR="$( cd "$( dirname "${BASH_SOURCE[0]:-$0}" )/../" && pwd )"
cd ${ROOTDIR}
source ${ROOTDIR}/conf/t2hx.sh #intel

BM="CoMD"
VERSION="3d48396b77ca8caa3124bc2391f9139c3ffb556c"
if [ ! -f ${ROOTDIR}/${BM}/bin/CoMD-openmp-mpi ]; then
	cd ${ROOTDIR}/${BM}/
	git checkout -b t2hx ${VERSION}
	for P in `ls ${ROOTDIR}/patches/${BM}/`; do
		git apply --check ${ROOTDIR}/patches/${BM}/${P}
		if [ "x$?" = "x0" ]; then git am --ignore-whitespace < ${ROOTDIR}/patches/${BM}/${P}; fi
	done
	cd ./src-openmp/
	sed -i -e '/STARTSDE/d' -e '/STOPSDE/d' -e '/ittnotify.h/d' ./CoMD.c
	sed -i -e 's#-ipo -xHost#-march=native#' -e 's# -I${ADVISOR_2018_DIR}/include##' -e 's# -L${ADVISOR_2018_DIR}/lib64 -littnotify##' ./Makefile
	make
	cd ${ROOTDIR}
fi

