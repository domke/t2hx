#!/bin/bash

ROOTDIR="$( cd "$( dirname "${BASH_SOURCE[0]}" )/../" && pwd )"
cd ${ROOTDIR}
source ${ROOTDIR}/conf/t2hx.sh intel

BM="HPL"
if [ ! -f ${ROOTDIR}/${BM}/bin/Linux_Intel64/xhpl ]; then
	mkdir -p ${ROOTDIR}/${BM}/
	cd ${ROOTDIR}/${BM}/
	wget http://www.netlib.org/benchmark/hpl/hpl-2.2.tar.gz
	tar xzf ./hpl-2.2.tar.gz -C ${ROOTDIR}/${BM} --strip-components 1
	for P in `ls ${ROOTDIR}/patches/${BM}/`; do
		patch -p1 < ${ROOTDIR}/patches/${BM}/${P}
	done
	sed -i -e '/STARTSDE/d' -e '/STOPSDE/d' -e '/ittnotify.h/d' ./testing/ptest/HPL_pddriver.c
	sed -i -e '/STARTSDE/d' -e '/STOPSDE/d' -e '/ittnotify.h/d' ./testing/ptest/HPL_pdtest.c
	sed -i -e "s#\$(HOME)/hpl#${ROOTDIR}/${BM}#g" -e 's/MPinc) /MPinc) #/' -e 's/MPlib) /MPlib) #/' -e 's/mpiicc/mpicc/' -e 's/ -mt_mpi//' -e 's/-fopenmp/-qopenmp-link=static/' -e 's/O3 -ipo/O3 -qopenmp -ipo/' ./Make.Linux_Intel64
	make arch=Linux_Intel64
	cd ${ROOTDIR}
fi

