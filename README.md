# TSUBAME2 HyperX experiment: benchmarks, tools, and logs

We developed a framework of scripts and git submodules to manage the R&D of the
routing engine, to set up the benchmarking infrastructure, and to perform the
measurements. After cloning our repository https://gitlab.com/domke/t2hx (or
downloading the artifacts from https://doi.org/10.5281/zenodo.3375075), one has
access to all benchmarks (see Table 2 of the paper), patches, and scripts.
Only minor modifications to the configuration files should be necessary, such as
changing host names, or adjustments to different node counts for the benchmarks,
before testing on another system. If users deviate from our OS version (CentOS
Linux release 7.4.1708) then some additional changes might be required.

## License
BSD-3-Clause

## Cloning this repo
```
git clone --recurse-submodules https://gitlab.com/domke/t2hx.git
# for devs: git@gitlab.com:domke/t2hx.git
```

## OS-level dependencies (based on CentOS Linux release 7.4.1708)
```
cmake autoconf automake libtool cpupowerutils screen pdsh systemd-devel libnl3-devel
valgrind-devel bison flex gcc gcc-c++ gcc-gfortran wget libudev-devel zlib-devel
libstdc++-devel pciutils tcl tcl-devel tk glib2-devel kernel-devel vim rpm-build
pkgconfig make python2-numpy numactl-devel lsof psmisc git swig python-devel
```

## Required access rights
- **root** when changing IB routing with included subnet manager
- maybe **root** when interfacing with the admin node which runs PARX routing
- user level for benchmarking with other/existing routing engines

## HowTo use this framework (follow these instructions):
### Installation:
- `./inst/_init.sh routing` on a admin node, which will run OpenSM with PARX (req. **root**)
- `./inst/_init.sh` on one compute node with access to parallel FS or NFS
### Generation of host lists:
- modify and/or re-run `./inst/_gethostlists.sh` to create host lists
### Compile each benchmark:
- execute `./inst/*.sh` for all files (except those starting with underscore)
### Adjust configuration if necessary:
- change path to Intel compiler: conf/intel.cfg
- change input/#nodes/etc. per benchmark if desired: conf/*.cfg
- select topology/routing/placement to test different network: conf/t2hx.sh
- change RUNMODE when switching from capability to capacity runs: conf/t2hx.sh
- change NumOMP, HXSMHOST, OSM0TRIGGER, etc. if needed: conf/t2hx.sh
### Running all benchmarks:
- execute `./run/*.sh` for every benchmark

## Hints:
- if cloned directory is not in home/NFS or parallel FS, then additional steps will be needed
- install scripts set benchmark version (via git commit hash) to exact version used in the paper
- setup assumes mlx4_* naming of HCA, and 1st HCA is attached to Fat-Tree, and 2nd to HyperX
- further changes may be needed (to conf/* and run/*) if the statement above is not true

## Citing the HyperX experiment
Publications re-using (parts of) this repo and/or citing our work should refer to:

>J. Domke, S. Matsuoka, I.R. Ivanov, Y. Tsushima, T. Yuki, A. Nomura, S. Miura, N. McDonald, D.L. Floyd, N. Dube, "HyperX Topology: First at-scale Implementation and Comparison to the Fat-Tree," in Proceedings of the International Conference for High Performance Computing, Networking, Storage and Analysis, SC ’19, (Piscataway, NJ, USA), IEEE Press, Nov. 2019.

Bibtex Entry:

```bibtex
@inproceedings{domke_hyperx_2019,
    address = {Piscataway, NJ, USA},
    series = {{SC} '19},
    title = {{HyperX} {Topology}: {First} {At}-{Scale} {Implementation} and {Comparison} to the {Fat}-{Tree}},
    isbn = {978-1-4503-6229-0},
    doi = {10.1145/3295500.3356140},
    booktitle = {Proceedings of the {International} {Conference} for {High} {Performance} {Computing}, {Networking}, {Storage} and {Analysis}},
    publisher = {IEEE Press},
    author = {Domke, Jens and Matsuoka, Satoshi and Ivanov, Ivan R. and Tsushima, Yuki and Yuki, Tomoya and Nomura, Akihiro and Miura, Shin'ichi and McDonald, Nic and Floyd, Dennis L. and Dub{\'e}, Nicolas},
    month = nov,
    year = {2019},
    note = {Artifacts: https://doi.org/10.5281/zenodo.3375075},
    keywords = {network topology, routing, InfiniBand, HyperX, Fat-Tree, PARX}
}
```

## Development Team
Authored and maintained by Jens Domke ([@contact](http://domke.gitlab.io/#contact)) with help from Ivan R. Ivanov, Yuki Tsushima, and other.
