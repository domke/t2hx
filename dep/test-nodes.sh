#!/bin/bash

SCRIPT="${BASH_SOURCE[0]:-$0}"
ROOTDIR="$(cd "$(dirname ${SCRIPT})/../" && pwd)"
source ${ROOTDIR}/conf/t2hx.sh #intel

if ! [[ $1 = *"locally"* ]]; then
	pdsh -R ssh -w ^${ROOTDIR}/conf/hosts.full ${ROOTDIR}/dep/`basename ${SCRIPT}` locally 2> /dev/shm/na
	cat /dev/shm/na | /usr/bin/grep 'No route\|not known'
else
	### perform local tests ###

	# is home mounted properly?
	if ! /usr/bin/mount | /usr/bin/grep home > /dev/null 2>&1 ; then
		echo -e "${YEL}home not mounted${NCO}"
	fi

	# verify that IB stack is running
	if ! /usr/bin/systemctl status rdma | /usr/bin/grep active > /dev/null 2>&1 ; then
		echo -e "${YEL}rdma.service not or not porperly running${NCO}"
	fi

	# testing Fat-Tree HCA
	if ! /usr/sbin/ibstat mlx4_0 1 | /usr/bin/grep LinkUp > /dev/null 2>&1 ; then
		if ! /usr/sbin/ibstat mlx4_0 2 | /usr/bin/grep LinkUp > /dev/null 2>&1 ; then
			echo -e "${YEL}1st IB HCA without active ports or missing${NCO}"
		elif /usr/sbin/ibstat mlx4_0 2 | /usr/bin/grep '^SM.* 0$'  > /dev/null 2>&1 ; then
			echo -e "${YEL}1st IB HCA without connection to SM${NCO}"
		elif /usr/sbin/ibstat mlx4_0 2 | /usr/bin/grep '^Base.* 0$' > /dev/null 2>&1 ; then
			echo -e "${YEL}1st IB HCA without LID${NCO}"
		fi
	elif /usr/sbin/ibstat mlx4_0 1 | /usr/bin/grep '^SM.* 0$'  > /dev/null 2>&1 ; then
		echo -e "${YEL}1st IB HCA without connection to SM${NCO}"
	elif /usr/sbin/ibstat mlx4_0 1 | /usr/bin/grep '^Base.* 0$' > /dev/null 2>&1 ; then
		echo -e "${YEL}1st IB HCA without LID${NCO}"
	fi

	# testing HyperX HCA
	if ! /usr/sbin/ibstat mlx4_1 1 | /usr/bin/grep LinkUp > /dev/null 2>&1 ; then
		if ! /usr/sbin/ibstat mlx4_1 2 | /usr/bin/grep LinkUp > /dev/null 2>&1 ; then
			echo -e "${YEL}2nd IB HCA without active ports or missing${NCO}"
		elif /usr/sbin/ibstat mlx4_1 2 | /usr/bin/grep '^SM.* 0$'  > /dev/null 2>&1 ; then
			echo -e "${YEL}2nd IB HCA without connection to SM${NCO}"
		elif /usr/sbin/ibstat mlx4_1 2 | /usr/bin/grep '^Base.* 0$' > /dev/null 2>&1 ; then
			echo -e "${YEL}2nd IB HCA without LID${NCO}"
		fi
	elif /usr/sbin/ibstat mlx4_1 1 | /usr/bin/grep '^SM.* 0$'  > /dev/null 2>&1 ; then
		echo -e "${YEL}2nd IB HCA without connection to SM${NCO}"
	elif /usr/sbin/ibstat mlx4_1 1 | /usr/bin/grep '^Base.* 0$' > /dev/null 2>&1 ; then
		echo -e "${YEL}2nd IB HCA without LID${NCO}"
	fi

	# do we have all 58GB RAM or more? (meminfo shows kB)
	if [ `cat /proc/meminfo | /usr/bin/grep MemTotal | awk '/[0-9]+/{print $2}'` -lt 52428800 ]; then
		echo -e "${YEL}available memory smaller than expected${NCO}"
	fi

	# check cpu, number cores and freq
	if [ `/usr/bin/lscpu -a -e | /usr/bin/grep yes | /usr/bin/sed -e "s/[[:space:]]\+/ /g" | wc -l` -lt 24 ]; then
		echo -e "${YEL}cpu cores missing${NCO}"
	fi
	if [ `/usr/bin/lscpu -a -e | /usr/bin/grep yes | /usr/bin/sed -e "s/[[:space:]]\+/ /g" | cut -d ' ' -f7 | /usr/bin/sort -u -n | head -1 | /usr/bin/sed -e "s/\.[0-9]*//g"` -lt 2900 ]; then
		echo -e "${YEL}at least one core with max freq below 2.9Ghz${NCO}"
	fi
	if [ `/usr/bin/lscpu -a -e | /usr/bin/grep yes | /usr/bin/sed -e "s/[[:space:]]\+/ /g" | cut -d ' ' -f8 | /usr/bin/sort -u -n | head -1 | /usr/bin/sed -e "s/\.[0-9]*//g"` -lt 1500 ]; then
		echo -e "${YEL}at least one core with min freq below 1.6Ghz${NCO}"
	fi
fi
