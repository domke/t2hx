#!/bin/bash

SCRIPT="${BASH_SOURCE[0]:-$0}"
ROOTDIR="$(cd "$(dirname ${SCRIPT})/../" && pwd)"

pdsh -R ssh -w ^${ROOTDIR}/conf/hosts.full -x `/usr/bin/hostname -s` "/usr/bin/pkill -u `/usr/bin/whoami`"
