#!/usr/bin/env python

from sys import argv, exit
from os import path, remove, makedirs
from math import sqrt
from distutils import dir_util
from time import sleep
from subprocess import Popen, PIPE
from shlex import split as shsplit
from platform import node as hostname
from re import compile

class FakeSubp:
    returncode = 0

def exec_command(cmd, subp=None, wait=True, relaunch_on_error=False, ok_ret=0):
    if None != subp:
        subp.poll()
        if None == subp.returncode:
            return (subp, cmd, None)
        else:
            out, err = subp.stdout.read(), subp.stderr.read()
            ret = out.decode('utf8') + err.decode('utf8')
    if None == subp or (relaunch_on_error and ok_ret != subp.returncode):
        if None != subp:
             print 'WRN: last run of %s terminated with (returncode=%s):\n  Msg: %s' % (cmd, subp.returncode, ret)
        try:
            subp = Popen(shsplit(cmd), stdout=PIPE, stderr=PIPE, stdin=PIPE)
            if wait:
                out, err = subp.communicate()
                ret = out.decode('utf8') + err.decode('utf8')
                if ok_ret != subp.returncode:
                    print 'WRN: blocking call %s terminated with (returncode=%s):\n  Msg: %s' % (cmd, subp.returncode, ret)
            else:
                ret = None
        except OSError as e:
            raise RuntimeError("Command %s failed: %s" % (cmd, e))
    return (subp, cmd, ret)

def get_send_to_host(rank2host, peers, idx):
    ip_re = compile('^\s*(\d+\.\d+\.\d+\.)(\d+)')
    if len(peers) > 0:
        sendToHost = rank2host[peers.pop(0)]
        _, _, out = exec_command('getent hosts %s' % sendToHost, relaunch_on_error=True)
        for line in out.splitlines():
            if ip_re.match(line):
                m = ip_re.match(line)
                sendToHost = "%s%s" % (m.group(1), int(m.group(2)) + idx)
                break
    else:
        sendToHost = None
    return (sendToHost, peers)

if len(argv) != 4:
    exit('ERR: too many or too few arguments\nUsage: %s srcDIR dstDIR hostfile' % argv[0])

srcDIR = path.abspath(argv[1])
dstDIR = path.abspath(argv[2])
hostfile = path.abspath(argv[3])

if not path.exists(hostfile):
    exit('ERR: hostfile missing')

ip_re = compile('^\s*(\d+\.\d+\.\d+\.)(\d+)')
doneMSG = path.join(dstDIR, 'CopyDone')
#if path.exists(doneMSG): remove(doneMSG)
#dir_util.mkpath(dstDIR)

r = 0
rank = None
rank2host = {}
with open(hostfile, 'r') as f:
    for line in f:
        line = line.strip()
        if hostname().startswith(line):
            rank = r
            _, _, out = exec_command('getent hosts %s' % line, relaunch_on_error=True)
            for line2 in out.splitlines():
                if ip_re.match(line2):
                    m = ip_re.match(line2)
                    myib0 = "%s%s" % (m.group(1), int(m.group(2)) + 0)
                    myib1 = "%s%s" % (m.group(1), int(m.group(2)) + 1)
                    myib2 = "%s%s" % (m.group(1), int(m.group(2)) + 2)
                    myib3 = "%s%s" % (m.group(1), int(m.group(2)) + 3)
                    break
 !!! fix host name here
        if 'k' == line[0]:
            rank2host[r] = line
            r += 1
csize = r

if None == rank:
    exit('ERR: cannot find myself (%s) in the hostlist; hence %s is incomplete' % (hostname(), hostfile))

comm_pair = csize*[0]
comm_pairs = []
for x in range(csize): comm_pairs.append(comm_pair[:])
sendTo = [x for x in range(1, csize)]
doneTo = [0]
for step in range(csize):
    for sRank in range(len(doneTo)):
        c = 1
        while c > 0 and len(sendTo) > 0:
            rRank = sendTo.pop(0)
            comm_pairs[doneTo[sRank]][rRank] = 1
            doneTo.append(rRank)
            c -= 1

if rank == 0:
    if not path.exists(srcDIR):
        exit('ERR: srcDIR not accessible')
    # copy from lustre to local /tmp
    dir_util.copy_tree(srcDIR, dstDIR)
else:
    # wait for a copy to show up
    while not path.exists(doneMSG):
        sleep(5)
    remove(doneMSG)

#fromHost, _ = get_send_to_host(rank2host={0: hostname().split('.')[0]}, peers=[0], idx=0)

peers = [sendTo for sendTo in range(1, csize) if 0 != comm_pairs[rank][sendTo]]
while len(peers) > 0:
    sendToHost0, peers = get_send_to_host(rank2host, peers, 0)
    sendToHost1 = None #, peers = get_send_to_host(rank2host, peers, 1)
    sendToHost2 = None #, peers = get_send_to_host(rank2host, peers, 2)
    sendToHost3 = None #, peers = get_send_to_host(rank2host, peers, 3)
    print 'from %s to %s %s %s %s' %(hostname(), sendToHost0, sendToHost1, sendToHost2, sendToHost3)

    # send real data
    if sendToHost0:
        #cmd0 = 'rsync -q -a --skip-compress=gz --partial --recursive -e "/bb/system/uge/latest/bin/lx-amd64/qrsh -inherit" %s/ %s:%s' % (dstDIR, sendToHost0, dstDIR)
	cmd0 = 'rsync -q -a --skip-compress=gz --partial --recursive -e "ssh -o StrictHostKeyChecking=no -o Compression=no -c aes128-ctr -T -x" %s/ %s:%s' % (dstDIR, sendToHost0, dstDIR)
        cp0, cmd0, out0 = exec_command(cmd0, wait=False)
    else: cp0 = FakeSubp()

    if sendToHost1:
        cmd1 = 'rsync -q -a --skip-compress=gz --partial --recursive -e "/bb/system/uge/latest/bin/lx-amd64/qrsh -inherit" %s/ %s:%s' % (dstDIR, sendToHost1, dstDIR)
        cp1, cmd1, out1 = exec_command(cmd1, wait=False)
    else: cp1 = FakeSubp()

    if sendToHost2:
        cmd2 = 'rsync -q -a --skip-compress=gz --partial --recursive -e "/bb/system/uge/latest/bin/lx-amd64/qrsh -inherit" %s/ %s:%s' % (dstDIR, sendToHost2, dstDIR)
        cp2, cmd2, out2 = exec_command(cmd2, wait=False)
    else: cp2 = FakeSubp()

    if sendToHost3:
        cmd3 = 'rsync -q -a --skip-compress=gz --partial --recursive -e "/bb/system/uge/latest/bin/lx-amd64/qrsh -inherit" %s/ %s:%s' % (dstDIR, sendToHost3, dstDIR)
        cp3, cmd3, out3 = exec_command(cmd3, wait=False)
    else: cp3 = FakeSubp()

    another_round, c = True, 0
    # try for at least 1h
    while another_round and c < 360:
        sleep(10)
        c += 1
        print "round %s for host %s" % (c ,hostname())
        if 0 != cp0.returncode:
            cp0, cmd0, out0 = exec_command(cmd0, subp=cp0, wait=False, relaunch_on_error=True)
        if 0 != cp1.returncode:
            cp1, cmd1, out1 = exec_command(cmd1, subp=cp1, wait=False, relaunch_on_error=True)
        if 0 != cp2.returncode:
            cp2, cmd2, out2 = exec_command(cmd2, subp=cp2, wait=False, relaunch_on_error=True)
        if 0 != cp3.returncode:
            cp3, cmd3, out3 = exec_command(cmd3, subp=cp3, wait=False, relaunch_on_error=True)
        if 0 == cp0.returncode and 0 == cp1.returncode and 0 == cp2.returncode and 0 == cp3.returncode:
            another_round = False

    # send 'done' info
    if sendToHost0:
        #cmd0 = '/bb/system/uge/latest/bin/lx-amd64/qrsh -inherit %s "touch %s"' % (sendToHost0, doneMSG)
	cmd0 = 'ssh -o StrictHostKeyChecking=no %s "touch %s"' % (sendToHost0, doneMSG)
        _, _, _ = exec_command(cmd0, relaunch_on_error=True)

    if sendToHost1:
        cmd1 = '/bb/system/uge/latest/bin/lx-amd64/qrsh -inherit %s "touch %s"' % (myib1, sendToHost1, doneMSG)
        _, _, _ = exec_command(cmd1, relaunch_on_error=True)

    if sendToHost2:
        cmd2 = '/bb/system/uge/latest/bin/lx-amd64/qrsh -inherit %s "touch %s"' % (myib2, sendToHost2, doneMSG)
        _, _, _ = exec_command(cmd2, relaunch_on_error=True)

    if sendToHost3:
        cmd3 = '/bb/system/uge/latest/bin/lx-amd64/qrsh -inherit %s "touch %s"' % (myib3, sendToHost3, doneMSG)
        _, _, _ = exec_command(cmd3, relaunch_on_error=True)

if path.exists(doneMSG):
    remove(doneMSG)

exit(0)
