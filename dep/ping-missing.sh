#!/bin/bash

trap cleanup 2 11 15

cleanup() {
	exit 0
}

YEL='\033[1;33m'
NCO='\033[0m'

SCRIPT="${BASH_SOURCE[0]:-$0}"
ROOTDIR="$(cd "$(dirname ${SCRIPT})/../" && pwd)"

for x in `cat ${ROOTDIR}/conf/hosts.full | /usr/bin/grep -v '^#'`; do
	if ! ping -c1 ${x} > /dev/null; then echo -e "${YEL}${x}${NCO}"; fi;
done
