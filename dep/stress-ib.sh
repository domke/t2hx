#!/bin/bash

if [ -z "${STY}" ]; then echo 'ERR: please run within screen session; or remove check'; exit; fi
if [ -z "$1" ]; then echo 'ERR: need input parameter which specifies topology'; exit; fi
if ! [[ "$1" = *"tree"* ]] && ! [[ "$1" = *"hyperx"* ]]; then echo 'ERR: input must be either fattree or hyperx'; exit; fi
NOW="`date +'%F_%Hh%Mm%Ss.%N'`"
SCRIPT="${BASH_SOURCE[0]:-$0}"
BM="imb"
ROOTDIR="$(cd "$(dirname ${SCRIPT})/../" && pwd)"

cd ${ROOTDIR}
source ${ROOTDIR}/conf/t2hx.sh #intel
source ${ROOTDIR}/conf/${BM}.cfg single
export ALGOS="Alltoall"
export SEQMAX="14"
export NumRunsTEST="1"
export TOPOLOGY="$1"
export HL="full"
export TESTCONF="`/usr/bin/grep '^r' ${ROOTDIR}/conf/hosts.${HL} | /usr/bin/wc -l`"
unset PLACEMENT
unset APPID
ulimit -s unlimited
ulimit -n 4096

DEFINPUT=${INPUT}
cd ${APPDIR}
set -x; pdcp -R ssh -w ^${ROOTDIR}/conf/hosts.${HL} ${BINARY} /dev/shm/; set +x
for TOPO in ${TOPOLOGY}; do
	if [[ ${TOPO} = *"tree"* ]]; then HCA=mlx4_0; else HCA=mlx4_1; fi
	for ALGO in ${ALGOS}; do
		LOG="${ROOTDIR}/log/${SCRIPT}.log"
		mkdir -p `dirname ${LOG}`
		env >> ${LOG} 2>&1
		for NumMPI in ${TESTCONF}; do
			MPIEXECOPT="`echo ${MPI0EXECOPT} | sed -e \"s/HCA/${HCA}/\" -e \"s/LIST/${HL}/\"`"
			# ============================ pre-processing =========
			INPUT="`echo ${DEFINPUT} | sed -e \"s/NNODES/${NumMPI}/\" -e \"s/ALGO/${ALGO}/\" -e \"s/iter 100/iter 1/\"`"
			echo "2^${SEQMAX}" | bc -l > ./msglen.txt
			# ============================ pre-processing =========
			for i in `seq 1 ${NumRunsTEST}`; do
				echo "mpirun ${MPIEXECOPT} -np ${NumMPI} /dev/shm/`basename ${BINARY}` ${INPUT}" | tee -a ${LOG}
				START="`date +%s.%N`"
				timeout --kill-after=30s ${MAXTIME} mpirun ${MPIEXECOPT} -np ${NumMPI} /dev/shm/`basename ${BINARY}` ${INPUT} >> ${LOG} 2>&1
				if [ "x$?" = "x124" ] || [ "x$?" = "x137" ]; then echo -e "${YEL}Killed after exceeding ${MAXTIME} timeout${NCO}" | tee -a ${LOG}; fi
				ENDED="`date +%s.%N`"
				echo -e "${GRE}Total running time: `echo \"${ENDED} - ${START}\" | bc -l`${NCO}" | tee -a ${LOG}
			done
			# ============================ post-processing ========
			rm -f ./msglen.txt
			# ============================ post-processing ========
		done
	done
done
cd ${ROOTDIR}
set -x; pdsh -R ssh -w ^${ROOTDIR}/conf/hosts.${HL} "rm -f /dev/shm/`basename ${BINARY}`"; set +x
