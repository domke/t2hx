#!/usr/bin/env python
from sys import exit, argv
from os import path
from numpy import random, arange
from ClusterShell.NodeSet import NodeSet

#arg1: 'g' or 'r' for grouped/geometric or random
#arg2: num nodes in output
#arg3: short list/string of nodes to exclude, or
#      file with long list of nodes to exclude, one node per line

METHODE = argv[1]
NNODES = int(argv[2])
T2HX = list(NodeSet('r[10-13]n[00-11,16-31],r[20-23]n[00-11,16-31],r[30-33]n[00-11,16-31],r[40-43]n[00-11,16-31],r[50-53]n[00-11,16-31],r[60-63]n[00-11,16-31]'))
if len(argv) > 3:
	if path.isfile(argv[3]):
		with open(argv[3], 'r') as f:
			for node in f:
				if node.startswith('r'):
					T2HX.remove(node.rstrip())
	else:
		NOTAVAIL = argv[3]
		for node in NOTAVAIL.split(','):
			T2HX.remove(node)

if NNODES > len(T2HX):
	exit('ERR: not enough nodes')

O=[]
random.seed(0)
if METHODE=='g':
	c = -1
	for x in random.geometric(p=0.8, size=NNODES):
		c += x
		if c >= len(T2HX):
			c = 0
			for node in O:
				if node in T2HX:
					T2HX.remove(node)
		O.append(T2HX[c])

elif METHODE=='r':
	arr = arange(len(T2HX))
	random.shuffle(arr)
	for x in range(NNODES):
		O.append(T2HX[arr[x]])

O.sort()
# we wired a bit strange with rXXn16 and rXXn18 on one switch and 17+19 on the other
for R1 in range(1,7):
	for R2 in range(0,4):
		if O.count('r%s%sn17'%(R1,R2))==0 or O.count('r%s%sn18'%(R1,R2))==0:
			continue
		i1, i2 = O.index('r%s%sn17' % (R1,R2)), O.index('r%s%sn18' % (R1,R2))
		O[i1], O[i2] = O[i2], O[i1]
for x in O:
	print(x)
