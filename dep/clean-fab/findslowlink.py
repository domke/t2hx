#!/usr/bin/env python3

from sys import argv, exit
from sys import path as syspath
from os import path
syspath.append(path.join(path.dirname(path.realpath(__file__)), '..', 'parseIB.git'))
from ParseInfiniband.ibdiagnet import read_lst, read_pm

def read_sw_map(map_path):
    sw_map = {}
    try:
        with open(map_path, 'r') as map_file:
            for line in map_file:
                line = line.rstrip().split(',')
                if len(line) < 4:
                    # ignore invalid line
                    continue
                sw_map[int(line[0],16)] = {'Name': line[1], 'Rack': line[2], 'Topo': line[3]}
            return sw_map
    except FileNotFoundError:
        exit('Mapping file %s not found.\n' % map_path)

try:
    lst_file = argv[1]
except:
    pass
if len(argv) != 2 or not path.isfile(lst_file):
    exit("Usage:\t%s ./ibdiagnet2.lst\n\t(use ibdiagnet -o <path> to get inputs)" % path.basename(__file__))

sw_map = read_sw_map(path.join(path.dirname(path.realpath(__file__)), 'sw-map.cfg'))
ibd_net, ibd_sw, ibd_ca = read_lst(lst_file, False, 'Ports,Name,LinkSpeed')

for guid in ibd_sw:
    for port in ibd_net[guid]['PN']:
        nguid = ibd_net[guid]['PN'][port]['NodeGUID']
        nport = ibd_net[guid]['PN'][port]['PN']
        nspeed = ibd_net[guid]['PN'][port]['LinkSpeed']
        if nspeed != '4xQDR' and nguid in ibd_sw:
            print('slow link: 0x%016x %s (%s) <-> 0x%016x %s (%s)' % (int(guid,16), port, list(sw_map[int(guid,16)].values()), int(nguid,16), nport, list(sw_map[int(nguid,16)].values())))
        elif nspeed != '4xQDR':
            print('slow link: 0x%016x %s (%s) <-> CA %s' % (int(guid,16), port, list(sw_map[int(guid,16)].values()), ibd_net[nguid]['Name']))
