#!/usr/bin/env python3

from sys import argv, exit
from sys import path as syspath
from os import path
from re import compile
from numpy import linalg, array, array_equal, zeros, transpose
syspath.append(path.join(path.dirname(path.realpath(__file__)), '..', 'parseIB.git'))
from ParseInfiniband.ibnetdiscover import read as read_ibnet

def read_sw_map(map_path):
    sw_map = {}
    try:
        with open(map_path, 'r') as map_file:
            for line in map_file:
                line = line.rstrip().split(',')
                if len(line) < 4:
                    # ignore invalid line
                    continue
                sw_map[int(line[0],16)] = {'Name': line[1], 'Rack': line[2], 'Topo': line[3]}
            return sw_map
    except FileNotFoundError:
        exit('Mapping file %s not found.\n' % map_path)

if len(argv) != 2 or not path.isfile(argv[1]):
    exit("Usage:\t%s ./topo.ibnet\n\t(use ibnetdiscover > <path> to get input)" % path.basename(__file__))

t2_net, t2_sw_l, t2_ca_l = read_ibnet(argv[1], False, 'NodeID,Name,LinkSpeed')
hx_net, hx_sw_l, hx_ca_l = read_ibnet(path.join(path.dirname(path.realpath(__file__)), 'hx.expected'), False, 'NodeID,Name,LinkSpeed')
sw_map = read_sw_map(path.join(path.dirname(path.realpath(__file__)), 'sw-map.cfg'))
#fix switch and ca NodeIDs
for t2_sw in t2_sw_l:
    t2_net[t2_sw]['NodeID'] = sw_map[int(t2_net[t2_sw]['NodeID'].split('-')[1], 16)]['Name']
for t2_ca in t2_ca_l:
    t2_net[t2_ca]['NodeID'] = t2_net[t2_ca]['Name'].split(' ')[0]

for hx_sw in hx_sw_l+hx_ca_l:
    found_sw = False
    for t2_sw in t2_sw_l+t2_ca_l:
        if hx_net[hx_sw]['NodeID'] == t2_net[t2_sw]['NodeID']:
            found_sw = True
            found_rem_sw = False
            for hx_port in hx_net[hx_sw]['PN']:
                for t2_port in t2_net[t2_sw]['PN']:
                    #print(hx_net[hx_sw]['NodeID'], t2_net[t2_sw]['NodeID'])
                    if hx_net[hx_net[hx_sw]['PN'][hx_port]['NodeGUID']]['NodeID'] == t2_net[t2_net[t2_sw]['PN'][t2_port]['NodeGUID']]['NodeID']:
                        found_rem_sw = True
                        if hx_net[hx_sw]['PN'][hx_port]['LinkSpeed'] != t2_net[t2_sw]['PN'][t2_port]['LinkSpeed']:
                            print("Link speed mismatch (expected %s) for: %s %s" % (hx_net[hx_sw]['PN'][hx_port]['LinkSpeed'], t2_net[t2_sw]['NodeID'], t2_net[t2_sw]['PN'][t2_port]))
                        break
                if not found_rem_sw:
                    print("Link missing: %s -- %s" % (hx_net[hx_sw]['NodeID'], hx_net[hx_net[hx_sw]['PN'][hx_port]['NodeGUID']]['NodeID']))
            break
    if not found_sw and hx_sw in hx_sw_l:
        print("Switch %s missing from T2" % hx_net[hx_sw]['NodeID'])
    elif not found_sw and hx_sw in hx_ca_l:
        print("CA %s missing from T2" % hx_net[hx_sw]['NodeID'])

for hx_sw in hx_sw_l:
    sw_links, ca_links = [], []
    for hx_port in hx_net[hx_sw]['PN']:
        if hx_net[hx_net[hx_sw]['PN'][hx_port]['NodeGUID']]['NodeID'].startswith('IBEDGE'):
            sw_links.append(hx_net[hx_net[hx_sw]['PN'][hx_port]['NodeGUID']]['NodeID'])
        elif hx_net[hx_net[hx_sw]['PN'][hx_port]['NodeGUID']]['NodeID'].startswith('r'):
            ca_links.append(hx_net[hx_net[hx_sw]['PN'][hx_port]['NodeGUID']]['NodeID'])
        else:
            print("Unexpected remote NodeID found: %s -- %s" % (hx_net[hx_sw]['NodeID'], hx_net[hx_net[hx_sw]['PN'][hx_port]['NodeGUID']]['NodeID']))
    if len(set(sw_links)) != 7+11:
        print("HX: Missing or duplicated sw links attached to: %s" % hx_net[hx_sw]['NodeID'])
    if len(set(ca_links)) != 7:
        print("HX: Missing or duplicated ca links attached to: %s" % hx_net[hx_sw]['NodeID'])

for t2_sw in t2_sw_l:
    sw_links, ca_links = [], []
    hx_sw_links, hx_ca_links = [], []
    for hx_sw in hx_sw_l:
        if hx_net[hx_sw]['NodeID'] == t2_net[t2_sw]['NodeID']:
            for hx_port in hx_net[hx_sw]['PN']:
                if hx_net[hx_net[hx_sw]['PN'][hx_port]['NodeGUID']]['NodeID'].startswith('IBEDGE'):
                    hx_sw_links.append(hx_net[hx_net[hx_sw]['PN'][hx_port]['NodeGUID']]['NodeID'])
            break
    for t2_port in t2_net[t2_sw]['PN']:
        if t2_net[t2_net[t2_sw]['PN'][t2_port]['NodeGUID']]['NodeID'].startswith('IBEDGE'):
            if t2_net[t2_net[t2_sw]['PN'][t2_port]['NodeGUID']]['NodeID'] in sw_links:
                print("Duplicated sw links attached to: %s -> %s" % (t2_net[t2_sw]['NodeID'], t2_net[t2_net[t2_sw]['PN'][t2_port]['NodeGUID']]['NodeID']))
            sw_links.append(t2_net[t2_net[t2_sw]['PN'][t2_port]['NodeGUID']]['NodeID'])
        elif t2_net[t2_net[t2_sw]['PN'][t2_port]['NodeGUID']]['NodeID'].startswith('r'):
            if t2_net[t2_net[t2_sw]['PN'][t2_port]['NodeGUID']]['NodeID'] in ca_links:
                print("Duplicated sw links attached to: %s -> %s" % (t2_net[t2_sw]['NodeID'], t2_net[t2_net[t2_sw]['PN'][t2_port]['NodeGUID']]['NodeID']))
            ca_links.append(t2_net[t2_net[t2_sw]['PN'][t2_port]['NodeGUID']]['NodeID'])
        else:
            print("Unexpected remote NodeID found: %s -- %s" % (t2_net[t2_sw]['NodeID'], t2_net[t2_net[t2_sw]['PN'][t2_port]['NodeGUID']]['NodeID']))
    if len(set(sw_links)) != 7+11:
        print("Missing or duplicated sw links attached to: %s (%d) (missing: %s; too many %s)" % (t2_net[t2_sw]['NodeID'], 7+11-len(set(sw_links)), set(hx_sw_links).difference(set(sw_links)), set(sw_links).difference(set(hx_sw_links))))
    if len(set(ca_links)) != 7:
        print("Missing or duplicated ca links attached to: %s (%d)" % (t2_net[t2_sw]['NodeID'], 7-len(set(ca_links))))

