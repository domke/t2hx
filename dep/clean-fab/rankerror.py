#!/usr/bin/env python3

from sys import argv, exit
from sys import path as syspath
from os import path
syspath.append(path.join(path.dirname(path.realpath(__file__)), '..', 'parseIB.git'))
from ParseInfiniband.ibdiagnet import read_lst, read_pm

def read_cfg(cfg_path):
    cfg = []
    try:
        with open(cfg_path, 'r') as cfg_file:
            for line in cfg_file:
                line = line.rstrip().split('=')
                if len(line) < 2:
                    # ignore invalid line
                    continue
                if line[1]=='0':
                    continue
                if line[1]=='1':
                    cfg.append(line[0])
            if len(cfg) < 1:
                exit('ERR: All config were 0. Cannot rank.\n')
            return cfg
    except FileNotFoundError:
        exit('Config file %s not found.\n' % cfg_path)

def read_sw_map(map_path):
    sw_map = {}
    try:
        with open(map_path, 'r') as map_file:
            for line in map_file:
                line = line.rstrip().split(',')
                if len(line) < 4:
                    # ignore invalid line
                    continue
                sw_map[int(line[0],16)] = {'Name': line[1], 'Rack': line[2], 'Topo': line[3]}
            return sw_map
    except FileNotFoundError:
        exit('Mapping file %s not found.\n' % map_path)

try:
    lst_file, pm_file = argv[1], argv[2]
except:
    pass
if len(argv) != 3 or not path.isfile(lst_file) or not path.isfile(pm_file):
    exit("Usage:\t%s ./ibdiagnet2.lst ./ibdiagnet2.pm\n\t(use ibdiagnet -pm -o <path> to get inputs)" % path.basename(__file__))

cfg = read_cfg(path.join(path.dirname(path.realpath(__file__)), 'pm-filter.cfg'))
sw_map = read_sw_map(path.join(path.dirname(path.realpath(__file__)), 'sw-map.cfg'))
ibd_net, ibd_sw, ibd_ca = read_lst(lst_file, False, 'Ports,Name')
ibd_pm = read_pm(pm_file, False, False, ','.join(cfg), 'Name,LID,Dev')
#for guid in ibd_net:
#    print(guid,ibd_net[guid])
#for pguid in ibd_pm:
#    print(pguid,ibd_pm[pguid])
if int(ibd_sw[0],16) in sw_map and 'FT' in sw_map[int(ibd_sw[0],16)]['Topo']:
    isFT = True
    lvls = ['EDGE', 'Line', 'Spine']
    for sw in ibd_sw:
        ibd_net[sw]['DisabledUp'] = []
        ibd_net[sw]['DisabledDown'] = []
        ibd_net[sw]['Up'] = 0
        ibd_net[sw]['Down'] = 0
        lvl = [x for x in range(len(lvls)) if lvls[x] in ibd_net[sw]['Name']][0]
        for port in ibd_net[sw]['PN']:
            nguid = ibd_net[sw]['PN'][port]['NodeGUID']
            try:
                nlvl = [x for x in range(len(lvls)) if lvls[x] in ibd_net[nguid]['Name']][0]
            except:
                nlvl = -1
                pass
            if lvl < nlvl or lvl == 2: ibd_net[sw]['Up'] += 1
            if lvl > nlvl:             ibd_net[sw]['Down'] += 1
else:
    isFT = False

print('#============== SUGGESTIONS FOR DISABLING PORTS ==============')
for cnt in cfg:
    if 0 == sum([ibd_pm[guid]['Ports'][port][cnt] for guid in ibd_pm for port in ibd_pm[guid]['Ports'] if cnt in ibd_pm[guid]['Ports'][port] and 0 < ibd_pm[guid]['Ports'][port][cnt]]):
            continue
    print('#================== %s ==================' % cnt)
    bad_cnt = []
    for guid in ibd_pm:
        for port in ibd_pm[guid]['Ports']:
            if cnt in ibd_pm[guid]['Ports'][port] and 0 < ibd_pm[guid]['Ports'][port][cnt]:
                bad_cnt.append([ibd_pm[guid]['Ports'][port][cnt], guid, port])
    for c, pguid, port in sorted(bad_cnt, reverse=True):
        #disable the bad port AND the reverse direction
        if not pguid in ibd_ca and not pguid in ibd_sw: guid = '%016x' % (int(pguid, 16) - 1)
        else:                                           guid = pguid
        if port in ibd_net[guid]['PN']:
            nguid = ibd_net[guid]['PN'][port]['NodeGUID']
            nport = ibd_net[guid]['PN'][port]['PN']
            if guid in ibd_ca or nguid in ibd_ca: continue
            if isFT and (port in ibd_net[guid]['DisabledUp'] or port in ibd_net[guid]['DisabledDown']): continue
            print('ibportstate -G 0x%016x %s disable\t#val=%s info: %s' % (int(guid,16), port, c, list(sw_map[int(guid,16)].values())))
            print('ibportstate -G 0x%016x %s disable\t#reverse info: %s' % (int(nguid,16), nport, list(sw_map[int(nguid,16)].values())))
            if isFT and not (port in ibd_net[guid]['DisabledUp'] or port in ibd_net[guid]['DisabledDown']):
                lvl = [x for x in range(len(lvls)) if lvls[x] in ibd_net[guid]['Name']][0]
                nlvl = [x for x in range(len(lvls)) if lvls[x] in ibd_net[nguid]['Name']][0]
                if lvl < nlvl:
                    ibd_net[guid]['DisabledUp'].append(port)
                    ibd_net[guid]['Up'] -= 1
                    ibd_net[nguid]['DisabledDown'].append(nport)
                    ibd_net[nguid]['Down'] -= 1
                elif lvl > nlvl:
                    ibd_net[nguid]['DisabledUp'].append(nport)
                    ibd_net[nguid]['Up'] -= 1
                    ibd_net[guid]['DisabledDown'].append(port)
                    ibd_net[guid]['Down'] -= 1
                else:
                    exit('ERR: network not a fat-tree')
                #up in directors we cant do much... and every Line have less than 1 down anyhow and Spines have all down
                if lvl == 2 or nlvl == 2: continue
                if lvl == 0 and ibd_net[guid]['Up'] < 14:
                    print('# WRN above ibportstate change will degrade up links of 0x%016x beyond expected limits' % int(guid,16))
                if nlvl == 0 and ibd_net[nguid]['Up'] < 14:
                    print('# WRN above ibportstate change will degrade up links of 0x%016x beyond expected limits' % int(nguid,16))
        else:
            print('ibportstate -G 0x%016x %s disable\t#no remote, val=%s info: %s' % (int(guid,16), port, c, list(sw_map[int(guid,16)].values())))
    for c, pguid, port in sorted(bad_cnt, reverse=True):
        if not pguid in ibd_ca and not pguid in ibd_sw: guid = '%016x' % (int(pguid, 16) - 1)
        else:                                           guid = pguid
        if port in ibd_net[guid]['PN']:
            nguid = ibd_net[guid]['PN'][port]['NodeGUID']
            nport = ibd_net[guid]['PN'][port]['PN']
            if not guid in ibd_ca and not nguid in ibd_ca: continue
            if guid in ibd_ca:
                print('#BAD CA PORT: ibportstate -G 0x%016x %s disable\t#val=%s info: %s' % (int(pguid,16), port, c, ibd_net[guid]['Name']))
                print('#BAD CA PORT: ibportstate -G 0x%016x %s disable\t#reverse info: %s' % (int(nguid,16), nport, list(sw_map[int(nguid,16)].values())))
            else:
                print('#BAD SW PORT: ibportstate -G 0x%016x %s disable\t#val=%s info: %s' % (int(pguid,16), port, c, list(sw_map[int(guid,16)].values())))
                print('#BAD SW PORT: ibportstate -G 0x%016x %s disable\t#reverse info: %s' % (int(nguid,16), nport, ibd_net[nguid]['Name']))
        else:
            print('ibportstate -G 0x%016x %s disable\t#no remote, val=%s info: %s' % (int(guid,16), port, c, list(sw_map[int(guid,16)].values())))
