#!/usr/bin/env python3

from sys import exit, stdout
from os import path, walk, sep, system
from re import compile, findall
from subprocess import run


globconf = path.join(path.dirname(path.realpath(__file__)), '..', 'conf', 't2hx.sh')
logroot = path.join(path.dirname(path.realpath(__file__)), '..', 'log')
outroot = path.join(path.dirname(path.realpath(__file__)), '..', 'conf', 'comm4parx')
parser = path.join(path.dirname(path.realpath(__file__)), '..', 'dep', 'parse-ibprof.py')

benchmarks = ['amg', 'comd', 'graph500', 'hpl', 'milc', 'ntchem', 'simdl', 'baidu', 'ffvc', 'hpcg', 'imb', 'minife', 'multip2p', 'netgauge', 'qball', 'swfft', 'mpigraph']

num_mpi_reg = compile('ibprofile(\.[A-Za-z]+)?\.n(\d+)\..*tgz')

for benchmark in benchmarks:
    for root, dirs, files in walk(path.join(logroot, benchmark)):
        if not 'profiles' == path.basename(root): continue
        for f in files:
            if num_mpi_reg.match(f):
                if 'imb' == benchmark or 'simdl' == benchmark or 'multip2p' == benchmark:
                    subbenchmark = num_mpi_reg.match(f).group(1)
                    num_mpi = num_mpi_reg.match(f).group(2)
                else:
                    subbenchmark = ''
                    num_mpi = num_mpi_reg.match(f).group(2)
                # extract
                intoDir = path.join(outroot, 'tmp', '%s%s.%s' % (benchmark, subbenchmark, num_mpi))
                system('/usr/bin/mkdir -p %s' % intoDir)
                system('/usr/bin/tar xzf %s -C %s --strip-components 2' % (path.join(root, f), intoDir))
                # parse
                system('bash -c "source %s; %s %s/*.otf > %s"' % (globconf, parser, intoDir, path.join(outroot, '%s%s.%s.csv' % (benchmark, subbenchmark, num_mpi))))

# ibprof output is missing data of rank 0, cannot be parsed, fallback to tau
benchmarks = ['mvmc']
parser = path.join(path.dirname(path.realpath(__file__)), '..', 'dep', 'parse-tau.py')
num_mpi_reg = compile('tauprofile\.n(\d+)\..*tgz')
for benchmark in benchmarks:
    for root, dirs, files in walk(path.join(logroot, benchmark)):
        if not 'profiles' == path.basename(root): continue
        for f in files:
            if num_mpi_reg.match(f):
                subbenchmark = ''
                num_mpi = num_mpi_reg.match(f).group(1)
                # extract
                intoDir = path.join(outroot, 'tmp', '%s%s.%s' % (benchmark, subbenchmark, num_mpi))
                system('/usr/bin/mkdir -p %s' % intoDir)
                system('/usr/bin/tar xzf %s -C %s --strip-components 2' % (path.join(root, f), intoDir))
                # parse
                system('bash -c "source %s; %s %s/ %s >/dev/null"' % (globconf, parser, intoDir, path.join(outroot, '%s%s.%s.csv' % (benchmark, subbenchmark, num_mpi))))
                system('bash -c "/usr/bin/sed -i -e \'s/\.[0-9]*,/,/g\' -e \'s/\.[0-9]*$//g\' %s"' % path.join(outroot, '%s%s.%s.csv' % (benchmark, subbenchmark, num_mpi)))

print('Done')
print('#you should manually: rm %s' % path.join(outroot, 'tmp'))
