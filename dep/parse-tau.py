#!/usr/bin/env python3
import sys
import os
import re
import types


def get_events(events_regex, fn):
    """
    events_regex: list of tuples (regex, type of event it matches)
    fn: profile file file name
    """
    events = []
    with open(fn, 'r') as f:
        lines = f.readlines()
        for line in lines:
            for (e, t) in events_regex:
                m = e.match(line)
                if not m:
                    continue
                ev = types.SimpleNamespace()
                ev._type = t
                if t != "sent_all":
                    ev._to = int(m.group(1))
                else:
                    ev._to = -1
                ev._eventfun = m.group(2)
                ev._num = int(m.group(3))
                ev._max = int(m.group(4))
                ev._min = int(m.group(5))
                ev._mean = float(m.group(6))
                ev._sumsqr = float(m.group(7))
                events.append(ev)
    return events

def parse_profile(fn, out, node_num, node_id):
    """
    Parses the profile file and outputs the result to a file

    fn: file name of the profile
    out: file to write output to
    node_num: total number of nodes
    node_id: the id of the node profile to be parsed
    """

    print('Parsing ' + fn)

    # "Message size sent to node 6 : MPI_Isend()  " 6 16 16 16 1536
    sent_event_n = re.compile(
        '"Message size sent to node (\w+)\s+:\s+(\S+)\s+"'
        + '\s+(\S+)\s+(\S+)\s+(\S+)\s+(\S+)\s+(\S+)\s*'
        )

    # "Message size sent to node 6" 6 16 16 16 1536
    sent_event = re.compile(
        '"Message size sent to node (\w+)()"'
        + '\s+(\S+)\s+(\S+)\s+(\S+)\s+(\S+)\s+(\S+)\s*'
        )

    # "Message size sent to all nodes" 1290 1640 0 178.6666666666667 155405952
    sent_event_all = re.compile(
        '"Message size sent to all nodes()()"'
        + '\s+(\S+)\s+(\S+)\s+(\S+)\s+(\S+)\s+(\S+)\s*'
        )

    events_regex = [
        (sent_event, "sent"),
        #(sent_event_n, "sent_n"),
        #(sent_event_all, "sent_all"),
    ]
    events = get_events(events_regex, fn)

    sent_events = events
    mean_to = [0 for _ in range(node_num)]
    for ev in sent_events:
        mean_to[ev._to] = ev._mean
    mean_to = map(str, mean_to)
    out.write(",".join(mean_to))
    out.write("\n")

if __name__ == '__main__':

    if len(sys.argv) < 3:
        print('Please pass input directory and output file')
        exit(1)

    in_dir = sys.argv[1]
    out_file = sys.argv[2]

    node_profiles = [(fn.split('.')[1], fn) for fn in os.listdir(in_dir)]
    node_profiles.sort()
    node_num = len(node_profiles)

    with open(out_file, 'w') as out:
        for (n, fn) in node_profiles:
            parse_profile(os.path.join(in_dir, fn), out, node_num, n)

