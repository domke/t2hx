#!/usr/bin/env python

from sys import exit, stdout
from os import path, walk, sep
from re import compile, findall
from argparse import ArgumentParser
from contextlib import contextmanager
from numpy import percentile, array2string


@contextmanager
def __smart_open(filename=None):
    if filename and filename != '-':
        fh = open(filename, 'w')
    else:
        fh = stdout
    try:
        yield fh
    finally:
        if fh is not stdout:
            fh.close()


def parse_logs(args):
    if not isinstance(args, dict):
        exit('ERR: invalid function parameter type(s)')

    bm_reg = {
        'nummpi': compile('^mpirun.*\s+-np\s+(\d+)\s+.*'),
        'ignore': compile('^mpirun.*(libTAU.so|libibprof.so).*'),
        'amg': compile('Walltime of the main kernel:\s+([-+]?[0-9]*\.?[0-9]+([eE][-+]?[0-9]+)?)\s+sec'),                        #[s]
        #'comd': compile('\s*Average atom rate:\s+([-+]?[0-9]*\.?[0-9]+([eE][-+]?[0-9]+)?)\s+atoms/us'),                         #[atoms/us]
        'comd': compile('Walltime of the main kernel:\s+([-+]?[0-9]*\.?[0-9]+([eE][-+]?[0-9]+)?)\s+sec'),                       #[s]
        'graph500': compile('median_TEPS:\s+([-+]?[0-9]*\.?[0-9]+([eE][-+]?[0-9]+)?)'),                                         #[median TEPS]
        'hpl': compile('WC00C2R2\s+\d+\s+\d+\s+\d+\s+\d+\s+(?:\d+(?:\.\d+)?)\s+([-+]?[0-9]*\.?[0-9]+([eE][-+]?[0-9]+)?)'),      #[Gflop/s]
        'milc': compile('NERSC_TIME\s+([-+]?[0-9]*\.?[0-9]+([eE][-+]?[0-9]+)?)\s+secs'),                                        #[s]
        'mvmc': compile('Walltime of the main kernel:\s+([-+]?[0-9]*\.?[0-9]+([eE][-+]?[0-9]+)?)\s+sec'),                       #[s]
        'ntchem': compile('Walltime of the main kernel:\s+([-+]?[0-9]*\.?[0-9]+([eE][-+]?[0-9]+)?)\s+sec'),                     #[s]
	'simdl': compile('^\s*(?:400000000|4194304)\s+(?:\d+)\s+([-+]?[0-9]*\.?[0-9]+([eE][-+]?[0-9]+)?)'),                     #[usec] for t_min but really only interested in #valid_samples
        'baidu': compile('Verified allreduce for size\(#fp32 elems\)\s+(\d+)\s+\(([-+]?[0-9]*\.?[0-9]+([eE][-+]?[0-9]+)?)\s+'), #[s]
        'ffvc': compile('Walltime of the main kernel:\s+([-+]?[0-9]*\.?[0-9]+([eE][-+]?[0-9]+)?)\s+sec'),                       #[s]
        'hpcg': compile('HPCG result is VALID with a GFLOP/s rating of\s+([-+]?[0-9]*\.?[0-9]+([eE][-+]?[0-9]+)?)'),            #[Gflop/s]
        'imb': compile('^\s*(\d+)(?:\s+\d+)?\s+([-+]?[0-9]*\.?[0-9]+([eE][-+]?[0-9]+)?)'),                                      #[usec] for t_min
        'minife': compile('Walltime of the main kernel:\s+([-+]?[0-9]*\.?[0-9]+([eE][-+]?[0-9]+)?)\s+sec'),                     #[s]
        'multip2p': compile('^\s*4194304\s+(?:\d+)\s+([-+]?[0-9]*\.?[0-9]+([eE][-+]?[0-9]+)?)'),                                #[usec] for t_min but really only interested in #valid_samples
        'netgauge': compile('size:\s+\d+\s+num:\s+\d+\s+average:\s+(?:\d+(?:\.\d+)?)\s+\((\d+(?:\.\d+)?)\s+MiB/s\)'),           #[MiB/s]
        'qball': compile('<timing where="\s*run\s*"\s+name="\s*iteration\s*"\s+min="\s*(\d+(?:\.\d+)?)\s*".*count="\s*1\s*"'),  #[s]
        'swfft': compile('Walltime of the main kernel:\s+([-+]?[0-9]*\.?[0-9]+([eE][-+]?[0-9]+)?)\s+sec')                       #[s]
            }
    bm_unit = {
        'amg': 's',
        #'comd': 'atoms/us',
        'comd': 's',
        'graph500': 'median TEPS',
        'hpl': 'Gflop/s',
        'milc': 's',
        'mvmc': 's',
        'ntchem': 's',
        'simdl': 'usec',
        'baidu': 's',
        'ffvc': 's',
        'hpcg': 'Gflop/s',
        'imb': 'usec',
        'minife': 's',
        'multip2p': 'usec',
        'netgauge': 'MiB/s',
        'qball': 's',
        'swfft': 's'
            }

    logroot = path.join(path.dirname(path.realpath(__file__)), '..', 'log')

    result = {}
    ignore_this_result = False

    for root, dirs, files in walk(path.join(logroot, args.get('__benchmark__'))):
        if not 'throughput' == path.basename(root) and args.get('__throughput__'): continue
        if 'profiles' == path.basename(root) or ('throughput' == path.basename(root) and not args.get('__throughput__')): continue

        for logfile in files:
            if not (args.get('__topo__') in logfile and args.get('__routing__') in logfile and args.get('__placement__') in logfile): continue
            if 'imb' == args.get('__benchmark__') and not args.get('__subbenchmark__') in logfile: continue

            with open(path.join(root, logfile), 'r') as log:
                for line in log:
                    line = line.strip()
                    if bm_reg['nummpi'].match(line) and not bm_reg['ignore'].match(line):
                        ignore_this_result = False
                        num_mpi = int(bm_reg['nummpi'].match(line).group(1))
                        if not num_mpi in result:
                            result[num_mpi] = []
                    elif ignore_this_result:
                        continue
                    if bm_reg['ignore'].match(line):
                        ignore_this_result = True
                        continue
                    if bm_reg[args.get('__benchmark__')].match(line):
                        if 'baidu' == args.get('__benchmark__') or 'imb' == args.get('__benchmark__'):
                            size, time = int(bm_reg[args.get('__benchmark__')].match(line).group(1)), float(bm_reg[args.get('__benchmark__')].match(line).group(2))
                            if not 'Barrier' == args.get('__subbenchmark__'):
                                for i in range(len(result[num_mpi])):
                                    if result[num_mpi][i][0] == size:
                                        if result[num_mpi][i][0] > time:
                                            result[num_mpi][i][1] = time
                                        break
                                else:
                                    result[num_mpi].append([size, time])
                            else:
                                if num_mpi in result:
                                    result[num_mpi].append(time)
                                else:
                                    exit('ERR: someone ate my cookies')
                        else:
                            if num_mpi in result:
                                result[num_mpi].append(float(bm_reg[args.get('__benchmark__')].match(line).group(1)))
                            else:
                                exit('ERR: someone ate my cookies')

    if 'baidu' == args.get('__benchmark__') or ('imb' == args.get('__benchmark__') and not 'Barrier' == args.get('__subbenchmark__')):
        print('#num_mpi\tmsg_size\tt_min[%s]' % bm_unit[args.get('__benchmark__')])
        size_list = {}
        for cfg in sorted(result.keys()):
            for msg_size, t_min in result[cfg]:
                print('%s\t%s\t%s' % (cfg, msg_size, t_min))
                size_list[msg_size] = True
            for msg_size in [s for s in sorted(size_list.keys()) if not size_list[s]]:
                print('%s\t%s\t%s' % (cfg, msg_size, '-1'))
            for s in size_list: size_list[s] = False
    #elif 'imb' == args.get('__benchmark__') and 'Barrier' == args.get('__subbenchmark__'):
    #    print('#num_mpi\tt_min[%s]' % bm_unit[args.get('__benchmark__')])
    #    for cfg in sorted(result.keys()):
    #        print('%s\t%s' % (cfg, result[cfg][0][1]))
    else:
        print('#x_coord\tnum_mpi\tvalid_samples\tmin\tQ1\tmedian\tQ3\tmax (for quartile boxplot)\t#unit: [%s]' % bm_unit[args.get('__benchmark__')])
        c = 2
        if 'fattree' == args.get('__topo__') and 'ftree' == args.get('__routing__') and args.get('__placement__') and len(result.keys()) > 0 and min(result.keys()) == 14: c = 4
        if 'fattree' == args.get('__topo__') and 'ftree' == args.get('__routing__') and args.get('__placement__') and len(result.keys()) > 0 and min(result.keys()) == 16: c = 6
        for cfg in sorted(result.keys()):
            if len(result[cfg]):
                print('%s\t%s\t%s\t%s' % (c, cfg, len(result[cfg]), '\t'.join(['%.8f' % x for x in percentile(result[cfg], [0,25,50,75,100])])))
            else:
                print('%s\t%s\t%s\t%s' % (c, cfg, len(result[cfg]), '\t'.join(5*['-1'])))
            c += 2
    #print(result)


def _get_arg_parser():
    arg_parser = ArgumentParser()
    arg_parser.add_argument(
        '-b',
        dest='__benchmark__',
        help='name of the benchmark',
        required=True,
        choices=['amg', 'comd', 'graph500', 'hpl', 'milc', 'mvmc', 'ntchem', 'simdl', 'baidu', 'ffvc', 'hpcg', 'imb', 'minife', 'multip2p', 'netgauge', 'qball', 'swfft'],
        default=None)
    arg_parser.add_argument(
        '-s',
        dest='__subbenchmark__',
        help='name of the sub benchmark (only applies to IMB)',
        choices=['Barrier', 'Bcast', 'Gather', 'Scatter', 'Reduce', 'Allreduce', 'Alltoall'],
        default=None)
    arg_parser.add_argument(
        '-t',
        dest='__topo__',
        help='name of the network topology',
        required=True,
        choices=['fattree', 'hyperx'],
        default=None)
    arg_parser.add_argument(
        '-r',
        dest='__routing__',
        help='name of the applied routing algorithm',
        required=True,
        choices=['ftree', 'sssp', 'dfsssp', 'parx'],
        default=None)
    arg_parser.add_argument(
        '-p',
        dest='__placement__',
        help='placement of the mpi ranks',
        required=True,
        choices=['linear', 'random', 'cluster'],
        default=None)
    arg_parser.add_argument(
        '--tp',
        dest='__throughput__',
        help='analyze throughput experiment instead of single runs',
        action='store_true',
        default=False)

    args = vars(arg_parser.parse_args())

    bm = args.get('__benchmark__')
    if 'imb' == bm:
        if not args.get('__subbenchmark__'):
            exit('ERR: no sub-benchmark for IMB selected')

    return args


if __name__ == '__main__':
    args = _get_arg_parser()
    parse_logs(args)
