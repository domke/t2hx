#!/usr/bin/env python
from sys import exit, argv
from math import sqrt

N = int(argv[1])
m = int(sqrt(N))
for P in range(m, N+1):
	for Q in range(m, 0, -1):
		if P * Q == N:
			print('%s|%s' %(P, Q))
			exit()
