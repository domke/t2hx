#!/bin/bash

for CA in 0 1; do
	for P in 1 2; do
		if /usr/sbin/ibstat mlx4_${CA} ${P} 2>&1 | /usr/bin/grep LinkUp > /dev/null 2>&1 ; then
			if /usr/sbin/ibnetdiscover > /dev/shm/ibnet ; then
				for LID in `/usr/bin/grep ' lid ' /dev/shm/ibnet | sed -n -e 's/^.* lid //p' | cut -d ' ' -f1 | sort -u`; do
					/usr/sbin/perfquery -R -a ${LID} > /dev/null 2>&1
					/usr/sbin/perfquery -x -R -a ${LID} > /dev/null 2>&1
				done
			fi
		fi
	done
done
