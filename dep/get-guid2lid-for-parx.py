#!/usr/bin/env python3

from sys import exit, stdout
from sys import path as syspath
from os import path
from re import compile, findall
from argparse import ArgumentParser
from contextlib import contextmanager
from math import pow as mpow
syspath.append(path.join(path.dirname(path.realpath(__file__)), 'parseIB.git'))
from ParseInfiniband.ibdiagnet import read_lst
from ParseInfiniband.ibnetdiscover import read as read_ibnet

@contextmanager
def __smart_open(filename=None):
    if filename and filename != '-':
        fh = open(filename, 'w')
    else:
        fh = stdout
    try:
        yield fh
    finally:
        if fh is not stdout:
            fh.close()


def load_network_and_get_guids(args):
    if not isinstance(args, dict):
        exit('ERR: invalid function parameter type(s)')

    # parse the input files to get the network info
    if args.get('__load_input_type__') == 'lst':
        taurus_net, taurus_sw, taurus_ca = read_lst(
            args.get('__in_file__'), False, 'Node,Name,PortGUID')
    elif args.get('__load_input_type__') == 'ibnet':
        taurus_net, taurus_sw, taurus_ca = read_ibnet(
            args.get('__in_file__'), False, 'Node,NodeID,Name,PortGUID')

    return (sorted(taurus_sw), sorted(taurus_ca), taurus_net)


def read_sw_map(map_path):
    sw_map = {}
    try:
        with open(map_path, 'r') as map_file:
            for line in map_file:
                line = line.rstrip().split(',')
                if len(line) < 4:
                    # ignore invalid line
                    continue
                sw_map[int(line[0],16)] = {'Name': line[1], 'Rack': line[2], 'Topo': line[3]}
            return sw_map
    except FileNotFoundError:
        exit('Mapping file %s not found.\n' % map_path)


def _get_arg_parser():
    arg_parser = ArgumentParser()
    arg_parser.add_argument(
        '-o',
        dest='__out_dir__',
        help='name of the output directory to store guid2lid file [default: stdout]',
        metavar='<output dir>',
        default=None)
    arg_parser.add_argument(
        '-i',
        dest='__in_file__',
        help='name of the IB tool logs (either ibdiagnet\'s LST file or ibnetdiscover output)',
        required=True,
        metavar='<input file>',
        default=None)
    arg_parser.add_argument(
        '--it',
        dest='__load_input_type__',
        help='format of the input file [default: ibnet]',
        choices=['lst', 'ibnet'],
        default='ibnet')
    arg_parser.add_argument(
        '-l',
        dest='__lmc__',
        help='lid mask control (LMC) for port lids (default: 0)',
        type=int,
        required=True,
        metavar='<lmc>',
        default=0)

    args = vars(arg_parser.parse_args())

    odir = args.get('__out_dir__')
    if odir:
        if not path.exists(odir):
            exit('ERR: output directory (%s) not found' % odir)

    return args


if __name__ == '__main__':
    args = _get_arg_parser()

    sw, ca, netw = load_network_and_get_guids(args)
    sw_map = read_sw_map(path.join(path.dirname(path.realpath(__file__)), 'clean-fab', 'sw-map.cfg'))

    guid2lid_o = ''
    if args.get('__out_dir__'):
        guid2lid_o = path.join(args.get('__out_dir__'), 'guid2lid')
    lid_offset = int(mpow(2, args.get('__lmc__')))

    with __smart_open(guid2lid_o) as out:
        # layout of the 8x12 hyperx and its 4 quadrants
        # ---------------------------------------------------------------
        # | r10:sw1 r12:sw1 r20:sw1 ... r32:sw1 | r40:sw1  ...  r62:sw1 |
        # | r10:sw2 r12:sw2                :    |                       |
        # | r10:sw3            Q0               |           Q3          |
        # | r10:sw4 r12:sw4     ...     r32:sw4 |                       |
        # --------------------------------------------------------------|
        # | r11:sw1                             |                       |
        # | r11:sw2   :        Q1               |           Q2          |
        # | r11:sw3                             |                       |
        # | r11:sw4 r13:sw4                            ...      r63:sw4 |
        # ---------------------------------------------------------------
        # node in Q0 get lid 2--999, in Q1 1002--1999, etc
        # switches in Q0 get lid 10002-10999, etc
        baselid = 0
        lid_Q0 = 0 + lid_offset
        lid_Q1 = 1000 + lid_offset
        lid_Q2 = 2000 + lid_offset
        lid_Q3 = 3000 + lid_offset
        lid_R  = 4000 + lid_offset
        c_Q0, c_Q1, c_Q2, c_Q3, c_R = 0, 0, 0, 0, 0
        Q0re = compile('r[1-3]{1}[02]{1}[n:]{1}')
        Q1re = compile('r[1-3]{1}[13]{1}[n:]{1}')
        Q3re = compile('r[4-6]{1}[02]{1}[n:]{1}')
        Q2re = compile('r[4-6]{1}[13]{1}[n:]{1}')
        replace = {
                '0x78e7d10300213fbd': compile('r52n23'),
                '0x78e7d103002141d5': compile('r60n28'),
                '0x78e7d10300211abd': compile('r63n10')}
        for guid in ca:
            repl = False
            if Q0re.match(netw[guid]['Name']):
                lid = baselid + lid_Q0 + (c_Q0 * lid_offset)
                c_Q0 += 1
            elif Q1re.match(netw[guid]['Name']):
                lid = baselid + lid_Q1 + (c_Q1 * lid_offset)
                c_Q1 += 1
            elif Q2re.match(netw[guid]['Name']):
                lid = baselid + lid_Q2 + (c_Q2 * lid_offset)
                c_Q2 += 1
            elif Q3re.match(netw[guid]['Name']):
                lid = baselid + lid_Q3 + (c_Q3 * lid_offset)
                c_Q3 += 1
            else:
                lid = baselid + lid_R + (c_R * lid_offset)
                c_R += 1
            for g in replace:
                if replace[g].match(netw[guid]['Name']):
                    out.write('0x%016x 0x%04x 0x%04x\n\n' % (int(g, 16), lid, lid+lid_offset-1))
                    repl = True
                    break
            if not repl:
                out.write('0x%016x 0x%04x 0x%04x\n\n' % (int(netw[guid]['PortGUID'], 16), lid, lid+lid_offset-1))
        baselid = 10000
        c_Q0, c_Q1, c_Q2, c_Q3, c_R = 0, 0, 0, 0, 0
        for guid in sw:
            if Q0re.match(sw_map[int(guid,16)]['Rack']):
                lid = baselid + lid_Q0 + (c_Q0 * lid_offset)
                c_Q0 += 1
            elif Q1re.match(sw_map[int(guid,16)]['Rack']):
                lid = baselid + lid_Q1 + (c_Q1 * lid_offset)
                c_Q1 += 1
            elif Q2re.match(sw_map[int(guid,16)]['Rack']):
                lid = baselid + lid_Q2 + (c_Q2 * lid_offset)
                c_Q2 += 1
            elif Q3re.match(sw_map[int(guid,16)]['Rack']):
                lid = baselid + lid_Q3 + (c_Q3 * lid_offset)
                c_Q3 += 1
            else:
                lid = baselid + lid_R + (c_R * lid_offset)
                c_R += 1
            out.write('0x%016x 0x%04x 0x%04x\n\n' % (int(guid, 16), lid, lid))
