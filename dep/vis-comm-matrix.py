#!/usr/bin/env python3

"""
Usage:
vis-comm-matrix.py <matrix-file>
    will show the plot
vis-comm-matrix.py <matrix-file> save <output>.<ext>; ext can be png, svg, ...
    will save the plot to out.png

Note:
works with both, the output of parse-ibprof.py and parse-tau.py
"""

import csv
import sys
import numpy as np
import matplotlib.pyplot as plt

with open(sys.argv[1], 'r') as f:
    reader = csv.reader(f)
    matrix = list(reader)

matrix = list(map(lambda x: list(map(float, x)), matrix))
npmat = np.array(matrix)
print(npmat)
plt.imshow(npmat);
plt.colorbar()
if len(sys.argv) > 3 and sys.argv[2] == 'save':
    plt.savefig(sys.argv[3])
else:
    plt.show()
