#!/bin/bash

ROOTDIR="$( cd "$( dirname "${BASH_SOURCE[0]:-$0}" )/../" && pwd )"
cd ${ROOTDIR}

GRE='\033[0;32m'
YEL='\033[1;33m'
NCO='\033[0m'

if [ ! 0 ]; then
	# clustershell
	export PYTHONPATH=${PYTHONPATH}:${HOME}/.local/lib
	export PATH=${PATH}:${HOME}/.local/bin
	# and pdsh, pdcp (needed on all nodes to work)
	source ${ROOTDIR}/dep/pdsh-2.33/init
	##pdcp -w 'kiev[0-6]' ./hhh/testfile /dev/shm/
fi

# parallel cpy if needed and pdcp is too slow
export PATH=${ROOTDIR}/dep:${PATH}

# open mpi and tau
source $ROOTDIR/dep/ibprof/init
source $ROOTDIR/dep/tau/init
source $ROOTDIR/dep/openmpi/init

# omp threading: use 1 thread per physical core (nodes: dual socket Xeon 5670)
export NumOMP=$((2*6))

# remove and set individually if needed
unset APPID

# mpi option template
MPI0EXECOPT="--map-by node -x OMP_NUM_THREADS=${NumOMP} -x OMP_PROC_BIND=spread -mca btl openib,self,sm -mca btl_openib_if_include HCA -mca rmaps seq --hostfile ${ROOTDIR}/conf/hosts.LIST -mca orte_base_help_aggregate 0"
#-mca pml ob1	-> is default, no need to specify here
#-mca btl_openib_receive_queues P,65536,120,64,32"

if [[ "$1" = *"intel"* ]]; then
	# use intel compiler and mkl but not mpi
	source ${ROOTDIR}/conf/intel.cfg
	source ${INTEL_ICC} intel64 linux > /dev/null 2>&1
	source ${INTEL_MKL} intel64 lp64 > /dev/null 2>&1
	export OMPI_MPICC=icc
	export OMPI_MPICXX=icpc
	export OMPI_MPIF77=ifort
	export OMPI_MPIF90=ifort
	alias ar=`which xiar`
	alias ld=`which xild`
fi

export TOPOLOGY="hyperx"    #"fattree" #"hyperx"
export PLACEMENT="linear"   #"linear" #"random" #"cluster"
export ROUTING="sssp"       #"ftree" #"sssp" #"parx"
export RUNMODE="single"     #"single" #"multi"

export REPRODPAPER="enable" #"diable"
# Note: simdl/multip2p for RUNMODE=multi included a bug, so the intended
# input for simdl was overwritten by multip2p (and it tested multiple smaller
# msg size instead of 400MB msg size).
# For reproduciability purposes set REPRODEVAL=enable. For executing the
# "intended" workload in the throughput study, set REPRODPAPER=diable.

# dfsssp with SL/VL assignments to prevent deadlocks, so mpi needs to be aware
if [[ "${TOPOLOGY}" = *"hyperx"* ]]; then
	MPI0EXECOPT+=" -mca btl_openib_ib_path_record_service_level 1"
fi

# prepare config and template to trigger OpenSM on the admin node
if [[ "${TOPOLOGY}" = *"hyperx"* ]] && [[ "${ROUTING}" = *"parx"* ]]; then
	# max_lmc=0 -> use all; apm_over_lmc=0 -> disable apm
	# btls_per_lid=1 clear :D; pml=bfo (instead of ob1) -> allow lid-based "multipath"
	# and hack our way into separation of large/small msg instead of alternating usage
	MPI0EXECOPT+=" -mca btl_openib_max_lmc 0 -mca btl_openib_enable_apm_over_lmc 0 -mca btl_openib_btls_per_lid 1 -mca pml bfo"
	HXSMROOTDIR="/root/t2hx"
	export HXSMHOST="root@10.4.0.17"
	export OSM0TRIGGER="/usr/sbin/CommDemands2OSM.py --comm_demands_dir ${HXSMROOTDIR}/conf/comm4parx --comm_demand_files COMMCSV.csv --host_files_dir ${HXSMROOTDIR}/conf --host_files hosts.LIST --csv_output_dir ${HXSMROOTDIR}/conf/opensm --output sm1.hx.comm.demands.cfg --trigger_osm"
fi

