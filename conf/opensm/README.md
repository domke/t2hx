# files
- sm0. configs are for fat-tree
- sm1. configs belong to the hyperx

# setup options
```
mv /etc/opensm /etc/opensm.bak
ln -s <repo>/conf/opensm /etc/opensm
ln -s /etc/opensm/<choose> /etc/opensm/opensm.conf
systemctl restart opensm
```

# getting cn guid if desired
```
ibnetdiscover > /dev/shm/ibnet
grep '^\[.*"r.* HCA' /dev/shm/ibnet | cut -d '(' -f2 | cut -d ')' -f1 | sed -e 's/^/0x/' > /etc/opensm/<cn_guid_file>
```

# getting ft roots if desired
```
ibnetdiscover > /dev/shm/ibnet
grep 'Sw.* Spine' /dev/shm/ibnet | cut -d '"' -f2 | sed -e 's/S-/0x/' > /etc/opensm/<root_guid_file>
```
