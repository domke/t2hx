#!/bin/bash

ROOTDIR="$( cd "$( dirname "${BASH_SOURCE[0]:-$0}" )/../" && pwd )"
cd ${ROOTDIR}

#exec in subshell to not pollute the screen sessions
(
	source ${ROOTDIR}/conf/t2hx.sh
	if ! [[ "${RUNMODE}" = *"multi"* ]]; then
		echo "ERR: must set runmode to multi in ./conf/t2hx.sh"
		exit 42
	fi
); if [ "$?" = "42" ]; then exit; fi

#exec in subshell to not pollute the screen sessions
(
	source ${ROOTDIR}/conf/t2hx.sh
	if [[ ${TOPOLOGY} = *"hyperx"* ]] && [[ ${ROUTING} = *"parx"* ]]; then
		COMMCSVS=""
		HOSTLISTS=""
		# use exactly same order as we got from _gethostlists.sh
		APPS="multip2p.PingPong.56 mvmc.32 milc.32 ntchem.56 comd.56 simdl.SleepyAllreduce.56 minife.56 qball.56 ffvc.32 graph500.32 swfft.32 hpl.56 amg.56 hpcg.56"
		APPID=1
		for APPCFG in ${APPS}; do
			COMMCSVS+=" ${APPCFG}.csv"
			HOSTLISTS+=" hosts.${PLACEMENT}${APPID}.${APPCFG##*.}"
			APPID=$((APPID+1))
		done
		OSMTRIGGER="`echo ${OSM0TRIGGER} | sed -e \"s/COMMCSV\.csv/${COMMCSVS}/\" -e \"s/hosts.LIST/${HOSTLISTS}/\"`"
		/usr/bin/ssh ${HXSMHOST} "${OSMTRIGGER}"
		sleep 20
	fi; exit 0
); if [ "$?" = "42" ]; then exit; fi

screen -S amg      -d -m ${ROOTDIR}/run/amg.sh
screen -S comd     -d -m ${ROOTDIR}/run/comd.sh
screen -S ffvc     -d -m ${ROOTDIR}/run/ffvc.sh
screen -S graph500 -d -m ${ROOTDIR}/run/graph500.sh
screen -S hpcg     -d -m ${ROOTDIR}/run/hpcg.sh
screen -S hpl      -d -m ${ROOTDIR}/run/hpl.sh
screen -S milc     -d -m ${ROOTDIR}/run/milc.sh
screen -S minife   -d -m ${ROOTDIR}/run/minife.sh
screen -S multip2p -d -m ${ROOTDIR}/run/multip2p.sh
screen -S mvmc     -d -m ${ROOTDIR}/run/mvmc.sh
screen -S ntchem   -d -m ${ROOTDIR}/run/ntchem.sh
screen -S qball    -d -m ${ROOTDIR}/run/qball.sh
screen -S simdl    -d -m ${ROOTDIR}/run/simdl.sh
screen -S swfft    -d -m ${ROOTDIR}/run/swfft.sh

