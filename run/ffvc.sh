#!/bin/bash

run_with_tau () {
	mkdir ./tauprofile.n${NumMPI}.${NOW}
	PROFOPT="-x LD_PRELOAD=${TAUDIR}/lib/libTAU.so -x TAU_COMM_MATRIX=1 -x TAU_TRACE=0 -x PROFILEDIR=`pwd`/tauprofile.n${NumMPI}.${NOW}"
}
clean_up_tau () {
	tar czf ${ROOTDIR}/log/${BM}/tauprofile.n${NumMPI}.${NOW}.tgz ./tauprofile.n${NumMPI}.${NOW}
	rm -rf ./tauprofile.n${NumMPI}.${NOW}
}
run_with_ibprof () {
	rm -f ${ROOTDIR}/dep/openmpi; ln -s ${ROOTDIR}/dep/openmpi-ibprof ${ROOTDIR}/dep/openmpi
	PROFOPT="-x LD_PRELOAD=${IBPROFDIR}/lib/libibprof.so"
}
clean_up_ibprof () {
	mv ./ibprofile ./ibprofile.n${NumMPI}.${NOW}; tar czf ${ROOTDIR}/log/${BM}/ibprofile.n${NumMPI}.${NOW}.tgz ./ibprofile.n${NumMPI}.${NOW}
	rm -rf ./ibprofile.n${NumMPI}.${NOW}; rm -f ${ROOTDIR}/dep/openmpi; ln -s ${ROOTDIR}/dep/openmpi-default ${ROOTDIR}/dep/openmpi
}
run_profile() {
	if [[ ${TOPO} = *"tree"* ]] && [[ ${ROUTING} = *"sssp"* ]] && [ ${i} -eq $((NumRunsTEST-1)) ]; then run_with_tau; fi
	if [[ ${TOPO} = *"tree"* ]] && [[ ${ROUTING} = *"sssp"* ]] && [ ${i} -eq ${NumRunsTEST} ]; then run_with_ibprof; fi
}
clean_profile() {
	if [[ ${TOPO} = *"tree"* ]] && [[ ${ROUTING} = *"sssp"* ]] && [ ${i} -eq $((NumRunsTEST-1)) ]; then clean_up_tau; fi
	if [[ ${TOPO} = *"tree"* ]] && [[ ${ROUTING} = *"sssp"* ]] && [ ${i} -eq ${NumRunsTEST} ]; then clean_up_ibprof; fi
	unset PROFOPT
}

if [ -z "${STY}" ]; then echo 'ERR: please run within screen session; or remove check'; exit; fi
NOW="`date +'%F_%Hh%Mm%Ss.%N'`"
SCRIPT="${BASH_SOURCE[0]:-$0}"
BM="`basename ${SCRIPT%.sh}`"
ROOTDIR="$(cd "$(dirname ${SCRIPT})/../" && pwd)"

cd ${ROOTDIR}
source ${ROOTDIR}/conf/t2hx.sh #intel
source ${ROOTDIR}/conf/${BM}.cfg ${RUNMODE}
ulimit -s unlimited
ulimit -n 4096

DEFINPUT=${INPUT}
cd ${APPDIR}
set -x; pdcp -R ssh -w ^${ROOTDIR}/conf/hosts.full ${BINARY} /dev/shm/; set +x
for TOPO in ${TOPOLOGY}; do
	if [[ ${TOPO} = *"tree"* ]]; then HCA=mlx4_0; else HCA=mlx4_1; fi
	if [[ ${TOPO} = *"tree"* ]] && [[ ${ROUTING} = *"sssp"* ]]; then NumRunsTEST=$((2+NumRunsTEST)); fi
	LOG="${ROOTDIR}/log/${BM}/${TOPO}.${ROUTING}.${PLACEMENT}.${HCA}.${NOW}.log"
	if [[ ${RUNMODE} = *"multi"* ]]; then LOG="`echo ${LOG} | sed -e \"s#/log/${BM}/#/log/${BM}/throughput/#\"`"; fi
	mkdir -p `dirname ${LOG}`
	env >> ${LOG} 2>&1
	for TEST in ${TESTCONF}; do
		NumMPI="`echo ${TEST} | cut -d '|' -f1`"
		MPIEXECOPT="`echo ${MPI0EXECOPT} | sed -e \"s/HCA/${HCA}/\" -e \"s/LIST/${PLACEMENT}${APPID}.${NumMPI}/\"`"
		if [[ ${TOPO} = *"hyperx"* ]] && [[ ${ROUTING} = *"parx"* ]] && [[ ${RUNMODE} = *"single"* ]]; then
			OSMTRIGGER="`echo ${OSM0TRIGGER} | sed -e \"s/COMMCSV/${BM}.${NumMPI}/\" -e \"s/LIST/${PLACEMENT}.${NumMPI}/\"`"
			/usr/bin/ssh ${HXSMHOST} "${OSMTRIGGER}"	# ssh key is shared
			sleep 20					# wait a bit for sm to re-route
		fi
		for i in `seq 1 ${NumRunsTEST}`; do
			run_profile
			# ============================ pre-processing =========
			PX="`echo ${TEST} | cut -d '|' -f2`"
			PY="`echo ${TEST} | cut -d '|' -f3`"
			PZ="`echo ${TEST} | cut -d '|' -f4`"
			INPUT="`echo ${DEFINPUT} | sed -e \"s/PX/${PX}/\" -e \"s/PY/${PY}/\" -e \"s/PZ/${PZ}/\"`"
			if [ ${NumMPI} -gt 64 ]; then
				INPUT="`echo ${INPUT} | sed -e \"s/size=128/size=64/\"`"
			fi
			# ============================ pre-processing =========
			echo "mpirun ${MPIEXECOPT} ${PROFOPT} -np ${NumMPI} /dev/shm/`basename ${BINARY}` ${INPUT}" | tee -a ${LOG}
			START="`date +%s.%N`"
			timeout --kill-after=30s ${MAXTIME} mpirun ${MPIEXECOPT} ${PROFOPT} -np ${NumMPI} /dev/shm/`basename ${BINARY}` ${INPUT} >> ${LOG} 2>&1
			if [ "x$?" = "x124" ] || [ "x$?" = "x137" ]; then echo -e "${YEL}Killed after exceeding ${MAXTIME} timeout${NCO}" | tee -a ${LOG}; fi
			ENDED="`date +%s.%N`"
			# ============================ post-processing ========
			rm -f ./history_base.txt ./maprof_output.yaml
			# ============================ post-processing ========
			echo -e "${GRE}Total running time: `echo \"${ENDED} - ${START}\" | bc -l`${NCO}" | tee -a ${LOG}
			clean_profile
		done
	done
done
cd ${ROOTDIR}
set -x; pdsh -R ssh -w ^${ROOTDIR}/conf/hosts.full "rm -f /dev/shm/`basename ${BINARY}`"; set +x

